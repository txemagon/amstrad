;;
;; ENTITY COMPONENT
;; 	1B Component-Type		(cmp type)
;; 	2B Position				(x, y)
;; 	2B Velocity				(vx, vy)
;; 	2B Size					(sx, sy)
;; 	2B Sprite Pointer		(pspr)
;;	2B Last Video Pointer	(lastVP)
;;	2B AI-AIM Target 		(ai_aim_x, ai_aim_y)
;; 	1B AI-status 			(ai_st)
;; 	1B AI-Previous-Status 	(ai_pre_st)
;;  2B AI-Next-Patrol-Step 	(ai_patrol_step)
;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Easily assign offsets to Entity_t members without error
;; 		It adds sizes of types to an offset constant that assigns to members
__off = 0
.macro DefOffset _size, _name
	_name = __off				;; Define _name constant of current offset
	__off = __off + _size     	;; Add sizeof( _name ) = size_to offset
.endm	


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Easily define an enummeration starting in 0
;; 		It adds sizes of types to an offset constant
;;
.macro DefEnum 		_name
	_name'_offset = 0
.endm

;; 	Define enumeration element for an enumeration  name
.macro Enum _enumname, _element
	_enumname'_'_element = _enumname'_offset
	_enumname'_offset = _enumname'_offset + 1
.endm


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Default constructor for Entity components
;;
.macro DefineCmp_Entity_default
	DefineCmp_Entity 0, 0, 0, 0, e_w_invalidEntity, 1, e_cmp_default, 0x0000, e_ai_st_noAI
.endm


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;	Defines an Array of Entity_t with default values
;;
.macro DefineCmpArray_Entity _N 
	.rept _N 
		DefineCmp_Entity_default
	.endm
.endm

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Defines a New Entity_t_struxture
;;		All Entity_t data together to simplify access, at the  cost of
;;

.macro DefineCmp_Entity _x, _y, _vx, _vy, _w, _h, _cmp_type, _pspr, _aist
	.narg __argn
	.if __argn - 9 	;; Validate parameter number
		.error 1
	.else 
		;;;;;; Type of component
		.db _cmp_type		;; Types of components the entity has
		;;;;;; CMP: Position
		.db  _x,  _y 	;; Position
		;;;;;; CMP: Velocity 
		.db _vx, _vy 	;; Velocity
		;;;;;; CMP: Render
		.db  _w,  _h 	;; Size
		.dw	 _pspr 		;; Sprite Pointer
		.dw 0xCCCC 		;; Last Video Memory Pointer Value (for restoring background)
						;; Default to 0xCCCC value to ease memory restoration.
		;;;;;; CMP: AI
		.db 0x00, 0x00  ;; AI-aim (ai_aim_x, ai_aim_y)
		.db _aist       ;; AI-status (ai_st)
		.db _aist       ;; AI-previous status (ai_prev_st)
		.dw nullptr		;; Next AI Patrol Step (ai_patrol_step)
	.endif
.endm

;;-----------------------------
;;-- Entity_t offsets
;;-----------------------------
DefOffset 1, e_cmp_type				;; Component-Type 

DefOffset 1, e_x 	   				;; CMP: Position
DefOffset 1, e_y 	   

DefOffset 1, e_vx 	   				;; CMP: Velocity
DefOffset 1, e_vy 	   

DefOffset 1, e_w 	   				;; CMP: Render
DefOffset 1, e_h 	   
DefOffset 1, e_pspr_l   
DefOffset 1, e_pspr_h   
DefOffset 1, e_lastVP_l
DefOffset 1, e_lastVP_h

DefOffset 1, e_ai_aim_x 			;; CMP: AI
DefOffset 1, e_ai_aim_y 
DefOffset 1, e_ai_st    
DefOffset 1, e_ai_pre_st
DefOffset 1, e_ai_patrol_step_l
DefOffset 1, e_ai_patrol_step_h

DefOffset 0, sizeof_e				;; Total size of Entity Structure


;;-----------------------------
;;-- Entity_t AI-status enum
;;-----------------------------
DefEnum e_ai_st
Enum e_ai_st noAI      
Enum e_ai_st stand_by  
Enum e_ai_st move_to   
Enum e_ai_st patrol 	 


;;-----------------------------
;;-- Entity_t Component Types
;;----------------------------- 
e_cmp_AI 		= 0x01
e_cmp_Render 	= 0x02
e_cmp_Physics 	= 0x04
e_cmp_default 	= e_cmp_Render | e_cmp_Physics

;;-----------------------------
;; Entity_t status enum
;;-----------------------------
e_w_invalidEntity = 0xFF	;; Entitity width = -1


;;-----------------------------
;;-- Utility definitions
;;-----------------------------
nullptr = 0x0000