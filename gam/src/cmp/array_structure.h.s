;;
;; COMPONENT ARRAY DATA STRUCTURE
;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Generate data structure and array of type-T components
;;		Macro to generate all data structure required
;;		to managa an array of components of the same type.
;; It generates these labels.
;;		* T_num:	A byte to count the number or elements in the array
;;		* T_plast:	A word to store a pointer to the end of the array
;;					This will be the first free element in the array
;;		* T_array:	The array itself
;; INPUTS:
;;		_Tname:		Name of the component type
;;		_N:			Size of the array in number of components (to reserve)
;;		_DefineTypeMacroDefault:	Macro to be called to generate a de

.macro DefineComponentArrayStructure _Tname, _N, _DefineTypeMacroDefault
	_Tname'_num:		.db 0				;; Number of defined elements
	_Tname'_pend:		.dw _Tname'_array	;; Pointer to the end of
	_Tname'_array:							;; Array of elements of
	.rept	_N
		_DefineTypeMacroDefault 			;; Repeatedly call default
	.endm
.endm


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Same macro as before, but generating a bunch of zeros instead of
;; It uses _ComponentSize instead of _DefineTypeMacroDefault to pro
;; INPUTS:
;;		Same as before except,
;;		_ComponentSize: Size in bytes of T-component
;;
.macro DefineComponentArrayStructure_size _Tname, _N, _ComponentSize
	_Tname'_num: 		.db 0				;; Number of defined elements
	_Tname'_pend:		.dw _Tname'array 	;; Pointer to the end of
	_Tname'_array:							;; Array of elements of
		.ds 	_N * _ComponentSize
.endm


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Structure to store an array of pointers to AI
;; end in a null pointer
;; Always one pointer more than needed
;; INPUTS:
;;		Tname: 	Name of the component type
;; 		N:		Maximum number of pointers to store
.macro DefineComponentPointersArrayStructure_Size _Tname, _N
	_Tname'_ptr_pend: .dw _Tname'_ptr_array
	_Tname'_ptr_array::
	.rept _N
		.dw 0x0000
	.endm
.endm