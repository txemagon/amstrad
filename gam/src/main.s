.include "cpctelera.h.s"
.include "cpct_functions.h.s"
.include "man/game.h.s"

.module main

.area _DATA
.area _CODE

_main::
   ;; Disable firmware to prevent interferences
   call cpct_disableFirmware_asm

   ;; INIT Game Manager
   call man_game_init

loop:
    call    man_game_update
    ;waitVSyncs 2
    call    cpct_waitVSYNC_asm
    call    man_game_render
    jr    loop
