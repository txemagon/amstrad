;; 
;; SQAURE RENDER SYSTEM
;;		Renders Square components
;;
.include "cmp/entity.h.s"
.include "man/entity.h.s"
.include "cpctelera.h.s"
.include "cpct_functions.h.s"
.include "assets/assets.h.s"

.module sys_entity_render

;;===========================================================================
;; Square Render System Constants
screen_start = 0xC000 				;; Video memory start

;;===========================================================================
;; Render System Variables

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  SYS_ENTITYRENDER::Init
;; 		Inits the render system
;;  DESTROYS:  AF, BC, DE, HL
;;

sys_eren_init::
   	ld 		(_ent_array_ptr), hl 			 	;; [6]

	ld		 c, #0				;; [2] Mode 1
	call cpct_setVideoMode_asm 	;; [x] Sets the video mode
	ld 		hl, #_pal_main 		;; [3] Palette Array
	ld 		de, #16 			;; [3] Sizeof the palette
	call 	cpct_setPalette_asm	;; [x] Set the palette
	cpctm_setBorder_asm HW_WHITE;; [x] Set the border

	ld 		hl, #sys_eren_first_render_entities
	ld 		(_render_function_ptr), hl

		;; Render BG
;; 	ld 		hl, #_bg_chicagos
;;	ld 		de, #0xC000
;;	ld 		bc, #0x4000
;;	ldir

	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  SYS_ENTITYRENDER::Update
;;		Updates the render system
;; 		Draws all Entity Components
;;		Assumes that Entities are contigous and all values
;; 		Assumes thes is at least one entity to render
;;  INPUT:
;;		IX = Pointer to the entity array
;;		 A = Number of elements in the array
;; 	DESTROYS:  AF, HL, BC, DE, IX
;;	STACK USE: 2 bytes
;;
sys_eren_update::
_render_function_ptr = .+1
	;; Render Entities
	call sys_eren_first_render_entities	;; [x] Render all entities

	ret 


sys_eren_first_render_entities::

    ld 		ix, (_ent_array_ptr)				;; [6]	

_update_loop2:

	ld 		a, e_w(ix) 				;; [5] A = Entity.width
	cp 		#e_w_invalidEntity 		;; [1] if (Entity.width == invalid)
	jr  	z, _change_to_no_first
	
	;; Erase Previous Instance (Draw Background Pixels)
;	ld 		 e, e_lastVP_l(ix) 		;; [6] / DE points to last drawn memory address
;	ld 		 d, e_lastVP_h(ix)		;; [6] \
;	ld 		 l, e_pspr_l(ix)		;; [6] / HL Points to the sprite
;	ld 		 h, e_pspr_h(ix) 		;; [6] \
;	ld 		 b, e_w(ix) 			;; [6] B = Width
;	ld 		 c, e_h(ix) 			;; [6] C = Height
;	push  	bc 						;; [4] Push Width-Height
;;	call cpct_drawSolidBox_asm 		;; [x] Draw a solid box where the sprite lays
;	call cpct_drawSpriteBlended_asm

	;; Calculate new Video Memory Pointer
	ld 		de, #screen_start 		;; [3] DE Points to the start of video memory
	ld 		 c, e_x(ix) 			;; [6] C = X
	ld 		 b, e_y(ix) 			;; [6] B = Y
	call cpct_getScreenPtr_asm 		;; [x] Get Screen Address

	;; Store Video Memory Pointer as Last
	ld 		e_lastVP_l(ix), l 		;; [6] / Store HL as last 
	ld 		e_lastVP_h(ix), h 		;; [6] \ memory position

	;; Draw Entity Sprite
	ex 		de, hl 					;; [1] DE Points to video memory
	ld 		 l, e_pspr_l(ix)		;; [6] / HL Points to the sprite
	ld 		 h, e_pspr_h(ix) 		;; [6] \	
	ld 		 b, e_w(ix) 			;; [6] B = Width
	ld 		 c, e_h(ix) 			;; [6] C = Height
 									;;     Restore Width-Height
;		call cpct_drawSprite_asm		;; [x]
	call cpct_drawSpriteBlended_asm ;; [x]
    

	ld 		bc, #sizeof_e 			;; [3] BC = sizeof(SquareEntity)
	add 	ix, bc 					;; [4] IX Points to next Entity
	jr 		_update_loop2 			;; [3] Continue updating

_change_to_no_first:
	ld 		hl, #sys_eren_render_entities
	ld 		(_render_function_ptr), hl
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  SYS_ENTITYRENDER::Update
;; 		Draws all Entity Components
;;		Assumes that Entities are contigous and all values
;; 		Assumes thes is at least one entity to render
;;  INPUT:
;;		IX = Pointer to the entity array
;;		 A = Number of elements in the array
;; 	DESTROYS:  AF, HL, BC, DE, IX
;;	STACK USE: 2 bytes
;;
sys_eren_render_entities::

_ent_array_ptr = .+2
    ld 		ix, #0x0000				;; [4]	

_update_loop:

	ld 		a, e_w(ix) 				;; [5] A = Entity.width
	cp 		#e_w_invalidEntity 		;; [1] if (Entity.width == invalid)
	ret 	z
	
;	cpctm_setBorder_asm HW_RED
	;; Erase Previous Instance (Draw Background Pixels)
	ld 		 e, e_lastVP_l(ix) 		;; [6] / DE points to last drawn memory address
	ld 		 d, e_lastVP_h(ix)		;; [6] \
	ld 		 l, e_pspr_l(ix)		;; [6] / HL Points to the sprite
	ld 		 h, e_pspr_h(ix) 		;; [6] \
;	xor 	 a 						;; [1] A = Background colour
	ld 		 b, e_w(ix) 			;; [6] B = Width
	ld 		 c, e_h(ix) 			;; [6] C = Height
	push  	bc 						;; [4] Push Width-Height
;	call cpct_drawSolidBox_asm 		;; [x] Draw a solid box where the sprite lays
	call cpct_drawSpriteBlended_asm

	;; Calculate new Video Memory Pointer
	ld 		de, #screen_start 		;; [3] DE Points to the start of video memory
	ld 		 c, e_x(ix) 			;; [6] C = X
	ld 		 b, e_y(ix) 			;; [6] B = Y
	call cpct_getScreenPtr_asm 		;; [x] Get Screen Address

	;; Store Video Memory Pointer as Last
	ld 		e_lastVP_l(ix), l 		;; [6] / Store HL as last 
	ld 		e_lastVP_h(ix), h 		;; [6] \ memory position

	;; Draw Entity Sprite
	ex 		de, hl 					;; [1] DE Points to video memory
	ld 		 l, e_pspr_l(ix)		;; [6] / HL Points to the sprite
	ld 		 h, e_pspr_h(ix) 		;; [6] \	
	pop 	bc 						;; [3] Restore Width-Height
;		call cpct_drawSprite_asm		;; [x]
	call cpct_drawSpriteBlended_asm ;; [x]
    
;    cpctm_setBorder_asm HW_WHITE

	ld 		bc, #sizeof_e 			;; [3] BC = sizeof(SquareEntity)
	add 	ix, bc 					;; [4] IX Points to next Entity
	jr 		_update_loop 			;; [3] Continue updating