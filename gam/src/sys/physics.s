;;
;; ENTITY PHYSICS MOVEMENT
;;		Moves entities according to velocity
;; 		Takes into account Screen Borders
;;
.include "cmp/entity.h.s"
.include "man/entity.h.s"
.include "cpctelera.h.s"

.module sys_entity_physics

;;===========================================================================
;; Physics System Constants
screen_width   = 80
screen_height  = 200

;;
;; Inits the render system
;;
sys_physics_init::
   	ld 		(_ent_array_ptr), hl 			 	;; [6]
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; SYS_PHYSICS::Init
;; 		Updates all physics components of entities
;;		Assumes that entities are contigouous and all values are 
;;  INPUT:
;;			IX = Pointer to the entity array
;;  DESTROYS:  AF, BC, DE, IX
;;  STACK USE: 2 bytes
;;
sys_physics_update::

_ent_array_ptr = .+2
    ld 		ix, #0x0000				;; [4]

_update_loop:
	ld 		a, e_w(ix) 				;; [5] A = Entity.width
	cp 		#e_w_invalidEntity 		;; [1] if (Entity.width == invalid)
	ret 	z

	;; Update x
	ld 		 a, #screen_width + 1 	;; [2] /
	sub 	e_w(ix)					;; [5] | C = screeen width
	ld 		 c, a 					;; [1] \

	ld 		 a, e_x(ix)				;; [6] A = Entity.x
	add 	e_vx(ix) 				;; [5] Enitity.x += Entity.vx
	cp 		 c 						;; [5] Check for entity.x > screen width
	jr 		nc, invalid_x 			;; [2/3] If yes then Undo movement
valid_x:							;; IF
		ld 		 e_x(ix), a 		;; [6] New x is valid. Store value in x.
		jr 		endif_x				;; [3] End if part
invalid_x:
		ld 		 a, e_vx(ix) 		;; [6] /
		neg 						;; [2] | Entitiy.vx *= -1
		ld 		e_vx(ix), a 		;; [6] \
endif_x:

;; Update y
	ld 		 a, #screen_height + 1 	;; [2] /
	sub 	e_h(ix)					;; [5] | C = screeen height
	ld 		 c, a 					;; [1] \

	ld 		 a, e_y(ix)				;; [6] A = Entity.y
	add 	e_vy(ix) 				;; [5] Enitity.y += Entity.vy
	cp 		 c 						;; [5] Check for entity.y > screen height
	jr 		nc, invalid_y 			;; [2/3] If yes then Undo movement
valid_y:							;; IF
		ld 		 e_y(ix), a 		;; [6] New x is valid. Store value in y.
		jr 		endif_y				;; [3] End if part
invalid_y:
		ld 		 a, e_vy(ix) 		;; [6] /
		neg 						;; [2] | Entitiy.vy *= -1
		ld 		e_vy(ix), a 		;; [6] \
endif_y:

	ld 		de, #sizeof_e 			;; DE = sizeof(Entity_t)
	add 	ix, de 					;; IX Points to next Entity_t
	jr 		_update_loop 			;; Continue updating
	