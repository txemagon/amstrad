;;
;; ENTITY AI CONTROL SYSTEM
;;
.include "cmp/entity.h.s"
.include "man/patrol.h.s"
.include "man/entity.h.s"
.include "man/entity_ai.h.s"
.include "cpctelera.h.s"

.module sys_ai_control

;;===========================================================================
;; AI_CONTROL System Constants
;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; SYS_AI_CONTROL::Init
;; 		Inits the AI-control system
;;  INPUT: -
;;		HL = Pointer to the entity array
;
sys_ai_control_init::
	;; Get a pointer to the AI_Entity_array_Ptr
	;; And save it for further use
	call 	man_entity_ai_getArrayHL			;; [x]
    ld 		(_ent_array_ptr), hl 			 	;; [6]
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; SYS_AI_CONTROL::Update
;; 		Updates all AI entities
;;  INPUT:
;;			IX = Pointer to the entity array
;;  DESTROYS:  AF, BC, DE, HL, IX
;;
sys_ai_control_update::
_ent_array_ptr = .+1
    ld 		hl, #0x0000				;; [3] HL = &AI_Entity_Array_Ptr[0]

_loop:
	;; Get Pointer to next AI_Entity in DE
	ld 		e, (hl) 				;; [2] /
	inc 	hl 						;; [2] | DE = &AI_Entity
	ld 		d, (hl) 				;; [2] \
	inc 	hl 						;; [2] HL += 2 (Points to next pointer)

	;; Check for &AI_Entity being nullptr
	;; In which case return
	ld 		a, e 					;; [1]   /
	or 		d 						;; [1]   | IF (DDE == nullptr)
	ret 	z						;; [3/4] \ THEN Return

	;; IX = DE (IX = &AI_Entity)
	ld__ixl_e						;; [2] / IX = DE = &AI_Entity
	ld__ixh_d						;; [2] \

	;; Perform AI Behaviour
	ld 		a, e_ai_st(ix) 			;; [5] A = AI_Entity->ai_status
	cp 		#e_ai_st_stand_by 		;; [2] / IF (ai_status == stand_by)
	call 	z, sys_ai_stand_by		;; [x] \ THEN call stand_by
	cp 		#e_ai_st_move_to 		;; [2] / IF (ai_status == move_to)
	call 	z, sys_ai_move_to		;; [x] \ THEN call move_to
	cp 		#e_ai_st_patrol 		;; [2] / IF (ai_status == patrol)
	call 	z, sys_ai_patrol		;; [x] \ THEN call patro

	jr 		_loop
;;=============================================================================
;;=============================================================================
;; BEHAVIOURS
;;=============================================================================
;;=============================================================================

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; SYS_AI_CONTROL::stand_by
;;		An entity is waiting for an event to occur
;; INPUT:
;;		IX = Pointer to the entity
;; DESTROYS:	AF, IY
;;

sys_ai_stand_by::
_ent_array_ptr_temp_standby = .+2
	ld 		iy, #0x0000				;; [4]

	ld 		a, e_ai_aim_x(iy)
	or 		a
	ret 	z

	;; Pressed key: Move to player
	ld 		e_ai_pre_st(ix), #e_ai_st_stand_by
	ld 		e_ai_st(ix), #e_ai_st_patrol

    ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; SYS_AI_CONTROL::move_to
;;		The entity changes its velocity to move to
;;		an X, Y location (e_ai_aim_x, e_ai_aim_y)
;; INPUT:
;;		IX = Pointer to the entity
;; DESTROYS:	AF, IY
;;

sys_ai_move_to::
	ld 		e_vx(ix), #0

    ;; UPDATE VX
	ld 		a, e_ai_aim_x(ix)			;; A = x_objetivo
	sub 	e_x(ix)						;; A = obj_x - x
	jr 		nc, _objx_greater_or_equal	;; if ( obj_x - x > 0) objx_greater
_objx_lesser:
	ld 		e_vx(ix), #-1
	jr 		_endif_x
_objx_greater_or_equal:
    jr 		z, _arrived_x
	ld 		e_vx(ix), #1
	jr 		_endif_x
_arrived_x:
    ld		e_vx(ix), #0
	
_endif_x:

	;; UPDATE VY
	ld 		e_vy(ix), #0
    ;; if (obj_y > y) ...
	ld 		a, e_ai_aim_y(ix)			;; A = y_objetivo
	sub 	e_y(ix)						;; A = obj_y - y
	jr 		nc, _objy_greater_or_equal	;; if ( obj_y - y > 0) objx_greater
_objy_lesser:
	ld 		e_vy(ix), #-1
	jr 		_endif_y
_objy_greater_or_equal:
    jr 		z, _arrived_y
	ld 		e_vy(ix), #1
	jr 		_endif_y
_arrived_y:
    ld		e_vy(ix), #0
    ld 		a, e_vx(ix)					;; [5]
    or 		a
    jr 	 	nz, _endif_y
    ;; x e y arrived both
    ld 		a, e_ai_pre_st(ix)
	ld 		e_ai_st(ix), a  			;; [5] AI status
	ld 		e_ai_pre_st(ix), #e_ai_st_move_to
_endif_y:
    ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; SYS_AI_CONTROL::patrol
;;		
;; INPUT:
;;		IX = Pointer to the entity
;; DESTROYS:	AF, IY
;;

sys_ai_patrol::
	;; HL Points to next (X, Y) AI patrol step
	ld 		h, e_ai_patrol_step_h(ix)
	ld 		l, e_ai_patrol_step_l(ix)  

	
	ld 		a, (hl)					;; A = X
	cp      #patrol_invalid_move_x
	jr 		z, _reset_patrol

	;; Set AI-AIM (X, Y)
	ld 		e_ai_aim_x(ix), a

	inc 	hl
	ld 		a, (hl)					;; A = Y
	ld 		e_ai_aim_y(ix), a

	;; SET e_ai_patrol_step + 2
	inc 	hl
	ld 		e_ai_patrol_step_l(ix), l
	ld 		e_ai_patrol_step_h(ix), h

	;; CHANGE TO move to
	ld 		e_ai_pre_st(ix), #e_ai_st_patrol
	ld 		e_ai_st(ix), #e_ai_st_move_to

	ret

_reset_patrol:
	;; GET next 2 Bytes pointed by HL, which
	;; point to the start of the patrol
	inc 	hl
	ld 		a, (hl)
	inc 	hl
	ld 		h, (hl)

	;; Reset Patrol Step
	ld 		e_ai_patrol_step_l(ix), a
	ld 		e_ai_patrol_step_h(ix), h

	ret