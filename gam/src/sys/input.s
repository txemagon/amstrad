;;
;; INPUT SYSTEMS
;; 		Reads input from keyboard and joysticks
;; 		And updates controlled entities accordingly
;;
.include "cpctelera.h.s"
.include "man/entity.h.s"
.include "cmp/entity.h.s"
.include "cpct_functions.h.s"

.module sys_input

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; SYS_INPUT::Init
;; 		Initializes Input Systems
;;
sys_input_init::
	ld 		(_ent_array_ptr), hl 			 	;; [6]
	ret 										;; [3] Return

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; SYS_INPUT::Update
;; 		  Gets keyboard inputs and applies them
;; 		to keyboard controlled entities.
;; 		  At present stage, only entity[0] is
;; 		considered to be a keyboard controlled entity.
;; 		  Assumes entity 0 always exists
;;  INPUT
;;	   IX = Pointer to entity[0]
;;  DESTROYS: AF, BC, DE, HL, IX
;;
sys_input_update::
_ent_array_ptr = .+2
    ld 		ix, #0x0000				;; [4]
    
	;; Reset Velocities
	ld 	e_vx(ix), #0				;; [5] / Set Entity Velocity
	ld 	e_vy(ix), #0				;; [5] \

	;; Scan the keyboard
	call cpct_scanKeyboard_f_asm	;; [x] Update keyboard

	;; Check for movement keys
	ld 		hl, #Key_O 					;; [3]	 Hl holds Key O
	call cpct_isKeyPressed_asm			;; [x]   Check if Key O is pressed
	jr 		z, O_NotPressed 			;; [2/3] if (not pressed)
O_Pressed:
		ld 		e_vx(ix), #-1			;; [5]   Key_O is pressed => vx = -1
O_NotPressed:							;;

		ld 		hl, #Key_P 				;; [3] 	 HL holds Key_P
		call cpct_isKeyPressed_asm 		;; [x] 	 Check if Key_P is pressed
		jr 		 z, P_NotPressed 		;; [2/3] If (not pressed) skip update
P_Pressed:
		ld 		e_vx(ix), #1			;; [5]   Key_P is pressed => vx = 1
P_NotPressed:


	ld 		hl, #Key_Q 					;; [3]	 Hl holds Key Q
	call cpct_isKeyPressed_asm			;; [x]   Check if Key Q is pressed
	jr 		z, Q_NotPressed 			;; [2/3] if (not pressed)
Q_Pressed:
		ld 		e_vy(ix), #-2			;; [5]   Key_Q is pressed => vy = -1
Q_NotPressed:							;;

		ld 		hl, #Key_A 				;; [3] 	 HL holds Key_A
		call cpct_isKeyPressed_asm 	 	;; [x] 	 Check if Key_A is pressed
		jr 		 z, A_NotPressed 		;; [2/3] If (not pressed) skip update
A_Pressed:
		ld 		e_vy(ix), #2			;; [5]   Key_A is pressed => vy = 1
A_NotPressed:

		ld 		e_ai_aim_x(ix), #0		;; [5]   Key_Space is not pressed by default => ai_aim_x = 0
		ld 		hl, #Key_Space			;; [3] 	 HL holds Key_Space
		call cpct_isKeyPressed_asm 		;; [x] 	 Check if Key_Space is pressed
		jr 		 z, Space_NotPressed 	;; [2/3] If (not pressed) back to not aiming
Space_Pressed:
		ld 		e_ai_aim_x(ix), #1		;; [5]   Key_Space is pressed => ai_aim_x = 1
Space_NotPressed:

		ret 