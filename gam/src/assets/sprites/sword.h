// Data created with Img2CPC - (c) Retroworks - 2007-2017
#ifndef _IMG_SWORD_H_
#define _IMG_SWORD_H_

#include <types.h>
#define SP_SWORD_W 5
#define SP_SWORD_H 12
extern const u8 sp_sword[5 * 12];

#endif
