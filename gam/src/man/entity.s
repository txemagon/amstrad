;;
;; COMPONENT MANAGER
;;
.include "cmp/array_structure.h.s"
.include "cmp/entity.h.s"
.include "man/entity.h.s"
.include "man/entity_ai.h.s"
.include "assets/assets.h.s"
.include "cpctelera.h.s"

.module entity_manager

;;===========================================================================
;; Manager Member Variables

;;------------- Entity Components ------------
DefineComponentArrayStructure _entity, max_entities, DefineCmp_Entity_default

;;===========================================================================
;;===========================================================================
;; Manager Public Functions

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Man_Entity::getArray
;;	  Gets a pointer to the array of entities
;;	  in HL
;; INPUT: -
;; DESTROYS: HL
;; RETURNS:
;;	 HL:	Pointer to the start of the array
;;
man_entity_getArray::
	ld	hl, #_entity_array		;; [4] HL = Pointer to the entity array
	ret 						;; [3]

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Man_Entity::Init
;;	  Initializes the entity manger. It
;;	  sets up everything with 0 entities and
;;	  ready to start creating new ones.
;;	INPUT: -
;;	DESTROYS: AF, HL
;;  RETURNS:
;; 		HL: 	Pointer to the start of the array
;;
man_entity_init::
	;; Init other subsidiary managers
	call 	man_entity_ai_init 	;; [x]

	;; Reset all component vector values
	xor		 a 					;; [1] / _entity_num = 0
	ld		(_entity_num), a	;; [4] \ Set number of entities to zero

	ld 		hl, #_entity_array 	;; [3] / _entity_pend = & entities[0]
	ld 		(_entity_pend), hl 	;; [5] \ Entities end pointer points

	;; First entity must be marked as no entity
	;; as it is the last valid entity in the array
	ex		de, hl 						;; [1] Save HL into DE
	ld 		hl, #e_w					;; [3] HL = Offstet of e_w in Entity_t
	add 	hl, de 						;; [3] HL = Entity + e_w (Points to e_w field)
	ld 		(hl), #e_w_invalidEntity	;; [2] Entity[0].e_w = e_width_not_valid
	ex 		de, hl 						;; [1] Restore HL = &Entity[0] to

	ret 								;; [3] Everything initialized. Return.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Man_Entity::new
;;---------------------------------------------------------------------------
;;	   Adds a new entity component to the array
;;	   without initializing it. It does not
;;	   perform any check for space in the array.
;;	DESTROYS: F, BC, DE, HL
;;	RETURN:
;;	   DE: Points to added element
;;	   BC: Sizeof (Entity_t)
;;
man_entity_new::
	;; Increment number of reserved entities
	ld 		hl, #_entity_num 	;; [3] / _entity_num++
	inc 	(hl) 				;; [2] \

	;; Increment Array end pointer to point to the next
	;; free element in the array
	ld		hl, (_entity_pend)	;; [5] /
	ld 		 d, h 				;; [1] | / DE = HL
	ld 		 e, l 				;; [1] | \ DE Points to the added element
	ld 		bc, #sizeof_e 		;; [3] | _entity_pend += sizeof(Entity_t)
	add 	hl, bc 				;; [3] |
	ld 		(_entity_pend), hl 	;; [5] \ Store pend pointer updated
    
	ret 						;; [3]

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Man_Entity::create
;;---------------------------------------------------------------------------
;; 		Creates and initializes new entity
;;		Returns a pointer to newly created entity in IX
;;	INPUT:
;;		HL: Pointer to initializer values for the entity to be created
;;	DESTROYS:	F, BC, HL, DE
;;	STACK USE:	2 bytes
;;	RETURN:
;;		IX: Pointer to the newly created component
;;
man_entity_create::
	push	hl 				;; [4] Save pointer to the initialization values
	call 	man_entity_new	;; [x] Add a new entity to the vector first free place

	;;	 IX = DE (IX returns the value of the pointer to the entity created)
	ld__ixh_d				;; [2] / IX = DE
	ld__ixl_e				;; [2] \ IX points to the new entity space

	;; Copy initialization values to new entity
	;; DE points to the new added entity
	;; BC holds sizeof(Entity_t)
	;; Copy initialization values
	pop 	hl 				;; [3] HL = Pointer to the initialization values
	ldir 					;; [x] Initialization Values -> New Entity

	;; Check for components to add the entity to
	;; its correspondent pointer arrays in other
	;; managers
	ld 		a, e_cmp_type(ix) 		;; [5] A = Entity_t.component type
	and 	#e_cmp_AI 				;; [2] / If (has AI component)
	call 	nz, man_entity_ai_add	;; [x] \ the {Add it to Entity AI}

	ret 					;; [3] Return