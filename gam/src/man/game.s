;;
;; GAME SYSTEMS MANAGER
;;
.include "cpctelera.h.s"
.include "cmp/entity.h.s"
.include "sys/physics.h.s"
.include "sys/render.h.s"
.include "sys/input.h.s"
.include "sys/ai_control.h.s"
.include "man/patrol.h.s"
.include "man/entity.h.s"
.include "man/entity_ai.h.s"
.include "assets/assets.h.s"

.module game_manager

;;============================================
;; Manager Configuration Constants

;;============================================
;; Manager Member Variables
e_comp_ai_entity = e_cmp_default | e_cmp_AI
ent1: DefineCmp_Entity	 0,	  0,	1,	   2,	4,	16,	e_cmp_default, _sp_mainchar, e_ai_st_noAI
ent2: DefineCmp_Entity	70,  40, 0xFF, 	0xFE,	4,	 8,	e_cmp_AI, _sp_redball, e_ai_st_patrol
ent3: DefineCmp_Entity 	40,	120,	2,	0xFC,	5,	12,	e_cmp_AI, _sp_sword, e_ai_st_patrol

;;============================================
;; Manager Public Functions


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Man_Game::Init
;;	 Initializes the Game Manager to set
;; it up for the start of a new game.
;; INPUT: -
;; DESTROYS: AF; BF, DE, HL, IX 
;;
man_game_init::
	;; Init Entity Manager
	call	man_entity_init			;; HL = Entity Array

	;; Init Systems
	call	sys_physics_init
	call	sys_input_init
	call	sys_eren_init			;; Modifies HL
	call	sys_ai_control_init

	;; Init 3 Test Entities
	ld		hl, #ent1
	call	man_entity_create
	ld		hl, #ent2
	call	man_entity_create
	ld 		hl, #ent3
	call	man_entity_create

	call 	man_entity_ai_getArrayHL					;; \
	call 	man_patrol_init

	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Man_Game::Update
;;	 Updates 1 Game Cycle doing everything
;; except rendering. Rendering is
;; considered apart for its time constraints
;; INPUT: -
;; DESTROYS: - 
;;
man_game_update::
	call	sys_input_update
	call	sys_ai_control_update
	call	sys_physics_update

	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Man_Game::Render
;;	 Does rendering process apart from
;; game Update to account for its time constraints
;; INPUT: -
;; DESTROYS: - 
;;
man_game_render::
	;cpctm_setBorder_asm HW_RED
	call	sys_eren_update
	;cpctm_setBorder_asm HW_WHITE

	ret