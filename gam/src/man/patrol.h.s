;;
;; PATROL MANAGER
;;


;;
patrol_invalid_move_x = -1

.globl man_patrol_get
.globl man_patrol_init

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Defines a patrol name
.macro DefPatrol _name
	.globl _name
	_name::
.endm


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Defines a Point intented to be  used on patrols.
.macro DefPoint _x, _y
	.db _x, _y
.endm


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Ends a patrol by assigning invalid_move_x and 
;; 		points back to the first patrol point
;;
.macro EndPatrol _name
	.db  patrol_invalid_move_x
	.dw _name
.endm