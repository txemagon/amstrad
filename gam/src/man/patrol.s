;;
;; PATROL MANAGER
;;

.include "cpctelera.h.s"
.include "man/patrol.h.s"
.include "cmp/entity.h.s"

.module patrol_manager
;;
;; Patrol 01 Manually defined
;;
DefPatrol _patrol_01
DefPoint  06,  06
DefPoint  60,  40
DefPoint  30, 120
DefPoint   2,  50
EndPatrol _patrol_01

;;
;; Patrol 02 Manually defined
;;
DefPatrol _patrol_02
DefPoint  40,  10
DefPoint  30, 130
DefPoint  70, 150
DefPoint  60,  90
DefPoint  10,  40
EndPatrol _patrol_02


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  Gets in HL a pointer to the patrol number A
;; A = Patrol Select [0, 1, 2, ...]
;;
man_patrol_get::
	or 		a 
	jr 		nz, _p2
_p1:
	ld 		hl, #_patrol_01
	ret
_p2:
	ld 		hl, #_patrol_02
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Gets in HL a pointer to an array of pointers to IA objects
;; HL = Entity_t **
;; Sets patrol for all entities
;; 	  INPUTS: 		HL
;;	  DESTROYS: 	A, B, DE, HL, IX
man_patrol_init::
	;; First patrol will be Patrol-ID 0
	ld 		b, #0		;; B = Patrol ID

_loop:
	ld 		e, (hl)					;; Gets low byte of pointer
	inc 	hl
	ld 		d, (hl)                 ;; DE = (HL)
	inc 	hl 						;; HL Pointer to next object to be processed
	ld__ixh_d 						;; IX = DE
	ld__ixl_e
	;; Exit on invalid entity
	ld 		a, e_w(ix)
	cp 		#e_w_invalidEntity
	ret 	z

	;; Check if it has AI
	ld  	a, e_ai_st(ix)
	cp 		#e_ai_st_noAI
	jr 		z, _no_AI_ent

	;; Entity has AI, set Patrol
	push 	hl
	ld 		a, b
	call 	man_patrol_get
	ld 		e_ai_patrol_step_l(ix), l
	ld 		e_ai_patrol_step_h(ix), h 
	inc 	b
	pop 	hl

_no_AI_ent:

	jr 		_loop
	ret