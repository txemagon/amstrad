;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; AI ENTITY MANAGER
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

.include "cpctelera.h.s"
.include "cmp/array_structure.h.s"
.include "man/entity.h.s"

.module entity_ai_manager

;;===========================================================================
;; Entity AI Manager Member Variables


;;     Pointer arrays required by systems
DefineComponentPointersArrayStructure_Size _ai, max_entities

;;===========================================================================
;;===========================================================================
;; Member Public functions

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  Man_Entity_AI::init
;; 		Initializes the AI-Entity pointer vector
;;  INPUT:
;;  DESTROYS:  AF, BC; DE, HL
;;  RETURNS
;;		HL		Pointer to the start of the pointer array

man_entity_ai_init::
	;; -- Pointer to Array End to AI_Ptr_Array[0]
	ld 		hl, #_ai_ptr_array 			;; [3] HL Points to AI_Ptr_Array[0]
	ld 		(_ai_ptr_pend), hl 			;; [5] AI_Ptr_Pend = &AI_Ptr_Array[0]

	;; Set all values of the pointer array to nullptr
	;; filling in the array with 0's
	xor 	a 							;; [1] A = 0
	ld 		(hl), a 					;; [2] Set firs byte of the pointer to 0
	ld 		d, h 						;; [1] /
	ld 		e, l 						;; [1] | DE = HL + 1 
	inc 	de 							;; [2] \ start copying 0 there
	ld      bc, #2*max_entities-1 		;; [3] we need to copy 2 bytes 
										;; 	   the first we have already
	ldir								;; [10*max_entities-1] Fill with 0's

		;; 	BEWARE;
		;; This function does not end here.
		;; requires man_entity_ai_getArrayHL to be 
		;; inmediately after to perform
		;; HL = &AI_Ptr_Array[0] and return
		;; not repeating code and not requiring a call or jump

		

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Man_Entity_AI::getArrayHL
;; 		Loads HL with the address where the pointer
;;		array starts and returns it
;; INPUT: -
;; DESTROYS: 	HL
;; RETURNS:
;;		HL: 	Pointer to the array of pointers (AI_Ptr_Array)
;;
man_entity_ai_getArrayHL::
	ld 		hl, #_ai_ptr_array 			;;  [3] HL = &AI_Ptr_Array[0]
	ret 								;; 	[3] Return


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Man_Entity_AI::Add
;; 		Adds a new AI entity to the Ptr Array
;;	INPUT: 
;; 		IX: 	Pointer to the new entity to be added
;;	DESTROYS: 	A, HL
;; 	RETURNS:
;;		HL: 	Pointer to the end of AI:Ptr_Array (&AI_Ptr_array[END])
;;

man_entity_ai_add::
	;; Store IX in AI_Ptr_Array[PEND]
	;; (First free elemen in the AI_Ptr_Array)
	ld 		hl, (_ai_ptr_pend) 			;; [5] HL = &AI_Ptr_Array[PEND]
	ld__a_ixl 							;; [2]
	ld 		(hl), a 					;; [2]
	inc 	hl 							;; [2] AI_Ptr_Array[END] = IX
	ld__a_ixh 							;; [2]
	ld 		(hl),a 						;; [2]

	;; Move AI_Ptr_pend + 2 bytes to point to the
	;; next avaliable element, which must be a nullptr
	inc  	hl 							;; [2]
	ld 		(_ai_ptr_pend), hl 			;; [5] AI_Ptr_PEnd += 2

	ret 								;; [3]