ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 1.
Hexadecimal [16-Bits]



                              1 ;;
                              2 ;; PATROL MANAGER
                              3 ;;
                              4 
                              5 
                              6 ;;
                     FFFFFFFF     7 patrol_invalid_move_x = -1
                              8 
                              9 .globl man_patrol_get
                             10 .globl man_patrol_init
                             11 
                             12 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                             13 ;; Defines a patrol name
                             14 .macro DefPatrol _name
                             15 	.globl _name
                             16 	_name::
                             17 .endm
                             18 
                             19 
                             20 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                             21 ;; Defines a Point intented to be  used on patrols.
                             22 .macro DefPoint _x, _y
                             23 	.db _x, _y
                             24 .endm
                             25 
                             26 
                             27 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                             28 ;; Ends a patrol by assigning invalid_move_x and 
                             29 ;; 		points back to the first patrol point
                             30 ;;
                             31 .macro EndPatrol _name
                             32 	.db  patrol_invalid_move_x
                             33 	.dw _name
                             34 .endm
