ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 1.
Hexadecimal [16-Bits]



                              1 ;;
                              2 ;; ENTITY COMPONENT
                              3 ;; 	1B Component-Type		(cmp type)
                              4 ;; 	2B Position				(x, y)
                              5 ;; 	2B Velocity				(vx, vy)
                              6 ;; 	2B Size					(sx, sy)
                              7 ;; 	2B Sprite Pointer		(pspr)
                              8 ;;	2B Last Video Pointer	(lastVP)
                              9 ;;	2B AI-AIM Target 		(ai_aim_x, ai_aim_y)
                             10 ;; 	1B AI-status 			(ai_st)
                             11 ;; 	1B AI-Previous-Status 	(ai_pre_st)
                             12 ;;  2B AI-Next-Patrol-Step 	(ai_patrol_step)
                             13 ;;
                             14 
                             15 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                             16 ;; Easily assign offsets to Entity_t members without error
                             17 ;; 		It adds sizes of types to an offset constant that assigns to members
                     0000    18 __off = 0
                             19 .macro DefOffset _size, _name
                             20 	_name = __off				;; Define _name constant of current offset
                             21 	__off = __off + _size     	;; Add sizeof( _name ) = size_to offset
                             22 .endm	
                             23 
                             24 
                             25 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                             26 ;; Easily define an enummeration starting in 0
                             27 ;; 		It adds sizes of types to an offset constant
                             28 ;;
                             29 .macro DefEnum 		_name
                             30 	_name'_offset = 0
                             31 .endm
                             32 
                             33 ;; 	Define enumeration element for an enumeration  name
                             34 .macro Enum _enumname, _element
                             35 	_enumname'_'_element = _enumname'_offset
                             36 	_enumname'_offset = _enumname'_offset + 1
                             37 .endm
                             38 
                             39 
                             40 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                             41 ;; Default constructor for Entity components
                             42 ;;
                             43 .macro DefineCmp_Entity_default
                             44 	DefineCmp_Entity 0, 0, 0, 0, e_w_invalidEntity, 1, e_cmp_default, 0x0000, e_ai_st_noAI
                             45 .endm
                             46 
                             47 
                             48 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                             49 ;;	Defines an Array of Entity_t with default values
                             50 ;;
                             51 .macro DefineCmpArray_Entity _N 
                             52 	.rept _N 
                             53 		DefineCmp_Entity_default
                             54 	.endm
                             55 .endm
ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 2.
Hexadecimal [16-Bits]



                             56 
                             57 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                             58 ;; Defines a New Entity_t_struxture
                             59 ;;		All Entity_t data together to simplify access, at the  cost of
                             60 ;;
                             61 
                             62 .macro DefineCmp_Entity _x, _y, _vx, _vy, _w, _h, _cmp_type, _pspr, _aist
                             63 	.narg __argn
                             64 	.if __argn - 9 	;; Validate parameter number
                             65 		.error 1
                             66 	.else 
                             67 		;;;;;; Type of component
                             68 		.db _cmp_type		;; Types of components the entity has
                             69 		;;;;;; CMP: Position
                             70 		.db  _x,  _y 	;; Position
                             71 		;;;;;; CMP: Velocity 
                             72 		.db _vx, _vy 	;; Velocity
                             73 		;;;;;; CMP: Render
                             74 		.db  _w,  _h 	;; Size
                             75 		.dw	 _pspr 		;; Sprite Pointer
                             76 		.dw 0xCCCC 		;; Last Video Memory Pointer Value (for restoring background)
                             77 						;; Default to 0xCCCC value to ease memory restoration.
                             78 		;;;;;; CMP: AI
                             79 		.db 0x00, 0x00  ;; AI-aim (ai_aim_x, ai_aim_y)
                             80 		.db _aist       ;; AI-status (ai_st)
                             81 		.db _aist       ;; AI-previous status (ai_prev_st)
                             82 		.dw nullptr		;; Next AI Patrol Step (ai_patrol_step)
                             83 	.endif
                             84 .endm
                             85 
                             86 ;;-----------------------------
                             87 ;;-- Entity_t offsets
                             88 ;;-----------------------------
   0000                      89 DefOffset 1, e_cmp_type				;; Component-Type 
                     0000     1 	e_cmp_type = __off				;; Define _name constant of current offset
                     0001     2 	__off = __off + 1     	;; Add sizeof( _name ) = size_to offset
                             90 
   0000                      91 DefOffset 1, e_x 	   				;; CMP: Position
                     0001     1 	e_x = __off				;; Define _name constant of current offset
                     0002     2 	__off = __off + 1     	;; Add sizeof( _name ) = size_to offset
   0000                      92 DefOffset 1, e_y 	   
                     0002     1 	e_y = __off				;; Define _name constant of current offset
                     0003     2 	__off = __off + 1     	;; Add sizeof( _name ) = size_to offset
                             93 
   0000                      94 DefOffset 1, e_vx 	   				;; CMP: Velocity
                     0003     1 	e_vx = __off				;; Define _name constant of current offset
                     0004     2 	__off = __off + 1     	;; Add sizeof( _name ) = size_to offset
   0000                      95 DefOffset 1, e_vy 	   
                     0004     1 	e_vy = __off				;; Define _name constant of current offset
                     0005     2 	__off = __off + 1     	;; Add sizeof( _name ) = size_to offset
                             96 
   0000                      97 DefOffset 1, e_w 	   				;; CMP: Render
                     0005     1 	e_w = __off				;; Define _name constant of current offset
                     0006     2 	__off = __off + 1     	;; Add sizeof( _name ) = size_to offset
   0000                      98 DefOffset 1, e_h 	   
ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 3.
Hexadecimal [16-Bits]



                     0006     1 	e_h = __off				;; Define _name constant of current offset
                     0007     2 	__off = __off + 1     	;; Add sizeof( _name ) = size_to offset
   0000                      99 DefOffset 1, e_pspr_l   
                     0007     1 	e_pspr_l = __off				;; Define _name constant of current offset
                     0008     2 	__off = __off + 1     	;; Add sizeof( _name ) = size_to offset
   0000                     100 DefOffset 1, e_pspr_h   
                     0008     1 	e_pspr_h = __off				;; Define _name constant of current offset
                     0009     2 	__off = __off + 1     	;; Add sizeof( _name ) = size_to offset
   0000                     101 DefOffset 1, e_lastVP_l
                     0009     1 	e_lastVP_l = __off				;; Define _name constant of current offset
                     000A     2 	__off = __off + 1     	;; Add sizeof( _name ) = size_to offset
   0000                     102 DefOffset 1, e_lastVP_h
                     000A     1 	e_lastVP_h = __off				;; Define _name constant of current offset
                     000B     2 	__off = __off + 1     	;; Add sizeof( _name ) = size_to offset
                            103 
   0000                     104 DefOffset 1, e_ai_aim_x 			;; CMP: AI
                     000B     1 	e_ai_aim_x = __off				;; Define _name constant of current offset
                     000C     2 	__off = __off + 1     	;; Add sizeof( _name ) = size_to offset
   0000                     105 DefOffset 1, e_ai_aim_y 
                     000C     1 	e_ai_aim_y = __off				;; Define _name constant of current offset
                     000D     2 	__off = __off + 1     	;; Add sizeof( _name ) = size_to offset
   0000                     106 DefOffset 1, e_ai_st    
                     000D     1 	e_ai_st = __off				;; Define _name constant of current offset
                     000E     2 	__off = __off + 1     	;; Add sizeof( _name ) = size_to offset
   0000                     107 DefOffset 1, e_ai_pre_st
                     000E     1 	e_ai_pre_st = __off				;; Define _name constant of current offset
                     000F     2 	__off = __off + 1     	;; Add sizeof( _name ) = size_to offset
   0000                     108 DefOffset 1, e_ai_patrol_step_l
                     000F     1 	e_ai_patrol_step_l = __off				;; Define _name constant of current offset
                     0010     2 	__off = __off + 1     	;; Add sizeof( _name ) = size_to offset
   0000                     109 DefOffset 1, e_ai_patrol_step_h
                     0010     1 	e_ai_patrol_step_h = __off				;; Define _name constant of current offset
                     0011     2 	__off = __off + 1     	;; Add sizeof( _name ) = size_to offset
                            110 
   0000                     111 DefOffset 0, sizeof_e				;; Total size of Entity Structure
                     0011     1 	sizeof_e = __off				;; Define _name constant of current offset
                     0011     2 	__off = __off + 0     	;; Add sizeof( _name ) = size_to offset
                            112 
                            113 
                            114 ;;-----------------------------
                            115 ;;-- Entity_t AI-status enum
                            116 ;;-----------------------------
   0000                     117 DefEnum e_ai_st
                     0000     1 	e_ai_st_offset = 0
   0000                     118 Enum e_ai_st noAI      
                     0000     1 	e_ai_st_noAI = e_ai_st_offset
                     0001     2 	e_ai_st_offset = e_ai_st_offset + 1
   0000                     119 Enum e_ai_st stand_by  
                     0001     1 	e_ai_st_stand_by = e_ai_st_offset
                     0002     2 	e_ai_st_offset = e_ai_st_offset + 1
   0000                     120 Enum e_ai_st move_to   
                     0002     1 	e_ai_st_move_to = e_ai_st_offset
                     0003     2 	e_ai_st_offset = e_ai_st_offset + 1
   0000                     121 Enum e_ai_st patrol 	 
                     0003     1 	e_ai_st_patrol = e_ai_st_offset
ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 4.
Hexadecimal [16-Bits]



                     0004     2 	e_ai_st_offset = e_ai_st_offset + 1
                            122 
                            123 
                            124 ;;-----------------------------
                            125 ;;-- Entity_t Component Types
                            126 ;;----------------------------- 
                     0001   127 e_cmp_AI 		= 0x01
                     0002   128 e_cmp_Render 	= 0x02
                     0004   129 e_cmp_Physics 	= 0x04
                     0006   130 e_cmp_default 	= e_cmp_Render | e_cmp_Physics
                            131 
                            132 ;;-----------------------------
                            133 ;; Entity_t status enum
                            134 ;;-----------------------------
                     00FF   135 e_w_invalidEntity = 0xFF	;; Entitity width = -1
                            136 
                            137 
                            138 ;;-----------------------------
                            139 ;;-- Utility definitions
                            140 ;;-----------------------------
                     0000   141 nullptr = 0x0000
