ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 1.
Hexadecimal [16-Bits]



                              1 ;;
                              2 ;; COMPONENT ARRAY DATA STRUCTURE
                              3 ;;
                              4 
                              5 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                              6 ;; Generate data structure and array of type-T components
                              7 ;;		Macro to generate all data structure required
                              8 ;;		to managa an array of components of the same type.
                              9 ;; It generates these labels.
                             10 ;;		* T_num:	A byte to count the number or elements in the array
                             11 ;;		* T_plast:	A word to store a pointer to the end of the array
                             12 ;;					This will be the first free element in the array
                             13 ;;		* T_array:	The array itself
                             14 ;; INPUTS:
                             15 ;;		_Tname:		Name of the component type
                             16 ;;		_N:			Size of the array in number of components (to reserve)
                             17 ;;		_DefineTypeMacroDefault:	Macro to be called to generate a de
                             18 
                             19 .macro DefineComponentArrayStructure _Tname, _N, _DefineTypeMacroDefault
                             20 	_Tname'_num:		.db 0				;; Number of defined elements
                             21 	_Tname'_pend:		.dw _Tname'_array	;; Pointer to the end of
                             22 	_Tname'_array:							;; Array of elements of
                             23 	.rept	_N
                             24 		_DefineTypeMacroDefault 			;; Repeatedly call default
                             25 	.endm
                             26 .endm
                             27 
                             28 
                             29 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                             30 ;; Same macro as before, but generating a bunch of zeros instead of
                             31 ;; It uses _ComponentSize instead of _DefineTypeMacroDefault to pro
                             32 ;; INPUTS:
                             33 ;;		Same as before except,
                             34 ;;		_ComponentSize: Size in bytes of T-component
                             35 ;;
                             36 .macro DefineComponentArrayStructure_size _Tname, _N, _ComponentSize
                             37 	_Tname'_num: 		.db 0				;; Number of defined elements
                             38 	_Tname'_pend:		.dw _Tname'array 	;; Pointer to the end of
                             39 	_Tname'_array:							;; Array of elements of
                             40 		.ds 	_N * _ComponentSize
                             41 .endm
                             42 
                             43 
                             44 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                             45 ;; Structure to store an array of pointers to AI
                             46 ;; end in a null pointer
                             47 ;; Always one pointer more than needed
                             48 ;; INPUTS:
                             49 ;;		Tname: 	Name of the component type
                             50 ;; 		N:		Maximum number of pointers to store
                             51 .macro DefineComponentPointersArrayStructure_Size _Tname, _N
                             52 	_Tname'_ptr_pend: .dw _Tname'_ptr_array
                             53 	_Tname'_ptr_array::
                             54 	.rept _N
                             55 		.dw 0x0000
ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 2.
Hexadecimal [16-Bits]



                             56 	.endm
                             57 .endm
