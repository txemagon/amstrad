ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 1.
Hexadecimal [16-Bits]



                              1 ;;
                              2 ;; AI CONTROL SYSTEM
                              3 ;;
                              4 
                              5 
                              6 ;;-----------------------------------------------------
                              7 ;;    Public Function Intrface
                              8 ;;-----------------------------------------------------
                              9 
                             10 .globl sys_ai_control_init
                             11 .globl sys_ai_control_update
