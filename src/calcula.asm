org &8000

	ld	a,(ix+4)
	cp	0
	jr	z,MathAdd
	cp	1
	jr	z,MathSub
	cp	2
	jr	z,MathMult
	cp	3
	jr	MathDiv
	ret

SaveResult:
	ld	l,(ix+6)
	ld	h,(ix+7)
	ld	(hl),a
	ret


MathSub:
	ld	a,(ix+2)
	ld	b,(ix+0)
	sub	b
	jr	SaveResult


MathAdd:
	ld	a,(ix+2)
	ld	b,(ix+0)
	add	b
	jr	SaveResult


MathMult:
	ld	a,(ix+0)
	cp	0
	jr	z,SaveResult
	ld	b,a
	ld	c,(ix+2)
	ld	a,0

MathMultAgain:
	add	c
	djnz	MathMultAgain
	jr	SaveResult


MathDiv:
	ld	a,(ix+0)
	cp	0
	jr	z,SaveResult
	ld	a,(ix+2)
	ld	d,0
	ld	b,(ix+0)

MathDivAgain:
	inc	d
	sub	b
	jp	nc,MathDivAgain
	dec	d
	ld	a,d
	jr	SaveResult



	
	