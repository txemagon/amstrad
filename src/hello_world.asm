PrintChar		equ &BB5A

	org	&1200

	ld	hl,Message
	call	PrintString

	ret

PrintString:
	ld	a,(hl)
	cp	255
	ret	z
	inc	hl
	call	PrintChar
	jr	PrintString

Message:	db 'The world is a vampire 123!',255

NewLIne:
	ld 	a,&0D
	call 	PrintChar
	ld	a,&0A
	jp	PrintChar