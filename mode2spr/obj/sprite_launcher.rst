ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 1.
Hexadecimal [16-Bits]



                              1 .include "artwork/general_sprites.h.s"
ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 2.
Hexadecimal [16-Bits]



                              1 .include "sprites/sprite.h.s"
                              1 ;;
                              2 ;; SPRITE DEFINITION
                              3 ;;
                              4 
                              5 
                              6 
                              7 
                              8 ;;-----------------------
                              9 ;;-- Define Entity
                             10 ;;-----------------------
                             11 
                             12 
                             13 ;; OFFS	LEN	 Definition
                             14 ;;  00	 1		Width. 		Number of bytes per scan line.
                             15 ;;	01	 1		Length.		Number of bytes that build the sprite up.
                             16 ;;	02	LEN 	Data.		Bytes of sprite.
                             17 
                     0000    18 __off = 0
                             19 
                             20 .macro DefOffset 	_size, _name
                             21 	_name = __off
                             22 	__off = __off + _size
                             23 .endm
                             24 
                             25 
                             26 
                             27 ;;-----------------------
                             28 ;;-- Sprite Offsets
                             29 ;;-----------------------
                             30 
   4025                      31 DefOffset	1, spr_l		;; Sprite length
                     0000     1 	spr_l = __off
                     0001     2 	__off = __off + 1
   0000                      32 DefOffset 	1, spr_w		;; Sprite width
                     0001     1 	spr_w = __off
                     0002     2 	__off = __off + 1
   0000                      33 DefOffset 	1, spr_h		;; Sprite height
                     0002     1 	spr_h = __off
                     0003     2 	__off = __off + 1
   0000                      34 DefOffset	1, spr_d		;; Sprite data
                     0003     1 	spr_d = __off
                     0004     2 	__off = __off + 1
                             35 
                             36 
                             37 
                             38 .macro SetLabel 	_name, _row
                             39 r'_row'_'_name::
                             40 .endm
                             41 
                             42 
                             43 ;;-----------------------
                             44 ;;-- Reserve room for
                             45 ;;	 shifted sprites
                             46 ;;-----------------------
ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 3.
Hexadecimal [16-Bits]



                             47 
                             48 .macro DefArray		_name, _rows _len
                             49 	__row = 0
                             50 	.rept _rows
                             51 		SetLabel	_name,\__row
                             52 		.blkb		_len
                             53 		__row = __row + 1
                             54 	.endm
                             55 
                             56 .endm
                             57 
                             58 ;;-----------------------
                             59 ;;-- Define Sprite
                             60 ;;
                             61 ;;	 Defines 3 labels:
                             62 ;;		- Sprite beginning. 				XXXX::
                             63 ;;		- Sprite original data beginning.	XXXX_src_bmp_ptr::
                             64 ;;		- Shifted sprite array.				XXXX_array:
                             65 ;;
                             66 ;;	Define 1 constant:
                             67 ;;		- sizeof_XXXX.	Holds the length of sprite (data + header)
                             68 ;;-----------------------
                             69 .macro DefSprite 	_name, _width, _len
                             70 
                             71 __height = (_len / _width)
                             72 __new_len = __height * (_width + 1)			;; Leave one extra byte per row
                             73 
                             74 _name'_array::
                             75 	DefArray	_name, 8, __new_len 				;; Define a row per shifted sprite
                             76 
                             77 _name::
                             78 	.db		_len, _width, __height
                             79    
                             80 	esz_'_name = __new_len
                             81     sz_'_name = _len + spr_d
                             82 
                             83 _name'_src_bmp_ptr::
                             84 
                             85 .endm
                             86 
ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 4.
Hexadecimal [16-Bits]



                              2 
                              3 
                              4 
                              5 ;;-------------------------------------------------------------------------
                              6 ;; SPRITE STRUCTURE
                              7 ;;		Sprite Data Definition
                              8 ;;			Byte length, Row width in bytes, data
                              9 ;;
                             10 ;;		Defined Sprite
                             11 ;;			length:		1B. 	Number of bytes in sprite data definition
                             12 ;; 			width: 		1B.		Number of bytes per row.
                             13 ;;			height		1B.		Number of rows.
                             14 ;;			data 		n 		Data definition.
                             15 ;;
                             16 ;;	SYMBOLS [offsets]:
                             17 ;;		spr_l: 	0 		Total length
                             18 ;;		spr_w:	1 		Line width
                             19 ;;		spr_h:	2 		Sprite height
                             20 ;;		spr_d:	3 		Data offset
                             21 
                             22 
                             23 
                             24 .globl spr1
                             25 .globl spr1_array
                             26 
                             27 .globl spr2
                             28 .globl spr2_array
ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 5.
Hexadecimal [16-Bits]



                              2 
ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 6.
Hexadecimal [16-Bits]



                              3 .include "sprite_launcher.h.s"
ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 7.
Hexadecimal [16-Bits]



                              1 .include "./sprites/render_sprite_m2.h.s"
                              1 
                              2 .globl print_spr
ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 8.
Hexadecimal [16-Bits]



                              2 
                              3 .globl sprite_launcher
                              4 
                     0020     5 X = 32
                     0017     6 Y = 23
ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 9.
Hexadecimal [16-Bits]



                              4 .include "sprites/right_justify.h.s"
                              1 .globl spr_justify
ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 10.
Hexadecimal [16-Bits]



                              5 .include "sprites/spr_pos.h.s"
                              1 .globl spr_addr
                              2 .globl spr_dspl
ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 11.
Hexadecimal [16-Bits]



                              6 .include "sprites/spr_shift.h.s"
                              1 .globl  spr_shift
ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 12.
Hexadecimal [16-Bits]



                              7 .include "sprites/find_arr_beginning.h.s"
                              1 .globl calc_len
                              2 .globl find_arr_beginning
ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 13.
Hexadecimal [16-Bits]



                              8 .include "sprites/render_sprite_m2.h.s"
                              1 
                              2 .globl print_spr
ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 14.
Hexadecimal [16-Bits]



                              9 
                             10 
   0000                      11 sprite_launcher:
                             12 
                             13 	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                             14 	;;
                             15 	;;Generate shifted sprites
                             16 	;;
                             17 
                             18 
                             19 	;; COPY FIRST ROW JUSTIFIED
   4025 DD 2A C1 40   [20]   20 	ld		ix, (sprite)				;; Original data
   4029 16 00         [ 7]   21 	ld  	d, #0
   402B DD 5E 00      [19]   22 	ld     	e, spr_l(ix)				;; E = SPR_LEN
   402E CD 30 41      [17]   23 	call	spr_justify					;; Justify sprite with a bigger width
                             24 										;; IY = Pointer to start of array
                             25 
                             26 
                             27 	;; SHIFT 1 BIT EVERY ROW
                             28 
                             29 	;; Get data length
   4031 DD 2A C1 40   [20]   30 	ld  	ix, (sprite)
   4035 CD C4 40      [17]   31 	call  	calc_len
   4038 16 00         [ 7]   32 	ld  	d, #0
   403A 5F            [ 4]   33 	ld  	e, a 						;; DE = extended length
   403B 32 C3 40      [13]   34 	ld      (__ext_len), a
                             35 
                             36 	;; Advance pointers to the end of data
   403E FD E5         [15]   37 	push    iy
   4040 DD E1         [14]   38 	pop  	ix 							;; IX = Start of Array
                             39 
                             40 
   4042 06 07         [ 7]   41 	ld  	b, #7  						;; Shift 7 rows (one doesn't need to be shifted)
   4044                      42 	__shift_next_row:
   4044 C5            [11]   43 		push    bc
   4045 FD 19         [15]   44 		add  	iy, de 					;; IY += row_length (next row)
                             45 
   4047 DD 19         [15]   46 		add  	ix, de					;; Point to data end
   4049 FD 19         [15]   47 		add  	iy, de
                             48 
   404B 43            [ 4]   49 		ld      b, e 					;; B = EXT_DATA_LEN
                             50 
   404C CD 95 41      [17]   51 		call  	spr_shift				;; Displace 1 bit to left data.
   404F DD 19         [15]   52 		add  	ix, de
   4051 C1            [10]   53 		pop  	bc
   4052 10 F0         [13]   54 	djnz  		__shift_next_row
                             55 
                             56 
                             57 
                             58 
   4054                      59 __calc:
                             60 	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                             61 	;;
                             62 	;; Calculate Memory address
                             63 	;;
ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 15.
Hexadecimal [16-Bits]



   4054 DD 21 BD 40   [14]   64 	ld		ix, #coord
   4058 CD 48 41      [17]   65 	call	spr_addr					;; Call spr_addr
                             66 										;; HL holds VMEM
                             67 
                             68 
                             69 
                             70 
                             71 	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                             72 	;;
                             73 	;; Paint sprite at HL address
                             74 	;;
   405B DD 2A C1 40   [20]   75 	ld		ix, (sprite) 				;; IX = Sprite Header
   405F DD E5         [15]   76 	push    ix
   4061 CD D6 40      [17]   77 	call    find_arr_beginning 			;; IY = Array beginning
                             78 
   4064 DD 21 BD 40   [14]   79 	ld		ix, #coord
   4068 CD 85 41      [17]   80 	call    spr_dspl					;; A = Displacement
   406B B7            [ 4]   81 	or 		a
   406C 28 0D         [12]   82 	jr  	z, __adv_end
   406E 16 00         [ 7]   83 	ld  	d, #0
   4070 F5            [11]   84 	push    af
   4071 3A C3 40      [13]   85 	ld  	a, (__ext_len)
   4074 5F            [ 4]   86 	ld  	e, a
   4075 F1            [10]   87 	pop  	af
   4076                      88 	__advance_shift:
   4076 FD 19         [15]   89 		add     iy, de
   4078 3D            [ 4]   90 		dec  	a
   4079 20 FB         [12]   91 	jr    	nz, __advance_shift
   407B                      92 __adv_end:
                             93 
                             94 
   407B FD E5         [15]   95 	push  	iy 							;; DE = Sprite Data
   407D D1            [10]   96 	pop   	de
   407E DD E1         [14]   97 	pop 	ix							;; IX = Sprite Header
   4080 CD E9 40      [17]   98 	call	print_spr					;; Print Sprite.
                             99 
                            100 
                            101 
                            102 
                            103 	;; Move
                            104 
                            105 	;ld  	a, (coordY)
                            106 	;inc     a
                            107 	;ld   	(coordY), a
   4083 3A BF 40      [13]  108 	ld  	a, (coordX)
   4086 C6 01         [ 7]  109 	add  	a, #1
   4088 32 BF 40      [13]  110 	ld   	(coordX), a
   408B 3A C0 40      [13]  111 	ld      a, (coordX + 1)
   408E CE 00         [ 7]  112 	adc     a, #0
   4090 32 C0 40      [13]  113 	ld   	(coordX+1), a
   4093 FE 02         [ 7]  114 	cp 		#2
   4095 20 23         [12]  115 	jr      nz, __still_line
                            116 
   4097                     117 __line_might_end:
   4097 3A C3 40      [13]  118 	ld   	a, (__ext_len)
ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 16.
Hexadecimal [16-Bits]



   409A 47            [ 4]  119 	ld      b, a
   409B 3E 80         [ 7]  120 	ld  	a, #0x80
   409D 90            [ 4]  121 	sub  	b
   409E 47            [ 4]  122 	ld   	b, a
   409F 3A BF 40      [13]  123 	ld  	a, (coordX)
   40A2 B8            [ 4]  124 	cp    	b   		;;0x280 = 640 (- width * 8)
   40A3 20 15         [12]  125 	jr  	nz, __still_line
   40A5                     126 __line_ends:
   40A5 3A BF 40      [13]  127 	ld   	a,(coordX)
   40A8 AF            [ 4]  128 	xor     a
   40A9 32 BF 40      [13]  129 	ld   	(coordX),a
   40AC 3A C0 40      [13]  130 	ld   	a,(coordX+1)
   40AF AF            [ 4]  131 	xor     a
   40B0 32 C0 40      [13]  132 	ld   	(coordX+1),a
   40B3 3A BD 40      [13]  133 	ld  	a, (coordY)
   40B6 3C            [ 4]  134 	inc     a
   40B7 32 BD 40      [13]  135 	ld   	(coordY), a
   40BA                     136 __still_line:
                            137 
   40BA 18 98         [12]  138 	jr 		__calc
                            139 
   40BC C9            [10]  140 	ret
                            141 
                            142 
                            143 
                            144 
                            145 
                            146 
   40BD                     147 coord:
   40BD 17 00               148 coordY:		.dw Y
   40BF 20 00               149 coordX:		.dw X
                            150 
   40C1 15 44               151 sprite:	.dw spr2
   40C3 00                  152 __ext_len:  .db 00
                            153 
                            154 
