ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 1.
Hexadecimal [16-Bits]



                     C000     1 VMEM = 0xC000
                              2 
                              3 
                              4 ;; Calculate address from x,y coords in mode 2 (doesn't matter mode at all).
                              5 
                              6 ;;  y  Mem     Offs  LnScr  LineSet 
                              7 ;; 00: &C000 - &0000 - 0   - 0
                              8 ;; 01: &C800 - &0800 - 1   - 0
                              9 ;; 02: &D000 - &1000 - 2   - 0
                             10 ;; 03: &D800 - &1800 - 3   - 0
                             11 ;; 04: &E000 - &2000 - 4   - 0
                             12 ;; 05: &E800 - &2800 - 5   - 0
                             13 ;; 06: &F000 - &3000 - 6   - 0
                             14 ;; 07: &F800 - &3800 - 7   - 0
                             15 ;; 08: &C050 - &0050 - 0   - 1
                             16 ;; 09: &C850 - &0850 - 1   - 1
                             17 ;; 10: &D050 - &1050 - 2   - 1
                             18 ;; 11: &D850 - &1850 - 3   - 1
                             19 ;; ...
                             20 ;;
                             21 ;; ADDRESS DE = VMEM + LINESCREEN * &800 + LINESET * &50
                             22 ;; LINESCREEN = Y % 8
                             23 ;; LINESET    = Y / 8
                             24 ;;
                             25 ;; DE = VMEM + (Y % 8) * &800 + (Y / 8) * &50
                             26 ;;
                             27 ;; DE = VMEM + (Y % 8) * &800 + (Y / 8) * (&40 + &10)
                             28 
                             29 
                             30 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                             31 ;; SPR_ADDR
                             32 ;; Calculate the byte address in which a coordinate
                             33 ;; x,y abides
                             34 ;;
                             35 ;; MODIFIES
                             36 ;; 	A,B,C,D,E, HL
                             37 ;;
                             38 ;; PARAMETERS
                             39 ;;	X Coordinate (Up to 640) (IX+2)
                             40 ;;	Y Coordinate (Up to 200) (IX+0)
                             41 ;;
                             42 ;; RETURNS
                             43 ;;	HL: Video memory
                             44 ;;
   4148                      45 spr_addr:
                             46 
                             47 	;; Pop X parameter
   4148 DD 4E 02      [19]   48 	ld	c, 2(ix)		;; Extract X coordinate
   414B DD 46 03      [19]   49 	ld	b, 3(ix)
                             50 
                             51 	;; X = X / 8
   414E CB 38         [ 8]   52 	srl	b
   4150 CB 19         [ 8]   53 	rr	c			;; x = x / 2
   4152 CB 38         [ 8]   54 	srl	b
   4154 CB 19         [ 8]   55 	rr	c			;; x = x / 4
ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 2.
Hexadecimal [16-Bits]



   4156 CB 38         [ 8]   56 	srl	b
   4158 CB 19         [ 8]   57 	rr	c			;; x = x / 8
                             58 
                             59 	;; DE = &C000 + x
   415A 11 00 C0      [10]   60 	ld	de,#VMEM	;; DE = &C000
   415D 7B            [ 4]   61 	ld	a,e			;; 
   415E 81            [ 4]   62 	add	c			;; E + LOW(x)
   415F 5F            [ 4]   63 	ld	e,a			;; E =E + x
   4160 8A            [ 4]   64 	adc	d			;; If ( E + x > &FF), add carry
   4161 93            [ 4]   65 	sub	e			;; A = E + x + D - ( E - x)
   4162 80            [ 4]   66 	add	b			;; A += HIGH(x)
   4163 57            [ 4]   67 	ld	d,a			;; DE += x
                             68 
                             69 
                             70 	;; Fetch Y parameter
   4164 DD 46 00      [19]   71 	ld	b,(ix)		;; Y is only 1 byte long
                             72 
                             73 	;; LINESCREEN
   4167 78            [ 4]   74 	ld	a,b			;; A = y
   4168 E6 F8         [ 7]   75 	and	#0xF8		;; A = (y / 8) * 8. Integer division.
   416A 6F            [ 4]   76 	ld	l,a
   416B 26 00         [ 7]   77 	ld	h,#0
   416D 29            [11]   78 	add	hl,hl		;; HL = (y / 8) * 16
   416E 44            [ 4]   79 	ld	b,h
   416F 4D            [ 4]   80 	ld	c,l			;; BC = HL = (y / 8) * 16
   4170 29            [11]   81 	add	hl,hl		;; HL = (y / 8) * 32
   4171 29            [11]   82 	add	hl,hl		;; HL = (y / 8) * 64
                             83 
                             84 	;; Add BC = (y / 8) * 16
   4172 7D            [ 4]   85 	ld 	a,l
   4173 81            [ 4]   86 	add	c
   4174 6F            [ 4]   87 	ld	l,a
   4175 8C            [ 4]   88 	adc	h			;; Carry when adding C.
   4176 95            [ 4]   89 	sub	l
   4177 80            [ 4]   90 	add	b			;; B holds carry from add hl, hl
   4178 67            [ 4]   91 	ld	h,a
                             92 
                             93 
                             94 	;; LINESET
   4179 DD 7E 00      [19]   95 	ld	a,0(ix)		;; A = y
   417C E6 07         [ 7]   96 	and	#0x07			;; A = y % 8
   417E 07            [ 4]   97 	rlca				;; A = (y % 8) * 2
   417F 07            [ 4]   98 	rlca				;; A = (y % 8) * 4
   4180 07            [ 4]   99 	rlca				;; A = (y % 8) * 8
   4181 84            [ 4]  100 	add	h			;; A = HL + A * &100
   4182 67            [ 4]  101 	ld	h,a			;; HL = (y % 8) * &800
                            102 
   4183 19            [11]  103 	add	hl,de			;; HL += x
                            104 
                            105 
   4184 C9            [10]  106 	ret
                            107 
                            108 
                            109 
                            110 	
ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 3.
Hexadecimal [16-Bits]



                            111 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                            112 ;; 	FUNC: 	spr_dspl
                            113 ;;
                            114 ;;	Calculate how many bits shall be shifted to the left
                            115 ;;
                            116 ;;  MODIFIES
                            117 ;;  	A, B
                            118 ;;
                            119 ;;	PARAMS
                            120 ;;		IX: Pointer to Y,X coordinate
                            121 ;;
                            122 ;;  RETURNS
                            123 ;; 		A: Positions to be displaced to the left (8 - RIGHT)
                            124 ;;
                            125 
   4185                     126 spr_dspl:
                            127 
   4185 DD 7E 02      [19]  128 	ld 		a, 2(ix)
   4188 E6 07         [ 7]  129 	and 	#0x07
   418A 47            [ 4]  130 	ld  	b, a
   418B 3E 08         [ 7]  131 	ld  	a, #0x08
   418D 90            [ 4]  132 	sub  	b
   418E FE 08         [ 7]  133 	cp      #8
   4190 20 02         [12]  134 	jr		nz, __end
   4192 D6 08         [ 7]  135 	sub 	#8
                            136 
   4194                     137 __end:
   4194 C9            [10]  138 	ret
