ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 1.
Hexadecimal [16-Bits]



                              1 
                              2 ;;-----------------------------------------------
                              3 ;; FUNC:  spr_shift
                              4 ;; 		  Shift 1 bit to the left every width
                              5 ;;
                              6 ;;  MODIFIES
                              7 ;;		 B, IX, IY
                              8 ;;
                              9 ;;	PARAMS
                             10 ;;		IX = Source data end (next position)
                             11 ;; 		IY = Destiny end (next position)
                             12 ;;		 B = Data length
                             13 
   4195                      14 spr_shift:
   4195 DD 2B         [10]   15 		dec 	ix
   4197 FD 2B         [10]   16 		dec  	iy
   4199 DD 7E 00      [19]   17 		ld  	a, 0(ix)
   419C CB 17         [ 8]   18 		rl  	a
   419E FD 77 00      [19]   19 		ld 		0(iy), a
                             20 
   41A1 10 F2         [13]   21 	djnz  		spr_shift
                             22 
   41A3 C9            [10]   23 	ret
