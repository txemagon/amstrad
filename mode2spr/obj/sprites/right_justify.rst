ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 1.
Hexadecimal [16-Bits]



                              1 .include "artwork/general_sprites.h.s"
ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 2.
Hexadecimal [16-Bits]



                              1 .include "sprites/sprite.h.s"
                              1 ;;
                              2 ;; SPRITE DEFINITION
                              3 ;;
                              4 
                              5 
                              6 
                              7 
                              8 ;;-----------------------
                              9 ;;-- Define Entity
                             10 ;;-----------------------
                             11 
                             12 
                             13 ;; OFFS	LEN	 Definition
                             14 ;;  00	 1		Width. 		Number of bytes per scan line.
                             15 ;;	01	 1		Length.		Number of bytes that build the sprite up.
                             16 ;;	02	LEN 	Data.		Bytes of sprite.
                             17 
                     0000    18 __off = 0
                             19 
                             20 .macro DefOffset 	_size, _name
                             21 	_name = __off
                             22 	__off = __off + _size
                             23 .endm
                             24 
                             25 
                             26 
                             27 ;;-----------------------
                             28 ;;-- Sprite Offsets
                             29 ;;-----------------------
                             30 
   410A                      31 DefOffset	1, spr_l		;; Sprite length
                     0000     1 	spr_l = __off
                     0001     2 	__off = __off + 1
   0000                      32 DefOffset 	1, spr_w		;; Sprite width
                     0001     1 	spr_w = __off
                     0002     2 	__off = __off + 1
   0000                      33 DefOffset 	1, spr_h		;; Sprite height
                     0002     1 	spr_h = __off
                     0003     2 	__off = __off + 1
   0000                      34 DefOffset	1, spr_d		;; Sprite data
                     0003     1 	spr_d = __off
                     0004     2 	__off = __off + 1
                             35 
                             36 
                             37 
                             38 .macro SetLabel 	_name, _row
                             39 r'_row'_'_name::
                             40 .endm
                             41 
                             42 
                             43 ;;-----------------------
                             44 ;;-- Reserve room for
                             45 ;;	 shifted sprites
                             46 ;;-----------------------
ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 3.
Hexadecimal [16-Bits]



                             47 
                             48 .macro DefArray		_name, _rows _len
                             49 	__row = 0
                             50 	.rept _rows
                             51 		SetLabel	_name,\__row
                             52 		.blkb		_len
                             53 		__row = __row + 1
                             54 	.endm
                             55 
                             56 .endm
                             57 
                             58 ;;-----------------------
                             59 ;;-- Define Sprite
                             60 ;;
                             61 ;;	 Defines 3 labels:
                             62 ;;		- Sprite beginning. 				XXXX::
                             63 ;;		- Sprite original data beginning.	XXXX_src_bmp_ptr::
                             64 ;;		- Shifted sprite array.				XXXX_array:
                             65 ;;
                             66 ;;	Define 1 constant:
                             67 ;;		- sizeof_XXXX.	Holds the length of sprite (data + header)
                             68 ;;-----------------------
                             69 .macro DefSprite 	_name, _width, _len
                             70 
                             71 __height = (_len / _width)
                             72 __new_len = __height * (_width + 1)			;; Leave one extra byte per row
                             73 
                             74 _name'_array::
                             75 	DefArray	_name, 8, __new_len 				;; Define a row per shifted sprite
                             76 
                             77 _name::
                             78 	.db		_len, _width, __height
                             79    
                             80 	esz_'_name = __new_len
                             81     sz_'_name = _len + spr_d
                             82 
                             83 _name'_src_bmp_ptr::
                             84 
                             85 .endm
                             86 
ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 4.
Hexadecimal [16-Bits]



                              2 
                              3 
                              4 
                              5 ;;-------------------------------------------------------------------------
                              6 ;; SPRITE STRUCTURE
                              7 ;;		Sprite Data Definition
                              8 ;;			Byte length, Row width in bytes, data
                              9 ;;
                             10 ;;		Defined Sprite
                             11 ;;			length:		1B. 	Number of bytes in sprite data definition
                             12 ;; 			width: 		1B.		Number of bytes per row.
                             13 ;;			height		1B.		Number of rows.
                             14 ;;			data 		n 		Data definition.
                             15 ;;
                             16 ;;	SYMBOLS [offsets]:
                             17 ;;		spr_l: 	0 		Total length
                             18 ;;		spr_w:	1 		Line width
                             19 ;;		spr_h:	2 		Sprite height
                             20 ;;		spr_d:	3 		Data offset
                             21 
                             22 
                             23 
                             24 .globl spr1
                             25 .globl spr1_array
                             26 
                             27 .globl spr2
                             28 .globl spr2_array
ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 5.
Hexadecimal [16-Bits]



                              2 .include "sprites/find_arr_beginning.h.s"
                              1 .globl calc_len
                              2 .globl find_arr_beginning
ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 6.
Hexadecimal [16-Bits]



                              3 
                              4 ;;------------------------------------------------------------------
                              5 ;; FUNC: right_justify
                              6 ;;	Pad with 0's on left hand side (leave memory with garbage)
                              7 ;;
                              8 ;;	PARAMS
                              9 ;; 		IX = Points to source
                             10 ;; 		IY = Destiny
                             11 ;;		 L = Final width
                             12 ;;		BC = Height + Original width
                             13 
   0000                      14 right_justify:
                             15 
                             16 
                             17 	;;---------------------
                             18 	;; PREPROTOCOL
                             19 	;;
                             20 
                             21 	;; Save registers
   410A DD E5         [15]   22 	push  	ix
   410C FD E5         [15]   23 	push  	iy
   410E E5            [11]   24 	push    hl
   410F C5            [11]   25 	push    bc
                             26 
                             27 
                             28 	;; Calculate pad size
   4110 7D            [ 4]   29 	ld   	a, l
   4111 91            [ 4]   30 	sub 	c
   4112 6F            [ 4]   31 	ld		l, a  						;; L = PAD_SIZE
                             32 
                             33 
                             34 	;;---------------------
                             35 	;; LOOP
                             36 	;;
   4113                      37 	_next_row:
                             38 
   4113 C5            [11]   39 		push  	bc						;; Save Height
                             40 
                             41 
                             42 		;; Pad
                             43 
   4114 45            [ 4]   44 		ld  	b, l  					;; REPEAT B = Pad Size
   4115                      45 		_start_pad:
   4115 FD 23         [10]   46 			inc  	iy 					;; inc IY pad size
   4117 10 FC         [13]   47 		djnz  		_start_pad
                             48 
                             49 
                             50 
                             51 
                             52 		;; Copy original width from HL to DE
                             53 
   4119 41            [ 4]   54 		ld  	b, c 					;; REPEAT B = Original Width
   411A                      55 		_start_data:
   411A DD 7E 00      [19]   56 			ld 		a, 0(ix)
   411D FD 77 00      [19]   57 			ld  	0(iy), a 			;; (IY) = (IX)
ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 7.
Hexadecimal [16-Bits]



   4120 DD 23         [10]   58 			inc  	ix
   4122 FD 23         [10]   59 			inc  	iy
   4124 10 F4         [13]   60 		djnz  		_start_data
                             61 
                             62 
                             63 
   4126 C1            [10]   64 		pop		bc 						;; B = Height
   4127 10 EA         [13]   65 	djnz  	_next_row
                             66 
                             67 
                             68 
                             69 	;;---------------------
                             70 	;; POSTPROTOCOL
                             71 	;;
   4129 C1            [10]   72 	pop  	bc
   412A E1            [10]   73 	pop  	hl
   412B FD E1         [14]   74 	pop  	iy
   412D DD E1         [14]   75 	pop  	ix
                             76 
   412F C9            [10]   77 	ret
                             78 
                             79 
                             80 
                             81 
                             82 ;;------------------------------------------------------------------
                             83 ;; FUNC:  	spr_justify
                             84 ;; 			Rights justifies sprite to width + 1
                             85 ;;	PARAMS
                             86 ;; 		IX = Points to source
                             87 
                             88 
   4130                      89 spr_justify::
                             90 
   4130 CD D6 40      [17]   91 	call  	find_arr_beginning			;; IY = Pointer to start of Array
   4133 FD E5         [15]   92 	push   	iy 							;; Store Array start
                             93 
                             94 	;; ld      iy, #spr1_array  		;; Justified data
                             95 
                             96 
                             97 
                             98 
   4135 DD 7E 01      [19]   99 	ld  	a, spr_w(ix)  				
   4138 4F            [ 4]  100 	ld  	c, a  						;; C = Width
   4139 3C            [ 4]  101 	inc  	a 
   413A 6F            [ 4]  102 	ld  	l, a 						;; L = Width + 1
   413B DD 46 02      [19]  103 	ld  	b, spr_h(ix) 				;; B = Height
   413E 11 03 00      [10]  104 	ld   	de,#spr_d 
   4141 DD 19         [15]  105 	add  	ix, de 						;; ix points to source data
                            106 
   4143 CD 0A 41      [17]  107 	call	right_justify
                            108 
   4146 D1            [10]  109 	pop 	de 							;; return in DE array start
   4147 C9            [10]  110 	ret
                            111 
