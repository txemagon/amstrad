ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 1.
Hexadecimal [16-Bits]



                              1 ;;
                              2 ;; SPRITE DEFINITION
                              3 ;;
                              4 
                              5 
                              6 
                              7 
                              8 ;;-----------------------
                              9 ;;-- Define Entity
                             10 ;;-----------------------
                             11 
                             12 
                             13 ;; OFFS	LEN	 Definition
                             14 ;;  00	 1		Width. 		Number of bytes per scan line.
                             15 ;;	01	 1		Length.		Number of bytes that build the sprite up.
                             16 ;;	02	LEN 	Data.		Bytes of sprite.
                             17 
                     0000    18 __off = 0
                             19 
                             20 .macro DefOffset 	_size, _name
                             21 	_name = __off
                             22 	__off = __off + _size
                             23 .endm
                             24 
                             25 
                             26 
                             27 ;;-----------------------
                             28 ;;-- Sprite Offsets
                             29 ;;-----------------------
                             30 
   0000                      31 DefOffset	1, spr_l		;; Sprite length
                     0000     1 	spr_l = __off
                     0001     2 	__off = __off + 1
   0000                      32 DefOffset 	1, spr_w		;; Sprite width
                     0001     1 	spr_w = __off
                     0002     2 	__off = __off + 1
   0000                      33 DefOffset 	1, spr_h		;; Sprite height
                     0002     1 	spr_h = __off
                     0003     2 	__off = __off + 1
   0000                      34 DefOffset	1, spr_d		;; Sprite data
                     0003     1 	spr_d = __off
                     0004     2 	__off = __off + 1
                             35 
                             36 
                             37 
                             38 .macro SetLabel 	_name, _row
                             39 r'_row'_'_name::
                             40 .endm
                             41 
                             42 
                             43 ;;-----------------------
                             44 ;;-- Reserve room for
                             45 ;;	 shifted sprites
                             46 ;;-----------------------
                             47 
ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 2.
Hexadecimal [16-Bits]



                             48 .macro DefArray		_name, _rows _len
                             49 	__row = 0
                             50 	.rept _rows
                             51 		SetLabel	_name,\__row
                             52 		.blkb		_len
                             53 		__row = __row + 1
                             54 	.endm
                             55 
                             56 .endm
                             57 
                             58 ;;-----------------------
                             59 ;;-- Define Sprite
                             60 ;;
                             61 ;;	 Defines 3 labels:
                             62 ;;		- Sprite beginning. 				XXXX::
                             63 ;;		- Sprite original data beginning.	XXXX_src_bmp_ptr::
                             64 ;;		- Shifted sprite array.				XXXX_array:
                             65 ;;
                             66 ;;	Define 1 constant:
                             67 ;;		- sizeof_XXXX.	Holds the length of sprite (data + header)
                             68 ;;-----------------------
                             69 .macro DefSprite 	_name, _width, _len
                             70 
                             71 __height = (_len / _width)
                             72 __new_len = __height * (_width + 1)			;; Leave one extra byte per row
                             73 
                             74 _name'_array::
                             75 	DefArray	_name, 8, __new_len 				;; Define a row per shifted sprite
                             76 
                             77 _name::
                             78 	.db		_len, _width, __height
                             79    
                             80 	esz_'_name = __new_len
                             81     sz_'_name = _len + spr_d
                             82 
                             83 _name'_src_bmp_ptr::
                             84 
                             85 .endm
                             86 
