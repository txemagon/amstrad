ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 1.
Hexadecimal [16-Bits]



                              1 .include "artwork/general_sprites.h.s"
ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 2.
Hexadecimal [16-Bits]



                              1 .include "sprites/sprite.h.s"
                              1 ;;
                              2 ;; SPRITE DEFINITION
                              3 ;;
                              4 
                              5 
                              6 
                              7 
                              8 ;;-----------------------
                              9 ;;-- Define Entity
                             10 ;;-----------------------
                             11 
                             12 
                             13 ;; OFFS	LEN	 Definition
                             14 ;;  00	 1		Width. 		Number of bytes per scan line.
                             15 ;;	01	 1		Length.		Number of bytes that build the sprite up.
                             16 ;;	02	LEN 	Data.		Bytes of sprite.
                             17 
                     0000    18 __off = 0
                             19 
                             20 .macro DefOffset 	_size, _name
                             21 	_name = __off
                             22 	__off = __off + _size
                             23 .endm
                             24 
                             25 
                             26 
                             27 ;;-----------------------
                             28 ;;-- Sprite Offsets
                             29 ;;-----------------------
                             30 
   40C4                      31 DefOffset	1, spr_l		;; Sprite length
                     0000     1 	spr_l = __off
                     0001     2 	__off = __off + 1
   0000                      32 DefOffset 	1, spr_w		;; Sprite width
                     0001     1 	spr_w = __off
                     0002     2 	__off = __off + 1
   0000                      33 DefOffset 	1, spr_h		;; Sprite height
                     0002     1 	spr_h = __off
                     0003     2 	__off = __off + 1
   0000                      34 DefOffset	1, spr_d		;; Sprite data
                     0003     1 	spr_d = __off
                     0004     2 	__off = __off + 1
                             35 
                             36 
                             37 
                             38 .macro SetLabel 	_name, _row
                             39 r'_row'_'_name::
                             40 .endm
                             41 
                             42 
                             43 ;;-----------------------
                             44 ;;-- Reserve room for
                             45 ;;	 shifted sprites
                             46 ;;-----------------------
ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 3.
Hexadecimal [16-Bits]



                             47 
                             48 .macro DefArray		_name, _rows _len
                             49 	__row = 0
                             50 	.rept _rows
                             51 		SetLabel	_name,\__row
                             52 		.blkb		_len
                             53 		__row = __row + 1
                             54 	.endm
                             55 
                             56 .endm
                             57 
                             58 ;;-----------------------
                             59 ;;-- Define Sprite
                             60 ;;
                             61 ;;	 Defines 3 labels:
                             62 ;;		- Sprite beginning. 				XXXX::
                             63 ;;		- Sprite original data beginning.	XXXX_src_bmp_ptr::
                             64 ;;		- Shifted sprite array.				XXXX_array:
                             65 ;;
                             66 ;;	Define 1 constant:
                             67 ;;		- sizeof_XXXX.	Holds the length of sprite (data + header)
                             68 ;;-----------------------
                             69 .macro DefSprite 	_name, _width, _len
                             70 
                             71 __height = (_len / _width)
                             72 __new_len = __height * (_width + 1)			;; Leave one extra byte per row
                             73 
                             74 _name'_array::
                             75 	DefArray	_name, 8, __new_len 				;; Define a row per shifted sprite
                             76 
                             77 _name::
                             78 	.db		_len, _width, __height
                             79    
                             80 	esz_'_name = __new_len
                             81     sz_'_name = _len + spr_d
                             82 
                             83 _name'_src_bmp_ptr::
                             84 
                             85 .endm
                             86 
ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 4.
Hexadecimal [16-Bits]



                              2 
                              3 
                              4 
                              5 ;;-------------------------------------------------------------------------
                              6 ;; SPRITE STRUCTURE
                              7 ;;		Sprite Data Definition
                              8 ;;			Byte length, Row width in bytes, data
                              9 ;;
                             10 ;;		Defined Sprite
                             11 ;;			length:		1B. 	Number of bytes in sprite data definition
                             12 ;; 			width: 		1B.		Number of bytes per row.
                             13 ;;			height		1B.		Number of rows.
                             14 ;;			data 		n 		Data definition.
                             15 ;;
                             16 ;;	SYMBOLS [offsets]:
                             17 ;;		spr_l: 	0 		Total length
                             18 ;;		spr_w:	1 		Line width
                             19 ;;		spr_h:	2 		Sprite height
                             20 ;;		spr_d:	3 		Data offset
                             21 
                             22 
                             23 
                             24 .globl spr1
                             25 .globl spr1_array
                             26 
                             27 .globl spr2
                             28 .globl spr2_array
ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 5.
Hexadecimal [16-Bits]



                              2 
                              3 
                              4 
                              5 ;;---------------------------------------------------
                              6 ;; 	FUNC: 	Return in IY where array starts
                              7 ;;
                              8 ;;  MODIFIES
                              9 ;;		A, B, C, E
                             10 ;;
                             11 ;;  PARAMS
                             12 ;; 		IX: Pointer to header of sprite
                             13 ;;
                             14 ;;	RETURNS
                             15 ;;		A: 	New length
                             16 
   0000                      17 calc_len:
                             18 	;; A = (len + len / width)
   40C4 DD 7E 00      [19]   19 	ld     	a, spr_l(ix)
   40C7 5F            [ 4]   20 	ld  	e, a  						;; E = DATA_LEN
   40C8 DD 4E 01      [19]   21 	ld  	c, spr_w(ix)
   40CB 06 00         [ 7]   22 	ld      b, #0  						;; B = Quotient (len / width)
                             23 
   40CD                      24 	_div_width:
   40CD 04            [ 4]   25 		inc  	b
   40CE 91            [ 4]   26 		sub		c
   40CF D2 CD 40      [10]   27 	jp      nc, _div_width
   40D2 05            [ 4]   28 	dec   	b
                             29 
   40D3 78            [ 4]   30 	ld   	a, b 				
   40D4 83            [ 4]   31 	add  	e 							;; A = len + len / width
                             32 
   40D5 C9            [10]   33 	ret
                             34 
                             35 
                             36 ;;---------------------------------------------------
                             37 ;; 	FUNC: 	Return in IY where array starts
                             38 ;;
                             39 ;;
                             40 ;;  MODIFIES
                             41 ;;		A, B, C, D, E, IY
                             42 ;;
                             43 ;; 	PARAMS
                             44 ;; 		IX: Pointer to header of sprite
                             45 ;;
                             46 ;;	RETURNS
                             47 ;;		IY: Pointer to start of array.
                             48 
   40D6                      49 find_arr_beginning:
                             50 	;; IY = IX - 8 * (len + len / width)
   40D6 CD C4 40      [17]   51 	call  	calc_len  					;; A = (len + len / width)
   40D9 ED 44         [ 8]   52 	neg
                             53 
   40DB 5F            [ 4]   54 	ld  	e, a
   40DC 16 FF         [ 7]   55 	ld  	d,#0xFF						;; DE = - (len + len / width)
                             56 
ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 6.
Hexadecimal [16-Bits]



                             57 
   40DE DD E5         [15]   58 	push  	ix
   40E0 FD E1         [14]   59 	pop     iy
   40E2 06 08         [ 7]   60 	ld  	b, #8
                             61 
   40E4                      62 	_upstream_iy:						;; IY = IX - 8 * (len + len / width)
   40E4 FD 19         [15]   63 		add		iy, de
   40E6 10 FC         [13]   64 	djnz 		_upstream_iy
                             65 
   40E8 C9            [10]   66 	ret
