ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 1.
Hexadecimal [16-Bits]



                              1 .include "artwork/general_sprites.h.s"
ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 2.
Hexadecimal [16-Bits]



                              1 .include "sprites/sprite.h.s"
                              1 ;;
                              2 ;; SPRITE DEFINITION
                              3 ;;
                              4 
                              5 
                              6 
                              7 
                              8 ;;-----------------------
                              9 ;;-- Define Entity
                             10 ;;-----------------------
                             11 
                             12 
                             13 ;; OFFS	LEN	 Definition
                             14 ;;  00	 1		Width. 		Number of bytes per scan line.
                             15 ;;	01	 1		Length.		Number of bytes that build the sprite up.
                             16 ;;	02	LEN 	Data.		Bytes of sprite.
                             17 
                     0000    18 __off = 0
                             19 
                             20 .macro DefOffset 	_size, _name
                             21 	_name = __off
                             22 	__off = __off + _size
                             23 .endm
                             24 
                             25 
                             26 
                             27 ;;-----------------------
                             28 ;;-- Sprite Offsets
                             29 ;;-----------------------
                             30 
   42B9                      31 DefOffset	1, spr_l		;; Sprite length
                     0000     1 	spr_l = __off
                     0001     2 	__off = __off + 1
   42B9                      32 DefOffset 	1, spr_w		;; Sprite width
                     0001     1 	spr_w = __off
                     0002     2 	__off = __off + 1
   42B9                      33 DefOffset 	1, spr_h		;; Sprite height
                     0002     1 	spr_h = __off
                     0003     2 	__off = __off + 1
   42CB                      34 DefOffset	1, spr_d		;; Sprite data
                     0003     1 	spr_d = __off
                     0004     2 	__off = __off + 1
                             35 
                             36 
                             37 
                             38 .macro SetLabel 	_name, _row
                             39 r'_row'_'_name::
                             40 .endm
                             41 
                             42 
                             43 ;;-----------------------
                             44 ;;-- Reserve room for
                             45 ;;	 shifted sprites
                             46 ;;-----------------------
ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 3.
Hexadecimal [16-Bits]



                             47 
                             48 .macro DefArray		_name, _rows _len
                             49 	__row = 0
                             50 	.rept _rows
                             51 		SetLabel	_name,\__row
                             52 		.blkb		_len
                             53 		__row = __row + 1
                             54 	.endm
                             55 
                             56 .endm
                             57 
                             58 ;;-----------------------
                             59 ;;-- Define Sprite
                             60 ;;
                             61 ;;	 Defines 3 labels:
                             62 ;;		- Sprite beginning. 				XXXX::
                             63 ;;		- Sprite original data beginning.	XXXX_src_bmp_ptr::
                             64 ;;		- Shifted sprite array.				XXXX_array:
                             65 ;;
                             66 ;;	Define 1 constant:
                             67 ;;		- sizeof_XXXX.	Holds the length of sprite (data + header)
                             68 ;;-----------------------
                             69 .macro DefSprite 	_name, _width, _len
                             70 
                             71 __height = (_len / _width)
                             72 __new_len = __height * (_width + 1)			;; Leave one extra byte per row
                             73 
                             74 _name'_array::
                             75 	DefArray	_name, 8, __new_len 				;; Define a row per shifted sprite
                             76 
                             77 _name::
                             78 	.db		_len, _width, __height
                             79    
                             80 	esz_'_name = __new_len
                             81     sz_'_name = _len + spr_d
                             82 
                             83 _name'_src_bmp_ptr::
                             84 
                             85 .endm
                             86 
ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 4.
Hexadecimal [16-Bits]



                              2 
                              3 
                              4 
                              5 ;;-------------------------------------------------------------------------
                              6 ;; SPRITE STRUCTURE
                              7 ;;		Sprite Data Definition
                              8 ;;			Byte length, Row width in bytes, data
                              9 ;;
                             10 ;;		Defined Sprite
                             11 ;;			length:		1B. 	Number of bytes in sprite data definition
                             12 ;; 			width: 		1B.		Number of bytes per row.
                             13 ;;			height		1B.		Number of rows.
                             14 ;;			data 		n 		Data definition.
                             15 ;;
                             16 ;;	SYMBOLS [offsets]:
                             17 ;;		spr_l: 	0 		Total length
                             18 ;;		spr_w:	1 		Line width
                             19 ;;		spr_h:	2 		Sprite height
                             20 ;;		spr_d:	3 		Data offset
                             21 
                             22 
                             23 
                             24 .globl spr1
                             25 .globl spr1_array
                             26 
                             27 .globl spr2
                             28 .globl spr2_array
ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 5.
Hexadecimal [16-Bits]



                              2 
                              3 .area _DATA
                              4 
                              5 ;;-------------------------------------------------------------------------
                              6 ;; MACRO DEFINED DATA
                              7 ;;
                              8 ;;	LABELS:
                              9 ;;		spr1_array::			8 rows with 8 shifted sprites. Only data.
                             10 ;;								One extra byte every width.
                             11 ;;
                             12 ;;		spr1::					Original data (header + data)
                             13 ;;		spr1_src_bmp_ptr::		Pointer to original data
                             14 ;;
                             15 ;;	SYMBOLS:
                             16 ;;		esz_spr1 		= 		extend sizeof. 	0x10 ( 8 rows of 1 byte => 8 rows of (1+1) bytes)	
                             17 ;; 		sz_spr1 		= 		sizeof.			0x0A (header: 2, data: 8)
                             18 ;;
                             19 
                     0009    20 __sprite_len = __spr1_end - __spr1_strt
   42CB                      21 DefSprite		spr1, 1, __sprite_len
                              1 
                     0009     2 __height = (__sprite_len / 1)
                     0012     3 __new_len = __height * (1 + 1)			;; Leave one extra byte per row
                              4 
   42DD                       5 spr1_array::
   42DD                       6 	DefArray	spr1, 8, __new_len 				;; Define a row per shifted sprite
                     0000     1 	__row = 0
                              2 	.rept 8
                              3 		SetLabel	spr1,\__row
                              4 		.blkb		__new_len
                              5 		__row = __row + 1
                              6 	.endm
   42EF                       1 		SetLabel	spr1,\__row
   42EF                       1 r0_spr1::
   4301                       2 		.blkb		__new_len
                     0001     3 		__row = __row + 1
   4301                       1 		SetLabel	spr1,\__row
   4313                       1 r1_spr1::
   4313                       2 		.blkb		__new_len
                     0002     3 		__row = __row + 1
   4325                       1 		SetLabel	spr1,\__row
   4325                       1 r2_spr1::
   4337                       2 		.blkb		__new_len
                     0003     3 		__row = __row + 1
   4337                       1 		SetLabel	spr1,\__row
   4349                       1 r3_spr1::
   0036                       2 		.blkb		__new_len
                     0004     3 		__row = __row + 1
   0048                       1 		SetLabel	spr1,\__row
   0048                       1 r4_spr1::
   0048                       2 		.blkb		__new_len
                     0005     3 		__row = __row + 1
   005A                       1 		SetLabel	spr1,\__row
   005A                       1 r5_spr1::
   005A                       2 		.blkb		__new_len
ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 6.
Hexadecimal [16-Bits]



                     0006     3 		__row = __row + 1
   006C                       1 		SetLabel	spr1,\__row
   006C                       1 r6_spr1::
   006C                       2 		.blkb		__new_len
                     0007     3 		__row = __row + 1
   007E                       1 		SetLabel	spr1,\__row
   007E                       1 r7_spr1::
   007E                       2 		.blkb		__new_len
                     0008     3 		__row = __row + 1
                              7 
                              7 
   0090                       8 spr1::
   4349 09 01 09              9 	.db		__sprite_len, 1, __height
                             10    
                     0012    11 	esz_spr1 = __new_len
                     000C    12     sz_spr1 = __sprite_len + spr_d
                             13 
   434C                      14 spr1_src_bmp_ptr::
                             15 
   434C                      22 __spr1_strt:
   434C 00 18 3C 7E DB FF    23 	.db		0x00,0x18,0x3C,0x7E,0xDB,0xFF,0x5A,0x81,0x42
        5A 81 42
   4355                      24 __spr1_end:
                             25 
                             26 
                             27 
                             28 
                             29 ;;-------------------------------------------------------------------------
                             30 ;; MACRO DEFINED DATA
                             31 ;;
                             32 ;;	LABELS:
                             33 ;;		spr2_array::			8 rows with 8 shifted sprites. Only data.
                             34 ;;								One extra byte every width.
                             35 ;;
                             36 ;;		spr2::					Original data (header + data)
                             37 ;;		spr2_src_bmp_ptr::		Pointer to original data
                             38 ;;
                             39 ;;	SYMBOLS:
                             40 ;;		esz_spr2 		= 		extend sizeof. 	0x15 ( 7 rows of 2 byte => 7 rows of (2+1) bytes)	
                             41 ;; 		sz_spr2 		= 		sizeof.			0x10 (header: 2, data: 14)
                             42 ;;
                             43 
                     0010    44 __sprite_len = __spr2_end - __spr2_strt
   4355                      45 DefSprite		spr2, 2, __sprite_len
                              1 
                     0008     2 __height = (__sprite_len / 2)
                     0018     3 __new_len = __height * (2 + 1)			;; Leave one extra byte per row
                              4 
   4355                       5 spr2_array::
   4355                       6 	DefArray	spr2, 8, __new_len 				;; Define a row per shifted sprite
                     0000     1 	__row = 0
                              2 	.rept 8
                              3 		SetLabel	spr2,\__row
                              4 		.blkb		__new_len
                              5 		__row = __row + 1
ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 7.
Hexadecimal [16-Bits]



                              6 	.endm
   436D                       1 		SetLabel	spr2,\__row
   436D                       1 r0_spr2::
   4385                       2 		.blkb		__new_len
                     0001     3 		__row = __row + 1
   4385                       1 		SetLabel	spr2,\__row
   439D                       1 r1_spr2::
   439D                       2 		.blkb		__new_len
                     0002     3 		__row = __row + 1
   43B5                       1 		SetLabel	spr2,\__row
   43B5                       1 r2_spr2::
   43CD                       2 		.blkb		__new_len
                     0003     3 		__row = __row + 1
   43CD                       1 		SetLabel	spr2,\__row
   43E5                       1 r3_spr2::
   43E5                       2 		.blkb		__new_len
                     0004     3 		__row = __row + 1
   43FD                       1 		SetLabel	spr2,\__row
   43FD                       1 r4_spr2::
   4415                       2 		.blkb		__new_len
                     0005     3 		__row = __row + 1
   0114                       1 		SetLabel	spr2,\__row
   0114                       1 r5_spr2::
   0114                       2 		.blkb		__new_len
                     0006     3 		__row = __row + 1
   012C                       1 		SetLabel	spr2,\__row
   012C                       1 r6_spr2::
   012C                       2 		.blkb		__new_len
                     0007     3 		__row = __row + 1
   0144                       1 		SetLabel	spr2,\__row
   0144                       1 r7_spr2::
   0144                       2 		.blkb		__new_len
                     0008     3 		__row = __row + 1
                              7 
                              7 
   015C                       8 spr2::
   4415 10 02 08              9 	.db		__sprite_len, 2, __height
                             10    
                     0018    11 	esz_spr2 = __new_len
                     0013    12     sz_spr2 = __sprite_len + spr_d
                             13 
   4418                      14 spr2_src_bmp_ptr::
                             15 
   4418                      46 __spr2_strt:
   4418 00 00 07 E0 1F F8    47 	.db 	0x00,0x00,0x07,0xE0,0x1F,0xF8,0x3F,0xFC,0x6D,0xB6,0xFF,0xFF,0x39,0x9C,0x10,0x08
        3F FC 6D B6 FF FF
        39 9C 10 08
   4428                      48 __spr2_end:
                             49 
                             50 .area _CODE
