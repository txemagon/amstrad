.include "cpctelera.h.s"


.include "sprite_launcher.h.s"


.area _DATA

string: .asciz "CPCtelera up and running!";

.area _CODE

.globl cpct_disableFirmware_asm
.globl cpct_getScreenPtr_asm
.globl cpct_setDrawCharM1_asm
.globl cpct_drawStringM1_asm
.globl cpct_setVideoMode_asm

;;
;; MAIN function. This is the entry point of the application.
;;    _main:: global symbol is required for correctly compiling and linking
;;
_main::
   ;; Disable firmware to prevent it from interfering with string drawing
   call     cpct_disableFirmware_asm

   ld       c, #2                            ;; C = Video Mode
   call     cpct_setVideoMode_asm    


   call     sprite_launcher


   ;; Loop forever
loop:
   jr    loop














;; DEMO CODE

   ;; Set up draw char colours before calling draw string
   ld    d, #0         ;; D = Background PEN (0)
   ld    e, #3         ;; E = Foreground PEN (3)

   call cpct_setDrawCharM1_asm   ;; Set draw char colours

   ;; Calculate a video-memory location for printing a string
   ld   de, #CPCT_VMEM_START_ASM ;; DE = Pointer to start of the screen
   ld    b, #24                  ;; B = y coordinate (24 = 0x18)
   ld    c, #16                  ;; C = x coordinate (16 = 0x10)

   call cpct_getScreenPtr_asm    ;; Calculate video memory location and return it in HL

   ;; Print the string in video memory
   ;; HL already points to video memory, as it is the return
   ;; value from cpct_getScreenPtr_asm
   ld   iy, #string    ;; IY = Pointer to the string 

   call cpct_drawStringM1_asm  ;; Draw the string


