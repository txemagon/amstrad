.include "artwork/general_sprites.h.s"

.area _DATA

;;-------------------------------------------------------------------------
;; MACRO DEFINED DATA
;;
;;	LABELS:
;;		spr1_array::			8 rows with 8 shifted sprites. Only data.
;;								One extra byte every width.
;;
;;		spr1::					Original data (header + data)
;;		spr1_src_bmp_ptr::		Pointer to original data
;;
;;	SYMBOLS:
;;		esz_spr1 		= 		extend sizeof. 	0x10 ( 8 rows of 1 byte => 8 rows of (1+1) bytes)	
;; 		sz_spr1 		= 		sizeof.			0x0A (header: 2, data: 8)
;;

__sprite_len = __spr1_end - __spr1_strt
DefSprite		spr1, 1, __sprite_len
__spr1_strt:
	.db		0x00,0x18,0x3C,0x7E,0xDB,0xFF,0x5A,0x81,0x42
__spr1_end:




;;-------------------------------------------------------------------------
;; MACRO DEFINED DATA
;;
;;	LABELS:
;;		spr2_array::			8 rows with 8 shifted sprites. Only data.
;;								One extra byte every width.
;;
;;		spr2::					Original data (header + data)
;;		spr2_src_bmp_ptr::		Pointer to original data
;;
;;	SYMBOLS:
;;		esz_spr2 		= 		extend sizeof. 	0x15 ( 7 rows of 2 byte => 7 rows of (2+1) bytes)	
;; 		sz_spr2 		= 		sizeof.			0x10 (header: 2, data: 14)
;;

__sprite_len = __spr2_end - __spr2_strt
DefSprite		spr2, 2, __sprite_len
__spr2_strt:
	.db 	0x00,0x00,0x07,0xE0,0x1F,0xF8,0x3F,0xFC,0x6D,0xB6,0xFF,0xFF,0x39,0x9C,0x10,0x08
__spr2_end:

.area _CODE
