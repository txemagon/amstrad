.include "sprites/sprite.h.s"



;;-------------------------------------------------------------------------
;; SPRITE STRUCTURE
;;		Sprite Data Definition
;;			Byte length, Row width in bytes, data
;;
;;		Defined Sprite
;;			length:		1B. 	Number of bytes in sprite data definition
;; 			width: 		1B.		Number of bytes per row.
;;			height		1B.		Number of rows.
;;			data 		n 		Data definition.
;;
;;	SYMBOLS [offsets]:
;;		spr_l: 	0 		Total length
;;		spr_w:	1 		Line width
;;		spr_h:	2 		Sprite height
;;		spr_d:	3 		Data offset



.globl spr1
.globl spr1_array

.globl spr2
.globl spr2_array