.include "artwork/general_sprites.h.s"

.include "sprite_launcher.h.s"
.include "sprites/right_justify.h.s"
.include "sprites/spr_pos.h.s"
.include "sprites/spr_shift.h.s"
.include "sprites/find_arr_beginning.h.s"
.include "sprites/render_sprite_m2.h.s"


sprite_launcher:

	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;;
	;;Generate shifted sprites
	;;


	;; COPY FIRST ROW JUSTIFIED
	ld		ix, (sprite)				;; Original data
	ld  	d, #0
	ld     	e, spr_l(ix)				;; E = SPR_LEN
	call	spr_justify					;; Justify sprite with a bigger width
										;; IY = Pointer to start of array


	;; SHIFT 1 BIT EVERY ROW

	;; Get data length
	ld  	ix, (sprite)
	call  	calc_len
	ld  	d, #0
	ld  	e, a 						;; DE = extended length
	ld      (__ext_len), a

	;; Advance pointers to the end of data
	push    iy
	pop  	ix 							;; IX = Start of Array


	ld  	b, #7  						;; Shift 7 rows (one doesn't need to be shifted)
	__shift_next_row:
		push    bc
		add  	iy, de 					;; IY += row_length (next row)

		add  	ix, de					;; Point to data end
		add  	iy, de

		ld      b, e 					;; B = EXT_DATA_LEN

		call  	spr_shift				;; Displace 1 bit to left data.
		add  	ix, de
		pop  	bc
	djnz  		__shift_next_row




__calc:
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;;
	;; Calculate Memory address
	;;
	ld		ix, #coord
	call	spr_addr					;; Call spr_addr
										;; HL holds VMEM




	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;;
	;; Paint sprite at HL address
	;;
	ld		ix, (sprite) 				;; IX = Sprite Header
	push    ix
	call    find_arr_beginning 			;; IY = Array beginning

	ld		ix, #coord
	call    spr_dspl					;; A = Displacement
	or 		a
	jr  	z, __adv_end
	ld  	d, #0
	push    af
	ld  	a, (__ext_len)
	ld  	e, a
	pop  	af
	__advance_shift:
		add     iy, de
		dec  	a
	jr    	nz, __advance_shift
__adv_end:


	push  	iy 							;; DE = Sprite Data
	pop   	de
	pop 	ix							;; IX = Sprite Header
	call	print_spr					;; Print Sprite.




	;; Move

	;ld  	a, (coordY)
	;inc     a
	;ld   	(coordY), a
	ld  	a, (coordX)
	add  	a, #1
	ld   	(coordX), a
	ld      a, (coordX + 1)
	adc     a, #0
	ld   	(coordX+1), a
	cp 		#2
	jr      nz, __still_line

__line_might_end:
	ld   	a, (__ext_len)
	ld      b, a
	ld  	a, #0x80
	sub  	b
	ld   	b, a
	ld  	a, (coordX)
	cp    	b   		;;0x280 = 640 (- width * 8)
	jr  	nz, __still_line
__line_ends:
	ld   	a,(coordX)
	xor     a
	ld   	(coordX),a
	ld   	a,(coordX+1)
	xor     a
	ld   	(coordX+1),a
	ld  	a, (coordY)
	inc     a
	ld   	(coordY), a
__still_line:

	jr 		__calc

	ret






coord:
coordY:		.dw Y
coordX:		.dw X

sprite:	.dw spr2
__ext_len:  .db 00


