
;;-----------------------------------------------
;; FUNC:  spr_shift
;; 		  Shift 1 bit to the left every width
;;
;;  MODIFIES
;;		 B, IX, IY
;;
;;	PARAMS
;;		IX = Source data end (next position)
;; 		IY = Destiny end (next position)
;;		 B = Data length

spr_shift:
		dec 	ix
		dec  	iy
		ld  	a, 0(ix)
		rl  	a
		ld 		0(iy), a

	djnz  		spr_shift

	ret