.include "artwork/general_sprites.h.s"
.include "sprites/find_arr_beginning.h.s"

;;------------------------------------------------------------------
;; FUNC: right_justify
;;	Pad with 0's on left hand side (leave memory with garbage)
;;
;;	PARAMS
;; 		IX = Points to source
;; 		IY = Destiny
;;		 L = Final width
;;		BC = Height + Original width

right_justify:


	;;---------------------
	;; PREPROTOCOL
	;;

	;; Save registers
	push  	ix
	push  	iy
	push    hl
	push    bc


	;; Calculate pad size
	ld   	a, l
	sub 	c
	ld		l, a  						;; L = PAD_SIZE


	;;---------------------
	;; LOOP
	;;
	_next_row:

		push  	bc						;; Save Height


		;; Pad

		ld  	b, l  					;; REPEAT B = Pad Size
		_start_pad:
			inc  	iy 					;; inc IY pad size
		djnz  		_start_pad




		;; Copy original width from HL to DE

		ld  	b, c 					;; REPEAT B = Original Width
		_start_data:
			ld 		a, 0(ix)
			ld  	0(iy), a 			;; (IY) = (IX)
			inc  	ix
			inc  	iy
		djnz  		_start_data



		pop		bc 						;; B = Height
	djnz  	_next_row



	;;---------------------
	;; POSTPROTOCOL
	;;
	pop  	bc
	pop  	hl
	pop  	iy
	pop  	ix

	ret




;;------------------------------------------------------------------
;; FUNC:  	spr_justify
;; 			Rights justifies sprite to width + 1
;;	PARAMS
;; 		IX = Points to source


spr_justify::

	call  	find_arr_beginning			;; IY = Pointer to start of Array
	push   	iy 							;; Store Array start

	;; ld      iy, #spr1_array  		;; Justified data




	ld  	a, spr_w(ix)  				
	ld  	c, a  						;; C = Width
	inc  	a 
	ld  	l, a 						;; L = Width + 1
	ld  	b, spr_h(ix) 				;; B = Height
	ld   	de,#spr_d 
	add  	ix, de 						;; ix points to source data

	call	right_justify

	pop 	de 							;; return in DE array start
	ret

