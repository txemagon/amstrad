.include "artwork/general_sprites.h.s"



;;---------------------------------------------------
;; 	FUNC: 	Return in IY where array starts
;;
;;  MODIFIES
;;		A, B, C, E
;;
;;  PARAMS
;; 		IX: Pointer to header of sprite
;;
;;	RETURNS
;;		A: 	New length

calc_len:
	;; A = (len + len / width)
	ld     	a, spr_l(ix)
	ld  	e, a  						;; E = DATA_LEN
	ld  	c, spr_w(ix)
	ld      b, #0  						;; B = Quotient (len / width)

	_div_width:
		inc  	b
		sub		c
	jp      nc, _div_width
	dec   	b

	ld   	a, b 				
	add  	e 							;; A = len + len / width

	ret


;;---------------------------------------------------
;; 	FUNC: 	Return in IY where array starts
;;
;;
;;  MODIFIES
;;		A, B, C, D, E, IY
;;
;; 	PARAMS
;; 		IX: Pointer to header of sprite
;;
;;	RETURNS
;;		IY: Pointer to start of array.

find_arr_beginning:
	;; IY = IX - 8 * (len + len / width)
	call  	calc_len  					;; A = (len + len / width)
	neg

	ld  	e, a
	ld  	d,#0xFF						;; DE = - (len + len / width)


	push  	ix
	pop     iy
	ld  	b, #8

	_upstream_iy:						;; IY = IX - 8 * (len + len / width)
		add		iy, de
	djnz 		_upstream_iy

	ret
