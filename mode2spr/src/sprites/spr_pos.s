VMEM = 0xC000


;; Calculate address from x,y coords in mode 2 (doesn't matter mode at all).

;;  y  Mem     Offs  LnScr  LineSet 
;; 00: &C000 - &0000 - 0   - 0
;; 01: &C800 - &0800 - 1   - 0
;; 02: &D000 - &1000 - 2   - 0
;; 03: &D800 - &1800 - 3   - 0
;; 04: &E000 - &2000 - 4   - 0
;; 05: &E800 - &2800 - 5   - 0
;; 06: &F000 - &3000 - 6   - 0
;; 07: &F800 - &3800 - 7   - 0
;; 08: &C050 - &0050 - 0   - 1
;; 09: &C850 - &0850 - 1   - 1
;; 10: &D050 - &1050 - 2   - 1
;; 11: &D850 - &1850 - 3   - 1
;; ...
;;
;; ADDRESS DE = VMEM + LINESCREEN * &800 + LINESET * &50
;; LINESCREEN = Y % 8
;; LINESET    = Y / 8
;;
;; DE = VMEM + (Y % 8) * &800 + (Y / 8) * &50
;;
;; DE = VMEM + (Y % 8) * &800 + (Y / 8) * (&40 + &10)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; SPR_ADDR
;; Calculate the byte address in which a coordinate
;; x,y abides
;;
;; MODIFIES
;; 	A,B,C,D,E, HL
;;
;; PARAMETERS
;;	X Coordinate (Up to 640) (IX+2)
;;	Y Coordinate (Up to 200) (IX+0)
;;
;; RETURNS
;;	HL: Video memory
;;
spr_addr:

	;; Pop X parameter
	ld	c, 2(ix)		;; Extract X coordinate
	ld	b, 3(ix)

	;; X = X / 8
	srl	b
	rr	c			;; x = x / 2
	srl	b
	rr	c			;; x = x / 4
	srl	b
	rr	c			;; x = x / 8

	;; DE = &C000 + x
	ld	de,#VMEM	;; DE = &C000
	ld	a,e			;; 
	add	c			;; E + LOW(x)
	ld	e,a			;; E =E + x
	adc	d			;; If ( E + x > &FF), add carry
	sub	e			;; A = E + x + D - ( E - x)
	add	b			;; A += HIGH(x)
	ld	d,a			;; DE += x


	;; Fetch Y parameter
	ld	b,(ix)		;; Y is only 1 byte long

	;; LINESCREEN
	ld	a,b			;; A = y
	and	#0xF8		;; A = (y / 8) * 8. Integer division.
	ld	l,a
	ld	h,#0
	add	hl,hl		;; HL = (y / 8) * 16
	ld	b,h
	ld	c,l			;; BC = HL = (y / 8) * 16
	add	hl,hl		;; HL = (y / 8) * 32
	add	hl,hl		;; HL = (y / 8) * 64

	;; Add BC = (y / 8) * 16
	ld 	a,l
	add	c
	ld	l,a
	adc	h			;; Carry when adding C.
	sub	l
	add	b			;; B holds carry from add hl, hl
	ld	h,a


	;; LINESET
	ld	a,0(ix)		;; A = y
	and	#0x07			;; A = y % 8
	rlca				;; A = (y % 8) * 2
	rlca				;; A = (y % 8) * 4
	rlca				;; A = (y % 8) * 8
	add	h			;; A = HL + A * &100
	ld	h,a			;; HL = (y % 8) * &800

	add	hl,de			;; HL += x


	ret



	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 	FUNC: 	spr_dspl
;;
;;	Calculate how many bits shall be shifted to the left
;;
;;  MODIFIES
;;  	A, B
;;
;;	PARAMS
;;		IX: Pointer to Y,X coordinate
;;
;;  RETURNS
;; 		A: Positions to be displaced to the left (8 - RIGHT)
;;

spr_dspl:

	ld 		a, 2(ix)
	and 	#0x07
	ld  	b, a
	ld  	a, #0x08
	sub  	b
	cp      #8
	jr		nz, __end
	sub 	#8

__end:
	ret