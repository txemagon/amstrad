.include "../sprites/sprite.h.s"
.include "./render_sprite_m2.h.s"

VMEM=0xC000

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 	SPRITE DEFINITION
;;
;;	Offset	Meaning
;;	00 			Data length
;;	01			Byte Width 	(of line)
;;	03			Byte Length	(of sprite)
;;	04...		Data Definition
;;
;;	SPRITE EXAMPLE
;;
;;	The following definition:
;;
;;	__sprite_len = __spr1_end - __spr1_strt
;;	DefSprite		spr1, 1, __sprite_len
;;	__spr1_strt:
;;		.db		0x18,0x3C,0x7E,0xDB,0xFF,0x5A,0x81,0x42
;;	__spr1_end:
;;
;;
;;	Generates:
;;
;;	DATA
;;
;;	spr1_array::
;;	r0_spr1::		.db 	00,00,00,00, 00,00,00,00, 00,00,00,00, 00,00,00,00
;;	r1_spr1::		.db 	00,00,00,00, 00,00,00,00, 00,00,00,00, 00,00,00,00
;;	r2_spr1::		.db 	00,00,00,00, 00,00,00,00, 00,00,00,00, 00,00,00,00
;;	r3_spr1::		.db 	00,00,00,00, 00,00,00,00, 00,00,00,00, 00,00,00,00
;;	r4_spr1::		.db 	00,00,00,00, 00,00,00,00, 00,00,00,00, 00,00,00,00
;;	r5_spr1::		.db 	00,00,00,00, 00,00,00,00, 00,00,00,00, 00,00,00,00
;;	r6_spr1::		.db 	00,00,00,00, 00,00,00,00, 00,00,00,00, 00,00,00,00
;;	r7_spr1::		.db 	00,00,00,00, 00,00,00,00, 00,00,00,00, 00,00,00,00
;;
;; 	spr1::			.db 	08,01,08
;;  spr1_src_bmp_ptr::
;; 				 	.db		0x18,0x3C,0x7E,0xDB,0xFF,0x5A,0x81,0x42
;;
;; USER SIMBOLS
;;
;;		esz_spr1 = 0x10		;; Extended sizeof spr1. Printable Data Length
;;		sz_spr1  = 0x0B		;; Size of original data + header 



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Print whole sprite
;;
;; MODIFIES
;;	A, B, DE, HL
;;
;; PARAMS
;;	HL:	Video memory address
;;	IX: Sprite Header
;;	DE:	Sprite Definition
;;
print_spr:

	ld  	b, spr_h(ix)
	
	_another_row:
		push    bc
		ld		b, spr_w(ix)			;; Original row width
		inc     b						;; Extended row width

		push  	hl
		_print_columns:
			ld		a,(de)				;; Fetch data from source
			ld 		(hl), a  			;; Print to screen

			inc  	hl					;; Advance pointers
			inc 	de

			djnz	_print_columns

		pop  	hl

		ld  	a, h 					;; Get next row video
		add  	#0x08 					;; memory address
		ld  	h, a 					;; Store in H (and a)

		sub  	#0xC0 					;; If not screen overflow
		jp  	nc, _still_same_char

		;; We have screen overflow and we advance to next character (0x050)
		;; As we are overflown H is in the 00's, so we add C0 to come back to screen
		;; and then 0x50 to advance a character
		ld  	bc,#0xC050
		add  	hl,bc



	    _still_same_char:

    pop  	bc
    djnz  	_another_row

    ret


