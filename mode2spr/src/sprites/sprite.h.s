;;
;; SPRITE DEFINITION
;;




;;-----------------------
;;-- Define Entity
;;-----------------------


;; OFFS	LEN	 Definition
;;  00	 1		Width. 		Number of bytes per scan line.
;;	01	 1		Length.		Number of bytes that build the sprite up.
;;	02	LEN 	Data.		Bytes of sprite.

__off = 0

.macro DefOffset 	_size, _name
	_name = __off
	__off = __off + _size
.endm



;;-----------------------
;;-- Sprite Offsets
;;-----------------------

DefOffset	1, spr_l		;; Sprite length
DefOffset 	1, spr_w		;; Sprite width
DefOffset 	1, spr_h		;; Sprite height
DefOffset	1, spr_d		;; Sprite data



.macro SetLabel 	_name, _row
r'_row'_'_name::
.endm


;;-----------------------
;;-- Reserve room for
;;	 shifted sprites
;;-----------------------

.macro DefArray		_name, _rows _len
	__row = 0
	.rept _rows
		SetLabel	_name,\__row
		.blkb		_len
		__row = __row + 1
	.endm

.endm

;;-----------------------
;;-- Define Sprite
;;
;;	 Defines 3 labels:
;;		- Sprite beginning. 				XXXX::
;;		- Sprite original data beginning.	XXXX_src_bmp_ptr::
;;		- Shifted sprite array.				XXXX_array:
;;
;;	Define 1 constant:
;;		- sizeof_XXXX.	Holds the length of sprite (data + header)
;;-----------------------
.macro DefSprite 	_name, _width, _len

__height = (_len / _width)
__new_len = __height * (_width + 1)			;; Leave one extra byte per row

_name'_array::
	DefArray	_name, 8, __new_len 				;; Define a row per shifted sprite

_name::
	.db		_len, _width, __height
   
	esz_'_name = __new_len
    sz_'_name = _len + spr_d

_name'_src_bmp_ptr::

.endm

