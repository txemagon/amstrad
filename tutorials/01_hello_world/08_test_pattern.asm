;; Try:
;;
;; call &8050,&40FF
;; call &8050,&80FF

org &8050
	; if (argc != 1)
	cp	1		;; Number of params in A
	ret	nz		;; No params given.

	;; Set 16 bit param in bc
	ld	b,(ix+1)
	ld	c,(ix+0)
	

	ld	hl,&C000

again:
	ld	a,(hl)		;; Bring screen to ACU
	bit	7,b		;; Check bit 7
	jr	z,no_and
	and	c

no_and:
	bit	6,b
	jr	z,no_or
	or	c

no_or:
	ld	(hl),a
	inc	l
	jp	nz,again
	inc	h
	jp	nz,again

	ret