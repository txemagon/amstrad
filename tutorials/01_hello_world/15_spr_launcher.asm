X	equ 32
Y	equ 23

org &8000

	ld 	de,(&9000)
	ld 	(coordX),de
	ld 	de,(&9002)
	ld	(coordY),de


	ld	b,2		;; Bit displacement
	ld	iy,sprite2
	ld	ix,sprite_shifted
	call	&8160		;; Fill sprite_shifted


loop:


	ld	ix,coord
	call	&8100		;; Call spr_addr
				;; HL holds VMEM

	ld	iy,sprite2
	call	&8200		;; call paint_spr2


	ret

coord:
coordY:		dw Y
coordX:		dw X

;;	Byte width, Byte length, data
sprite1:	db 1,10,00, &18,&3C,&7E,&DB,&FF,&5A,&81,&42,00
sprite2:	db 2,18,00,00,&07,&E0,&1F,&F8,&3F,&FC,&6D,&B6,&FF,&FF,&39,&9C,&10,&08,00,00

sprite_shifted: