VMEM	equ	&C000


;; Sprite Definition
;;
;;	Offset	Meaning
;;	00	Byte Width 	(of line)
;;	01	Byte Length	(of sprite)
;;	02...	Data Definition


org &8200

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Print whole sprite
;;
;; DESTROYS
;;	
;;
;; PARAMS
;;	HL:	Video memory address
;;	B:	Bit Displacement
;;	IY:	Sprite Definition
;;
print_spr2:
	push 	iy			;; Point ix to the begining of data
	pop	ix
	inc	ix
	inc 	ix

	ld	c,(iy+1)		;; C = BYTE_LENGTH

row:
	ld	b,(iy+0)		;; B = Number of params per line
	call 	inc_width


print_param:
	ld	e,(ix+0)		;; Load param
	ld	(hl),e			;; Push it to screen

	dec	c			;; One param less
	ret	z			;; No more params

	inc	ix			;; Point to next param

	call	next_row
	jr	nz,print_param
	jp	row


next_row:
	dec	b
	dec	hl
	jr	z,next_line
	
	ret


next_line:
	push	bc

		ld	a, (iy+0)	;; Sprite width
		
		ld 	b,a
		call 	inc_width

		push	af
		neg			;; in subs mode. Affects flags
		ld	b,&FF		;; Ca2(D)
		ld	c,a		;; Decrease sprite width
		pop	af
		add	hl, bc		;;

		ld	bc,&800		;; Add 1 line
		add	hl, bc		;; Advance one line

		jp	nc,no_new_char  ;; If screen overflow..
		ld 	bc, -8*&800	;; First line of new char line
		add	hl,bc		;; Back &4000 bytes
		ld	bc,&50
		add	hl,bc


no_new_char:
	pop	bc
	ret




;; inc hl in b
inc_width:
	push	bc
	ld	c,b
	ld	b,0
	add	hl,bc
	pop	bc

	ret