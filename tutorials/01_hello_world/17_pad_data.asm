;; PAD DATA SERIES
;;
;; EXAMPLE:
;;	db WW,LL,DD,DD,DD,DD,DD...
;;	db 1,09, &18,&3C,&7E,&DB,&FF,&5A,&81,&42,&00
;;
;;	WW: WIDTH: 	1 byte/row
;;	LL: LENGTH:	10 bytes
;;
;;	If we justify to 3 bytes we will get ...
;;
;;	db 1,27,    &00,&00,&18,&00,&00,&3C,&00,&00,&7E
;;	db          &00,&00,&DB,&00,&00,&FF,&00,&00,&5A
;;	db	    &00,&00,&81,&00,&00,&42,&00,&00,&00
