;; Execute call &8000 in modes 0,1 and 2
;PATTERN		equ	%11111111
;PATTERN		equ	%11110000
PATTERN		equ	%10101010
;PATTERN		equ	%10101100
;PATTERN		equ	%11001100

org &8000
	ld	hl,&C000

again_e
	ld	a,(hl)
	;xor	PATTERN			;; Change this pattern as you wish
	and	PATTERN
	ld	(hl),a
	inc	l
	jp	nz,again_e
	inc	h
	jp	nz,again_e

	ret