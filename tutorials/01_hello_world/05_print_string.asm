ThinkPositive	equ	1
PrintChar	equ	&BB5A

org &8100
	ld	hl, introduction
	call	print_string
	call	new_line
	ld	hl, message
	call	print_string

	ret

print_string:
	ld	a,(hl)
	cp	255
	ret	z
	inc	hl
	call	PrintChar
	jr	print_string


introduction:
	db 'Thought of the day...',255

ifdef	ThinkPositive
	message:	db 'Z80 is Awesome!', 255
else
	message:	db '6510 sucks!', 255
endif

new_line
	ld	a,13
	call	PrintChar
	ld	a,10
	call	PrintChar

	ret