ThinkPositive	equ	1
PrintChar	equ	&BB5A

org &8200

	ld	ix,square_brackets
	ld	hl,message
	ld	de,print_string
	call	do_brackets

	call	new_line

	ld	ix,curly_brackets
	ld	hl,message
	ld	de,print_string
	call	do_brackets

	ret

do_brackets:
	ld	a,(ix+0)
	call	PrintChar
	call	do_call_de
	ld	a,(ix+1)
	call	PrintChar

	ret

do_call_de:
	push	de

	ret

square_brackets:	db '[]'
curly_brackets:		db '{}'


print_string:
	ld	a,(hl)
	cp	255
	ret	z
	inc	hl
	call	PrintChar
	jr	print_string


introduction:
	db 'Thought of the day...',255

ifdef	ThinkPositive
	message:	db 'Z80 is Awesome!', 255
else
	message:	db '6510 sucks!', 255
endif

new_line
	ld	a,13
	call	PrintChar
	ld	a,10
	call	PrintChar

	ret