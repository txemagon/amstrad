org &8100
	ld	hl,&C000

again_b:
	ld	a,(hl)
	srl	a
	ld	(hl),a
	inc	l
	jp	nz,again_b
	inc	h
	jp	nz,again_b

	ret

org &8200
	di
	ld	hl,&FF00

again_c:
	ld	a,(hl)
	sll	a
	ld	(hl),a
	dec	l
	jp	nz,again_c
	dec	h
	ld 	a,h
	cp	&BF
	jp	nz,again_c

	ret