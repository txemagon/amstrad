;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Calculate new length in bytes 
;; when changing width
;;
;; MODIFIES
;;
;;
;; PARAMS
;;	A:	Data length (bytes)
;;	D:	New width   (bytes)
;;	E:	Old width   (bytes)
;;	
;; RETURN
;;	B:	New data length
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; unit_len = data_len / old_width 
	ld	c,0
unitarian_length:

	inc	c
	sub	e		;; Get the length if width = 1
	jr	nc, unitarian_length	

end_division_unitlen:
				;; C holds unitarian length
	
	;; new_len = unit_len * new_width
new_length:
	add	d
	djnz	new_length	;; Calculate new length
	ld	(ix+1),a	;; Store new length
