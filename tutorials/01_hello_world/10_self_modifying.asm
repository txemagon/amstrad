;; Make and, or or not with a pattern to screen
;; Try:
;;	call &8300,&1F0
;;	call &8300,&2F0
;;	call &8300,&3F0

org &8300
	cp	1
	ret	nz
	ld	a,(ix+1)		;; Load operation option

	;; case
	ld	hl,SMAND
	cp	1
	jr	z,start

	ld	hl,SMOR
	cp	2
	jr	z,start

	ld	hl,SMXOR
	cp	3
	jr	z,start

	ret				;; Invalid option: end

start:
	ld	a,(hl)			;; hl points to opcode
	ld	(self_modify),a		;; Opcode for operation
	ld	a,(ix+0)
	ld	(self_modify+1),a	;; Command parameter


	ld	hl,&C000
again_d:
	ld	a,(hl)
self_modify:
	nop
	nop
	ld	(hl),a
	inc	l
	jp	nz,again_d
	inc	h
	jp	nz,again_d

	ret


SMAND:	and	1
SMOR:	or	1
SMXOR	xor	1