org &8150


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Calculate Sprite bit displacement in mode 2
;;
;; MODIFIES
;;	A,B
;; PARAMS
;; 	BC: X Coordinate
;;
;; RETURNS
;;	B:  Necessary bit displacement
;;
spr_displacement2:
	ld 	a,c
	and	&07
	ld	b,a

	ret

