;; Prints a sprite in mode 2
;;
;; Usage:
;;	call	&8000,<bytes_width>,data..
;; Example:
;;	call &8000,1,&18,&3C,&7E,&DB,&FF,&5A,&81,&42
;;	call &8000,2,&07,&E0,&1F,&F8,&3F,&FC,&6D,&B6,&FF,&FF,&39,&9C,&10,&08

VMEM	equ	&C800
PCHAR	equ	&BB5A


org &8000

	;; C: Number of params
	;; D: Width of the sprite in bytes
	;; E: Sprite (param)

	;; Test number of params
	ld	b,a
	sub	2
	jp	c, usage		;; At least two params required.
	ld	a,b

	ld	b,0
	ld	c,a			;; Store number of parameters
	dec	c			;; last = params - 1
	
	;; Get the width of sprite in bytes in D
	add	ix,bc
	add	ix,bc
	ld	d,(ix)
	
	;; Advance to next param
	dec	ix
	dec	ix

	ld	hl,VMEM
	call	print_spr

	ret



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Print whole sprite
;;
;; DESTROYS
;;	A, B, C, E, IX, HL
;;
;; PARAMS
;;	HL:	Video memory address
;;
print_spr:
	
row:
	ld	b,d			;; Number of params per line

print_param:
	ld	e,(ix+0)		;; Load param
	ld	(hl),e


	dec	c			;; One param less
	ret	z			;; No more params

	dec	ix			;; Point to next param
	dec	ix

	call	next_row
	jr	nz,print_param
	jp	row


next_row:
	dec	b
	inc	hl
	jr	z,next_line
	
	ret


next_line:
	push	bc

		ld	a, d		;; Sprite width
		push	af
		neg			;; in subs mode. Affects flags
		ld	b,&FF		;; Ca2(D)
		ld	c,a		;; Decrease sprite width
		pop	af
		add	hl, bc		;;

		ld	bc,&800		;; Add 1 line
		add	hl, bc		;; Advance one line

		jp	nc,no_new_char  ;; If screen overflow..
		ld 	bc, -8*&800	;; First line of new char line
		add	hl,bc		;; Back &4000 bytes


no_new_char:
	pop	bc
	ret






;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; AUXILIARY FUNCTIONS
;;

;; Print Usage
usage:
	ld	hl, usage_mssg
	call	print_string
	
	ret



;; Text Functions
print_string:
	ld	a,(hl)
	cp	255
	jr	z, string_end
	call	PCHAR
	inc	hl
	jr	print_string


string_end:
	call 	new_line

	ret


new_line:
	ld	a,&A
	call	PCHAR
	ld	a,&D
	call	PCHAR

	ret

usage_mssg:	db 'Usage: call &8000,<byte width>,data...', 255
