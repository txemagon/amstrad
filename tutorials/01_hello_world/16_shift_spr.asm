org &8160

;; Shift n bits to the right (8-n to the left)
;; PARAMS
;; 	B  = Displacement to the right
;;	IY = Pointer to the original sprite
;; 	IX = Pointer to the empty array to fill with shifted array

sprite_shift:
	
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;; Calculate displacement to the left
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	ld	a, 8
	sub	b
	ld	c, a		;; C = displacement left




	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;; Add 1 to width
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	ld	a, (iy)		;; Fetch current width
	ld	b, a		;; B = current_width
	inc	a
	ld	(ix),a		;; Save increased width to its new location



	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;; Calculate new length in bytes
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	ld	a,(iy+1)	;; Load current length
				;; B already loaded with current length

unitarian_length:
	dec	b		;; One division by default
	jr	z, end_division_unitlen
	
	srl	a		;; Get the length if width = 1
	jr	unitarian_length

end_division_unitlen:
	ld	d,a		;; Store unitarian width
				;; A holds unitarian length
	ld	b,(iy)		;; Get the old width (A already has 1 width)
	

new_length:
	add	d
	djnz	new_length	;; Calculate new length
	ld	(ix+1),a	;; Store new length



	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;; Shift bits
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

one_bit_displacement:
	ld	b,(ix+1)	;; Byte length
	push	ix		;; Save entity start
	push	iy
	inc	ix		;; Advance to first data copy pos
	inc	ix		
	inc	iy		;; Advance source
	inc	iy


	;; DISPLACE ALL BYTES 1 BIT
	row_start:
		;; START AT THE END OF 4 BYTE (width=3)
		ld	d,0		;; DE = row width
		ld	e,(iy-2)
		add	iy,de		;; iy is 1 byte shorter
		inc	e
		add	ix,de		;; Advance to next row
		dec	ix		;; Last byte in row
		dec	iy

		
		;; SHIFT LAST BYTE (3)
		ld 	a,(iy)
		sla	a		;; Displace last byte to left
		ld	(ix),a		;; Save displaced byte
		dec	b		;; One byte done
		dec	e		;; Last byte doesn't has source

		;; SHIFT BYTES (2,1)
		displace_extra_bytes:
			dec	ix		;; Point to previous byte
			dec	iy
			dec 	e		;; width - 1
			jr	z,width_displacement_ends
			ld	a,(iy)
			rl	a		;; Account previous rotation
			ld	(ix),a
			dec	b		;; Another byte done
			jr	displace_extra_bytes

	width_displacement_ens:

		;; SHIFT BYTE 0
		

		;; ADVANCE POINTER 4 BYTES ANS START AGAIN
		jr	nz,row_start	;; if (B!=0) tackle width bytes more

	
	;; END OF ALL DATA
	pop	iy
	pop	ix		;; Restore entity start


	;; TACKLE ANOTHER BIT DISPLACEMENT
	dec	c		;; One bit displaced across all sprite
	jr	nz, one_bit_displacement


	ret