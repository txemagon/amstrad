PrintChar	equ	&BB5A

org &8300

	cp	1			; Number of params in BASIC call in a
	jp	nz,show_usage
	ld	a,'&'
	call	PrintChar		; Print Hex Char
	ld	a,(ix+1)		; High byte
	or	a
	call	nz,show_hex		; Print if not 0
	ld	a,(ix+0)		; Print low byte
	call	show_hex

	ret

;; Point with HL to message
show_usage:
	ld	hl, show_usage_message
	jp 	print_string

show_usage_message:
	defb	"Usage: Call &8300,[16 bit number]",255



;; show_hex
;;
;;	Prologue to print char function. Return in PrintChar
;;
;; DESTROYS
;;	A,B
;;
;; PARAMS
;; 	A: Holds the number to print.
;;
show_hex:
	ld	b,16
	call	math_div			;; Print hight nibble
	push	af
		ld	a,c			;; Load the result of math_div
		call	print_hex_char
	pop	af				;; Print low nibble
	jp	print_hex_char


print_hex_char:
	cp	10				;; If there is carry
	jr	c,print_hex_char_not_AtoF	;; then 10 is bigger than A
	add	7				;; 'A'-'9'=&41-&39=7
print_hex_char_not_AtoF:
	add	48				;; 48=&30='0'
	jp	PrintChar			;; Take advantage of PrintChar ret



;; math_div
;;
;;
;; DESTROYS
;;	A,C
;;
;; PARAMS
;; 	A: Dividend
;;	B: Divisor
;;
;; RETURNS
;;	C: Quotient 
math_div:
	ld	c,0
	cp	0	; 0 /x == 0 => end
	ret	z
math_div_again:
	sub	b
	inc	c
	jp	nc,math_div_again
	add	b
	dec	c

	ret



;; print_string
;;
;; PARAMS
;; 	HL: Pointer to first char
;;
;; DESTROYS
;;	A
print_string:
	ld	a,(hl)
	cp	255
	ret	z
	inc	hl
	call	PrintChar
	jr	print_string