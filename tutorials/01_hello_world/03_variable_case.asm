org &8000
	ld	a,(&9000)
	ld	bc,(&9001)

	cp	0
	jr	z,math_add
	cp	1
	jr	z,math_sub
	cp	2
	jr	z,math_mult
	cp	3
	jr	z,math_div

	ld	a,0
save_result:
	ld	(&9003),a
	ret

math_sub:
	ld	a,c
	sub	b
	jr	save_result

math_add:
	ld	a,c
	add	b
	jr	save_result
math_mult:
	ld	a,b
	cp	0
	jr	z,save_result
	ld	a,0
math_mult_again:
	add	c
	djnz	math_mult_again
	jr	save_result

math_div:
	ld	a,c
	cp	0
	jr	z,save_result
	ld	d,0
math_div_again:
	sub	b
	inc	d
	jp	nc,math_div_again
	dec	d
	ld	a,d
	jr	save_result