;;
;; RENDER SYSTEM
;;

.include "cpct_functions.h.s"
.include "entity_manager.h.s"

rendersys_init::
    ret

;; INPUT
;;      IX: Pointer to first entity to render
;;       A: Number of entities to render
rendersys_update::

_renloop:
    push     af
    ld       de, #0xC800      ;; DE = Pointer to screen init
    ld        b, e_y(ix)        ;; B = Y coordinate
    ld        c, e_x(ix)        ;; C = X coordinate
    call     cpct_getScreenPtr_asm

    ex       de, hl           ;; DE = Pointer to screen memory
    ld        a, e_col(ix)        ;; A = Color
    ld        c, e_w(ix)        ;; C = Width
    ld        b, e_h(ix)        ;; B = Height
    call     cpct_drawSolidBox_asm

    pop     af
    dec     a
    ret     z

    ld       bc, #sizeof_e
    add      ix, bc

    jp      _renloop
