.include "cpctelera.h.s"
.include "cpct_functions.h.s"
.include "render_system.h.s"
.include "entity_manager.h.s"


.area _DATA
.area _CODE

DefineEntity player, 20, 20, 1, 1, 4, 32, 0xF0
DefineEntity enemy , 40, 80,-1, 0, 3, 12, 0xFF

_main::
   ;; Disable firmware to prevent interferences
   call cpct_disableFirmware_asm

   ;; Init systems
   call rendersys_init

   ld       hl,#player
   call     entityman_create

   ld       hl,#enemy
   call     entityman_create


loop:
    call    entityman_getEntityArray_IX
    call    entityman_getNumEntities_A
    call    rendersys_update

   jr    loop
