ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 1.
Hexadecimal [16-Bits]



                              1 ;;
                              2 ;; RENDER SYSTEM
                              3 ;;
                              4 
ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 2.
Hexadecimal [16-Bits]



                              5 .include "cpct_functions.h.s"
                              1 ;;
                              2 ;; CPCT FUNCTIONS
                              3 ;;
                              4 
                              5 ;; PUBLIC FUNCTIONS
                              6 .globl cpct_getScreenPtr_asm
                              7 .globl cpct_drawSolidBox_asm
                              8 .globl cpct_disableFirmware_asm
                              9 
                             10 
                             11 ;; CONSTANTS
                             12 ;;.globl CPCT_VMEM_START_ASM
ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 3.
Hexadecimal [16-Bits]



                              6 .include "entity_manager.h.s"
                              1 ;;
                              2 ;; ENTITY MANAGER
                              3 ;;
                              4 
                              5 ;; PUBLIC FUNCTIONS
                              6 .globl entityman_getEntityArray_IX
                              7 .globl entityman_getNumEntities_A
                              8 .globl entityman_create
                              9 
                             10 
                             11 ;; CONSTANTS
                             12 
                             13 ;; ENTITY DEFINITION MACRO
                             14 .macro DefineEntity _name, _x, _y, _vx, _vy, _w, _h, _color
                             15 _name:
                             16     .db     _x
                             17     .db     _y
                             18     .db     _w
                             19     .db     _h
                             20     .db     _vx
                             21     .db     _vy
                             22     .db     _color
                             23 .endm
                             24 
                     0000    25 e_x   = 0
                     0001    26 e_y   = 1
                     0002    27 e_w   = 2
                     0003    28 e_h   = 3
                     0004    29 e_vx  = 4
                     0005    30 e_vy  = 5
                     0006    31 e_col = 6
                     0007    32 sizeof_e = 7
                             33 
                             34 .macro DefineEntityArray _N
                             35     .rept _N
                             36     DefineEntity aasaa, 0, 0, 0, 0, 0, 0, 0, 0
                             37     .endm
                             38 .endm
ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 4.
Hexadecimal [16-Bits]



                              7 
   4067                       8 rendersys_init::
   4067 C9            [10]    9     ret
                             10 
                             11 ;; INPUT
                             12 ;;      IX: Pointer to first entity to render
                             13 ;;       A: Number of entities to render
   4068                      14 rendersys_update::
                             15 
   4068                      16 _renloop:
   4068 F5            [11]   17     push     af
   4069 11 00 C8      [10]   18     ld       de, #0xC800      ;; DE = Pointer to screen init
   406C DD 46 01      [19]   19     ld        b, e_y(ix)        ;; B = Y coordinate
   406F DD 4E 00      [19]   20     ld        c, e_x(ix)        ;; C = X coordinate
   4072 CD 49 41      [17]   21     call     cpct_getScreenPtr_asm
                             22 
   4075 EB            [ 4]   23     ex       de, hl           ;; DE = Pointer to screen memory
   4076 DD 7E 06      [19]   24     ld        a, e_col(ix)        ;; A = Color
   4079 DD 4E 02      [19]   25     ld        c, e_w(ix)        ;; C = Width
   407C DD 46 03      [19]   26     ld        b, e_h(ix)        ;; B = Height
   407F CD 9D 40      [17]   27     call     cpct_drawSolidBox_asm
                             28 
   4082 F1            [10]   29     pop     af
   4083 3D            [ 4]   30     dec     a
   4084 C8            [11]   31     ret     z
                             32 
   4085 01 07 00      [10]   33     ld       bc, #sizeof_e
   4088 DD 09         [15]   34     add      ix, bc
                             35 
   408A C3 68 40      [10]   36     jp      _renloop
