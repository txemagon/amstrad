ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 1.
Hexadecimal [16-Bits]



                              1 ;;
                              2 ;; ENTITY MANAGER
                              3 ;;
                              4 
ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 2.
Hexadecimal [16-Bits]



                              5 .include "entity_manager.h.s"
                              1 ;;
                              2 ;; ENTITY MANAGER
                              3 ;;
                              4 
                              5 ;; PUBLIC FUNCTIONS
                              6 .globl entityman_getEntityArray_IX
                              7 .globl entityman_getNumEntities_A
                              8 .globl entityman_create
                              9 
                             10 
                             11 ;; CONSTANTS
                             12 
                             13 ;; ENTITY DEFINITION MACRO
                             14 .macro DefineEntityAnonymous  _x, _y, _vx, _vy, _w, _h, _color
                             15     .db     _x
                             16     .db     _y
                             17     .db     _w
                             18     .db     _h
                             19     .db     _vx
                             20     .db     _vy
                             21     .db     _color
                             22 .endm
                             23 
                             24 .macro DefineEntity _name, _x, _y, _vx, _vy, _w, _h, _color
                             25 _name::
                             26    DefineEntityAnonymous _x, _y, _vx, _vy, _w, _h, _color
                             27 .endm
                             28 
                     0000    29 e_x   = 0
                     0001    30 e_y   = 1
                     0002    31 e_w   = 2
                     0003    32 e_h   = 3
                     0004    33 e_vx  = 4
                     0005    34 e_vy  = 5
                     0006    35 e_col = 6
                     0007    36 sizeof_e = 7
                             37 
                             38 
                             39 .macro DefineEntityArray _name, _N
                             40 _name:
                             41     .rept _N
                             42     DefineEntityAnonymous 0xDE, 0xAD,  0xDE, 0xAD, 0xDE, 0xAD, 0xAA
                             43     .endm
                             44 .endm
ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 3.
Hexadecimal [16-Bits]



                              6 
                     0003     7 max_entities == 3
                              8 
   402B 00                    9 _num_entities::  .db 0
   402C 2E 40                10 _last_elem_ptr:: .dw _entity_array
   402E                      11 DefineEntityArray _entity_array, max_entities
   0003                       1 _entity_array:
                              2     .rept max_entities
                              3     DefineEntityAnonymous 0xDE, 0xAD,  0xDE, 0xAD, 0xDE, 0xAD, 0xAA
                              4     .endm
   0003                       1     DefineEntityAnonymous 0xDE, 0xAD,  0xDE, 0xAD, 0xDE, 0xAD, 0xAA
   402E DE                    1     .db     0xDE
   402F AD                    2     .db     0xAD
   4030 DE                    3     .db     0xDE
   4031 AD                    4     .db     0xAD
   4032 DE                    5     .db     0xDE
   4033 AD                    6     .db     0xAD
   4034 AA                    7     .db     0xAA
   000A                       1     DefineEntityAnonymous 0xDE, 0xAD,  0xDE, 0xAD, 0xDE, 0xAD, 0xAA
   4035 DE                    1     .db     0xDE
   4036 AD                    2     .db     0xAD
   4037 DE                    3     .db     0xDE
   4038 AD                    4     .db     0xAD
   4039 DE                    5     .db     0xDE
   403A AD                    6     .db     0xAD
   403B AA                    7     .db     0xAA
   0011                       1     DefineEntityAnonymous 0xDE, 0xAD,  0xDE, 0xAD, 0xDE, 0xAD, 0xAA
   403C DE                    1     .db     0xDE
   403D AD                    2     .db     0xAD
   403E DE                    3     .db     0xDE
   403F AD                    4     .db     0xAD
   4040 DE                    5     .db     0xDE
   4041 AD                    6     .db     0xAD
   4042 AA                    7     .db     0xAA
                             12 
   4043                      13 entityman_getEntityArray_IX::
   4043 DD 21 2E 40   [14]   14     ld      ix,#_entity_array
   4047 C9            [10]   15     ret
                             16 
   4048                      17 entityman_getNumEntities_A::
   4048 3A 2B 40      [13]   18     ld      a, (_num_entities)
   404B C9            [10]   19     ret
                             20 
                             21 ;; INPUT
                             22 ;;  HL: Pointer to entity initializer bytes
   404C                      23 entityman_create::
   404C ED 5B 2C 40   [20]   24     ld      de, (_last_elem_ptr)
   4050 01 07 00      [10]   25     ld      bc, #sizeof_e
   4053 ED B0         [21]   26     ldir
                             27 
   4055 3A 2B 40      [13]   28     ld      a, (_num_entities)
   4058 3C            [ 4]   29     inc     a
   4059 32 2B 40      [13]   30     ld      (_num_entities), a
                             31 
   405C 2A 2C 40      [16]   32     ld      hl, (_last_elem_ptr)
ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 4.
Hexadecimal [16-Bits]



   405F 01 07 00      [10]   33     ld      bc, #sizeof_e
   4062 09            [11]   34     add     hl,bc
   4063 22 2C 40      [16]   35     ld      (_last_elem_ptr), hl
                             36 
   4066 C9            [10]   37     ret
