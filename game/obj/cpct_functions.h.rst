ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 1.
Hexadecimal [16-Bits]



                              1 ;;
                              2 ;; CPCT FUNCTIONS
                              3 ;;
                              4 
                              5 ;; PUBLIC FUNCTIONS
                              6 .globl cpct_getScreenPtr_asm
                              7 .globl cpct_drawSolidBox_asm
                              8 .globl cpct_disableFirmware_asm
                              9 
                             10 
                             11 ;; CONSTANTS
                             12 ;;.globl CPCT_VMEM_START_ASM
