ASxxxx Assembler V02.00 + NoICE + SDCC mods  (Zilog Z80 / Hitachi HD64180), page 1.
Hexadecimal [16-Bits]



                              1 ;;
                              2 ;; ENTITY MANAGER
                              3 ;;
                              4 
                              5 ;; PUBLIC FUNCTIONS
                              6 .globl entityman_getEntityArray_IX
                              7 .globl entityman_getNumEntities_A
                              8 .globl entityman_create
                              9 
                             10 
                             11 ;; CONSTANTS
                             12 
                             13 ;; ENTITY DEFINITION MACRO
                             14 .macro DefineEntityAnonymous  _x, _y, _vx, _vy, _w, _h, _color
                             15     .db     _x
                             16     .db     _y
                             17     .db     _w
                             18     .db     _h
                             19     .db     _vx
                             20     .db     _vy
                             21     .db     _color
                             22 .endm
                             23 
                             24 .macro DefineEntity _name, _x, _y, _vx, _vy, _w, _h, _color
                             25 _name::
                             26    DefineEntityAnonymous _x, _y, _vx, _vy, _w, _h, _color
                             27 .endm
                             28 
                     0000    29 e_x   = 0
                     0001    30 e_y   = 1
                     0002    31 e_w   = 2
                     0003    32 e_h   = 3
                     0004    33 e_vx  = 4
                     0005    34 e_vy  = 5
                     0006    35 e_col = 6
                     0007    36 sizeof_e = 7
                             37 
                             38 
                             39 .macro DefineEntityArray _name, _N
                             40 _name:
                             41     .rept _N
                             42     DefineEntityAnonymous 0xDE, 0xAD,  0xDE, 0xAD, 0xDE, 0xAD, 0xAA
                             43     .endm
                             44 .endm
