#include "rendermanager.h"
#include "terminalrendersystem.h"

RenderManager::RenderManager() {
    m_renderer = new TerminalRenderSystem();
}

RenderManager::~RenderManager() {
    delete m_renderer;
}

RenderManager&
RenderManager::p() {
    static RenderManager instance;

    return instance;
}

RenderSystem&
RenderManager::rendersystem() {
    return *m_renderer;
}

void
RenderManager::changeToRenderer(RenderType t, TVecRenderObjs& robjs) {
    switch (t) {
        case RenderType::Terminal: switchRenderer<TerminalRenderSystem>(robjs); break;
        case RenderType::SFML: switchRenderer<SFMLRenderSystem>(robjs); break;
        case RenderType::SDL: switchRenderer<SDLRenderSystem>(robjs); break;
        case RenderType::Allegro: switchRenderer<AllegroRenderSystem>(robjs); break;
    }
}
