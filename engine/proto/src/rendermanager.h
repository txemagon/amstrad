
#ifndef __RENDERMANAGER_H__
#define __RENDERMANAGER_H__

#include <vector>
#include <rendersystem.h>
#include <renderobj.h>
#include <rederonjimpl.h>


enum class RenderType { Terminal, SFML, SDL, Allegro };
using TVecRenderObjs = std::vector <RenderObj *>;

class RenderManager
{
public:
    ~RenderManager ();

    static RenderManager& p();
    RenderSystem& rendersystem();
    void changeToRenderer (RenderType t, TVecRenderObjs& robjs);
    void switchToSFML (TVecRenderObjs& robjs);

    template <class TRenderTo>
    void switchRenderer (TVecRenderObjs& robjs) {
        delete m_renderer;
        m_renderer  = new TRenderTo();
        for (auto *o : robjs) {
            const char *file;
            RenderObjImpl *newimp;
            file = o->getImplementation()->getObjFile().c_str();
            newimp = m_renderer->createSpriteImpl(file);
        }
    }

private:
    RenderManager ();

    RenderSystem* m_renderer = nullptr;
};

#endif // RENDERMANAGER_H
