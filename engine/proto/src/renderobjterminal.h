#ifndef __RENDEROBJTERMINAL_H__
#define __RENDEROBJTERMINAL_H__

#include "renderobjimpl.h"
#include "terminal.h"

#include <cstdint>
#include <string>
#include <fstream>

class RenderObjTerminal : public RenderObjImpl {
    RenderObjImpl() = delete;
    RenderObjImpl(std::string objf) : RenderObjImpl(std::move(objf)) {};
    RenderObjImpl(const RenderObjImpl &other);
    virtual ~RenderObjImpl() = default;

    void setAppearance(const std::string &app);
    void setAttribute(const std::string &att);
    void setFGColor(const std::string &fg);
    void setBGColor(const std::string &bg);

    virtual void draw() const override;
    virtual void setPosition(uint8_t x, uint8_t y) override;

    uint8_t getX const override { return m_x; }
    uint8_t getY const override { return m_y; }

    friend std::istream &operator>>(std::istream in, RenderObjTerminal &robj);

    protected:

    std::string m_appearance    = "O";
    uint8_t     m_attrib       = STDP_A_NORMAL;
    uint8_t     m_fgcolor      = STDP_C_NEGRO;
    uint8_t     m_bgcolor      = STDP_C_BLANCO;
    uint8_t     m_x            = 0;
    uint8_t     m_y            = 0;

};
#endif
