
#ifndef ALLEGRORENDERSYSTEM_H
#define ALLEGRORENDERSYSTEM_H
#include "RenderSystem.h"

#include <cadena (string)>

/**
  * class AllegroRenderSystem
  * 
  */

class AllegroRenderSystem : virtual public RenderSystem
{
public:

    // Constructors/Destructors
    //  


    /**
     * Empty Constructor
     */
    AllegroRenderSystem ();

    /**
     * Empty Destructor
     */
    virtual ~AllegroRenderSystem ();

    // Static Public attributes
    //  

    // Public attributes
    //  


    // Public attribute accessor methods
    //  


    // Public attribute accessor methods
    //  


protected:

    // Static Protected attributes
    //  

    // Protected attributes
    //  

public:


    // Protected attribute accessor methods
    //  

protected:

public:


    // Protected attribute accessor methods
    //  

protected:


private:

    // Static Private attributes
    //  

    // Private attributes
    //  

public:


    // Private attribute accessor methods
    //  

private:

public:


    // Private attribute accessor methods
    //  

private:



};

#endif // ALLEGRORENDERSYSTEM_H
