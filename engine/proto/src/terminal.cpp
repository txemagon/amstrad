#include "terminal.h"
#include "ansi.h"

#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <sys/ioctl.h>
#include <unistd.h>

#define ERROR -1

using namespace std;

int Terminal::cursor_x_;
int Terminal::cursor_y_;
int Terminal::size_x_;
int Terminal::size_y_;
int Terminal::colour[8][8];

Terminal::terminalostream Terminal::sout;

Terminal::Terminal () {}
Terminal::~Terminal () {};

struct termios orig_termios;

int getWindowSize(int *rows, int *cols) {
    struct winsize ws;
    if (ioctl(STDOUT_FILENO, TIOCGWINSZ, &ws) == -1 || ws.ws_col == 0) {
        return -1;
    } else {
        *cols = ws.ws_col;
        *rows = ws.ws_row;
        return 0;
    }
}

void disableRawMode () {
    tcsetattr (STDIN_FILENO, TCSAFLUSH, &orig_termios);
}

void enableRawMode () {
    /* Keyboard modes
     * K_RAW
     * k_XLATE
     * k_MEDIUMRAW
     * K_UNICODE
     */
    tcgetattr (STDIN_FILENO, &orig_termios);
    atexit (disableRawMode);

    struct termios raw = orig_termios;
    raw.c_iflag &= ~(BRKINT | ICRNL | INPCK | ISTRIP | IXON);
    raw.c_oflag &= ~(OPOST);
    raw.c_cflag |= (CS8);
    raw.c_lflag &= ~(ECHO | ICANON | IEXTEN | ISIG);
    raw.c_cc[VMIN] = 0;
    raw.c_cc[VTIME] = 1;

    tcsetattr (STDIN_FILENO, TCSAFLUSH, &raw);
}

void
Terminal::init () {
    enableRawMode ();
    ANSI(CLEAR CURSOR_OFF);
    MOVE(0,0);
    getWindowSize (&size_x_, &size_y_);
}

void
Terminal::destroy () {
    disableRawMode ();
    MOVE(0,0);
    ANSI(CLEAR CURSOR_ON);
}

void
Terminal::set_cursor (int x, int y) { MOVE(x, y); }

int
get_cursor (int *rows, int *cols) {
    char buf[32];
    unsigned int i = 0;
    if (write(STDOUT_FILENO, "\x1b[6n", 4) != 4) return -1;
    while (i < sizeof(buf) - 1) {
        if (read(STDIN_FILENO, &buf[i], 1) != 1) break;
        if (buf[i] == 'R') break;
        i++;
    }
    buf[i] = '\0';
    if (buf[0] != '\x1b' || buf[1] != '[') return -1;
    if (sscanf(&buf[2], "%d;%d", rows, cols) != 2) return -1;
    return 0;
}

void
Terminal::change_colour (const char *ATTR, const char *FG, const char *BG) {
    ANSI (ATTR);
    ANSI (FG);
    ANSI (BG);
}

void
Terminal::clear () { ANSI (CLEAR); }

void
Terminal::horz_line (int min_x, int max_x, int y, chtype ch) {
    for (int i=min_x; i<=max_x; i++) {
        MOVE (y, i);
        printf ("%s", ch);
    }
    fflush (stdout);
}

void
Terminal::vert_line (int min_y, int max_y, int x, chtype ch) {
    for (int i=min_y; i<=max_y; i++) {
        MOVE (i, x);
        printf ("%s", ch);
    }
    fflush (stdout);
}

void
Terminal::frame (int min_x, int min_y, int max_x, int max_y, chtype ch) {
    horz_line (min_x, max_x, min_y, ch);
    horz_line (min_x, max_x, max_y, ch);
    vert_line (min_y, max_y, min_x, ch);
    vert_line (min_y, max_y, max_x, ch);
}

void
Terminal::box (int min_x, int min_y, int max_x, int max_y, chtype ch) {
    for (int i=min_y; i<=max_y; i++)
        horz_line (min_x, max_x, i, ch);
}

void
Terminal::write_char (chtype ch) {
    printf ("%s", ch);
    fflush (stdout);
}

void
Terminal::validate_cursor () {
    if (cursor_x_ >= size_x_ )
        cursor_x_ = 0;
    if (cursor_x_ <0 )
        cursor_x_ = size_x_ - 1;
    if (cursor_y_ >= size_y_ )
        cursor_y_ = 0;
    if (cursor_y_ <0 )
        cursor_y_ = size_y_ - 1;
}

int
Terminal::cursor_x () { return cursor_x_; }

int
Terminal::cursor_y () { return cursor_y_; }

int
Terminal::size_x () { return size_x_; }

int
Terminal::size_y () { return size_y_; }

int
Terminal::get_key () {
    return getchar ();
}
