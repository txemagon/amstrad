#ifndef __TERMINAL_H__
#define __TERMINAL_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <string>
#include <sstream>

#include "ansi.h"

typedef const char * chtype;

class Terminal
{
    public:
        class terminalostream
        {
            public:
                terminalostream() {};

                template <typename T>
                terminalostream& operator<<(const T& ui) {
                    std::stringstream ss;
                    ss << ui;
                    write_char(ss.str().c_str());
                    return *this;
                }
        }; // class terminalostream

        static terminalostream sout;

        Terminal ();
        ~Terminal();
        static void init     ();
        static void destroy  ();

        // Graphics primitives
        static bool is_attribute  (const int att);
        static bool is_colour     (const int color);
        static void set_cursor    (int x, int y);
        static void change_colour (
                const char *ATR,
                const char *CT,
                const char *CF);
        static void clear         ();
        static void refresh       ();
        static void write_char    (chtype c);
        static void write_str     (const char *str);
        static void horz_line     (int minX, int maxX, int Y, chtype ch);
        static void vert_line     (int minY, int maxY, int X, chtype ch);
        static void frame         (int minX, int minY, int maxX,
                                   int maxY, chtype ch);
        static void box           (int minX, int minY, int maxX,
                                   int maxY, chtype ch);
        static int cursor_x       ();
        static int cursor_y       ();
        static int size_x         ();
        static int size_y         ();


        // Keyboard primitives
        static int get_key ();

    private:
        static int cursor_x_, cursor_y_;  // Screen cursor position.
        static int size_x_, size_y_;      // Screen sizes.
        static int colour[8][8];          // Colour array.

        static void validate_cursor ();   // Validates cursor position.

}; // class Terminal

#endif
