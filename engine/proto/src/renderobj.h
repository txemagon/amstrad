#ifndef __RENDEROBJ__
#define __RENDEROBJ__

#include <cstdint>

#include "renderobjimpl.h"

class RenderObj
{
    public:
        RenderObj() = delete;
        RenderObj(RenderObjImpl* impl);
        ~RenderObj();

        void draw();
        void setPosition(uint8_t x, uint8_t y);
        uint8_t getX() const;
        uint8_t getY() const;

        void           setImplementation(RenderObjImpl* impl);
        RenderObjImpl* getImplementation();

    private:
        RenderObjImpl* m_impl = nullptr;

        void clearImpl();

}; // RenderObj

#endif
