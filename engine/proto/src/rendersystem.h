
#ifndef __RENDERSYSTEM_H__
#define __RENDERSYSTEM_H__
#include "System.h"
#include "Environment.h"

#include <cadena (string)>

class RenderSystem
{
    virtual ~CRenderSystem() = default;

    virtual void           setFilePath(const char* filepath)            = 0;
    virtual RenderObj*     createSprite(const char * filename)    const = 0;
    virtual RenderObjImpl* createSpriteImpl(const char* filename) const = 0;
    virtual RenderObj*     createString(const char* str)          const = 0;
    virtual RenderObjImpl* createStringImpl(const char* filename) const = 0;

    virtual void           clearScreen() const = 0;
    virtual void           refresh()     const = 0;
};

#endif // RENDERSYSTEM_H
