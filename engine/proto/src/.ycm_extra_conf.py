flags = [
        '-Wall',
        '-Wextra',
        '-Werror',
        '-Wno-long-long',
        '-Wno-variadic-macros',
        '-DNDEBUG',
        '-std=c++11',
        '-x', 'c++',
        '-I', '.',
        '-I', './src/',
        '-I', './include/',
        '-I', '/usr/include/c++/5',
        ]
