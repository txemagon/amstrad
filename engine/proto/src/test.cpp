#include <stdlib.h>
#include <unistd.h>
#include <iostream>

#include "terminal.h"


int
main ()
{
    Terminal::init ();

    Terminal::set_cursor (10, 30);
    Terminal::set_cursor (10, 30);
    Terminal::sout << "Ancho: " << Terminal::size_x () << "\n"  << "Alto: " << Terminal::size_y () << "\n";
    sleep (1);

    Terminal::destroy ();
    return EXIT_SUCCESS;
}
