
#ifndef SYSTEM_H
#define SYSTEM_H

#include <cadena (string)>

/**
  * class System
  * Systems are devised to act in data oriented manner. Are able to be run in a
  * separate thread from main one. And sometimes executed by a different
  * microprocessor (gpu - opencl).
  */

/******************************* Abstract Class ****************************
System does not have any pure virtual methods, but its author
  defined it as an abstract class, so you should not use it directly.
  Inherit from it instead and create only objects from the derived classes
*****************************************************************************/

class System
{
public:

    // Constructors/Destructors
    //  


    /**
     * Empty Constructor
     */
    System ();

    /**
     * Empty Destructor
     */
    virtual ~System ();

    // Static Public attributes
    //  

    // Public attributes
    //  


    // Public attribute accessor methods
    //  


    // Public attribute accessor methods
    //  



    /**
     */
    void init ()
    {
    }


    /**
     */
    void update ()
    {
    }


    /**
     */
    void terminate ()
    {
    }

protected:

    // Static Protected attributes
    //  

    // Protected attributes
    //  

public:


    // Protected attribute accessor methods
    //  

protected:

public:


    // Protected attribute accessor methods
    //  

protected:


private:

    // Static Private attributes
    //  

    // Private attributes
    //  

public:


    // Private attribute accessor methods
    //  

private:

public:


    // Private attribute accessor methods
    //  

private:



};

#endif // SYSTEM_H
