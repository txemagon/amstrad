
#ifndef RENDERMANAGER_H
#define RENDERMANAGER_H

#include <cadena (string)>

/**
  * class RenderManager
  * 
  */

class RenderManager
{
public:

    // Constructors/Destructors
    //  


    /**
     * Empty Constructor
     */
    RenderManager ();

    /**
     * Empty Destructor
     */
    virtual ~RenderManager ();

    // Static Public attributes
    //  

    // Public attributes
    //  


    // Public attribute accessor methods
    //  


    // Public attribute accessor methods
    //  


protected:

    // Static Protected attributes
    //  

    // Protected attributes
    //  

public:


    // Protected attribute accessor methods
    //  

protected:

public:


    // Protected attribute accessor methods
    //  

protected:


private:

    // Static Private attributes
    //  

    // Private attributes
    //  

    // List of available render systems.
    RenderSystem * [] render_system;
public:


    // Private attribute accessor methods
    //  

private:

public:


    // Private attribute accessor methods
    //  


    /**
     * Set the value of render_system
     * List of available render systems.
     * @param new_var the new value of render_system
     */
    void setRender_system (RenderSystem * [] new_var)     {
            render_system = new_var;
    }

    /**
     * Get the value of render_system
     * List of available render systems.
     * @return the value of render_system
     */
    RenderSystem * [] getRender_system ()     {
        return render_system;
    }
private:


    void initAttributes () ;

};

#endif // RENDERMANAGER_H
