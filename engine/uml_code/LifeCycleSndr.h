
#ifndef LIFECYCLESNDR_H
#define LIFECYCLESNDR_H

#include <cadena (string)>

/**
  * class LifeCycleSndr
  * 
  */

class LifeCycleSndr
{
public:

    // Constructors/Destructors
    //  


    /**
     * Empty Constructor
     */
    LifeCycleSndr ();

    /**
     * Empty Destructor
     */
    virtual ~LifeCycleSndr ();

    // Static Public attributes
    //  

    // Public attributes
    //  


    // Public attribute accessor methods
    //  


    // Public attribute accessor methods
    //  



    /**
     * @param  id
     */
    void notice_deleted (uint id)
    {
    }

protected:

    // Static Protected attributes
    //  

    // Protected attributes
    //  

public:


    // Protected attribute accessor methods
    //  

protected:

public:


    // Protected attribute accessor methods
    //  

protected:


private:

    // Static Private attributes
    //  

    // Private attributes
    //  

    LifeCycleRcv * port;
public:


    // Private attribute accessor methods
    //  

private:

public:


    // Private attribute accessor methods
    //  


    /**
     * Set the value of port
     * @param new_var the new value of port
     */
    void setPort (LifeCycleRcv * new_var)     {
            port = new_var;
    }

    /**
     * Get the value of port
     * @return the value of port
     */
    LifeCycleRcv * getPort ()     {
        return port;
    }
private:


    void initAttributes () ;

};

#endif // LIFECYCLESNDR_H
