
#ifndef PLAYER_H
#define PLAYER_H
#include "Agent.h"

#include <cadena (string)>
#include vector



/**
  * class Player
  * 
  */

class Player : public Agent
{
public:

    // Constructors/Destructors
    //  


    /**
     * Empty Constructor
     */
    Player ();

    /**
     * Empty Destructor
     */
    virtual ~Player ();

    // Static Public attributes
    //  

    // Public attributes
    //  


    // Public attribute accessor methods
    //  


    // Public attribute accessor methods
    //  


protected:

    // Static Protected attributes
    //  

    // Protected attributes
    //  

public:


    // Protected attribute accessor methods
    //  

protected:

public:


    // Protected attribute accessor methods
    //  

protected:


private:

    // Static Private attributes
    //  

    // Private attributes
    //  

public:


    // Private attribute accessor methods
    //  

private:

public:


    // Private attribute accessor methods
    //  

private:



};

#endif // PLAYER_H
