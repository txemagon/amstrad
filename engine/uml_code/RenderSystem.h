
#ifndef RENDERSYSTEM_H
#define RENDERSYSTEM_H
#include "System.h"
#include "Environment.h"

#include <cadena (string)>

/**
  * class RenderSystem
  * Calls render method for all visible and nearby GameObjects. Pending to decide
  * which kind of rendering will be. Support for multiple windows on the same scene
  * stiill not commited.
  */

/******************************* Abstract Class ****************************
RenderSystem does not have any pure virtual methods, but its author
  defined it as an abstract class, so you should not use it directly.
  Inherit from it instead and create only objects from the derived classes
*****************************************************************************/

class RenderSystem : virtual public System, public Environment
{
public:

    // Constructors/Destructors
    //  


    /**
     * Empty Constructor
     */
    RenderSystem ();

    /**
     * Empty Destructor
     */
    virtual ~RenderSystem ();

    // Static Public attributes
    //  

    // Public attributes
    //  


    // Public attribute accessor methods
    //  


    // Public attribute accessor methods
    //  


protected:

    // Static Protected attributes
    //  

    // Protected attributes
    //  

public:


    // Protected attribute accessor methods
    //  

protected:

public:


    // Protected attribute accessor methods
    //  

protected:


private:

    // Static Private attributes
    //  

    // Private attributes
    //  

public:


    // Private attribute accessor methods
    //  

private:

public:


    // Private attribute accessor methods
    //  

private:



};

#endif // RENDERSYSTEM_H
