
#ifndef SDLRENDERSYSTEM_H
#define SDLRENDERSYSTEM_H
#include "RenderSystem.h"

#include <cadena (string)>

/**
  * class SDLRenderSystem
  * 
  */

class SDLRenderSystem : virtual public RenderSystem
{
public:

    // Constructors/Destructors
    //  


    /**
     * Empty Constructor
     */
    SDLRenderSystem ();

    /**
     * Empty Destructor
     */
    virtual ~SDLRenderSystem ();

    // Static Public attributes
    //  

    // Public attributes
    //  


    // Public attribute accessor methods
    //  


    // Public attribute accessor methods
    //  


protected:

    // Static Protected attributes
    //  

    // Protected attributes
    //  

public:


    // Protected attribute accessor methods
    //  

protected:

public:


    // Protected attribute accessor methods
    //  

protected:


private:

    // Static Private attributes
    //  

    // Private attributes
    //  

public:


    // Private attribute accessor methods
    //  

private:

public:


    // Private attribute accessor methods
    //  

private:



};

#endif // SDLRENDERSYSTEM_H
