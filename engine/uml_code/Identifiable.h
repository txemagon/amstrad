
#ifndef IDENTIFIABLE_H
#define IDENTIFIABLE_H

#include <cadena (string)>

/**
  * class Identifiable
  * 
  */

class Identifiable
{
public:

    // Constructors/Destructors
    //  


    /**
     * Empty Constructor
     */
    Identifiable ();

    /**
     * Empty Destructor
     */
    virtual ~Identifiable ();

    // Static Public attributes
    //  

    // Public attributes
    //  


    // Public attribute accessor methods
    //  


    // Public attribute accessor methods
    //  


protected:

    // Static Protected attributes
    //  

    // Protected attributes
    //  

public:


    // Protected attribute accessor methods
    //  

protected:

public:


    // Protected attribute accessor methods
    //  

protected:


private:

    // Static Private attributes
    //  

    // Private attributes
    //  

    // id of the game object as hold by the entity manager.
    uint id;
public:


    // Private attribute accessor methods
    //  

private:

public:


    // Private attribute accessor methods
    //  


    /**
     * Set the value of id
     * id of the game object as hold by the entity manager.
     * @param new_var the new value of id
     */
    void setId (uint new_var)     {
            id = new_var;
    }

    /**
     * Get the value of id
     * id of the game object as hold by the entity manager.
     * @return the value of id
     */
    uint getId ()     {
        return id;
    }
private:


    void initAttributes () ;

};

#endif // IDENTIFIABLE_H
