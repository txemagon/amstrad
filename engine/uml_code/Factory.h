
#ifndef FACTORY_H
#define FACTORY_H

#include <cadena (string)>

/**
  * class Factory
  * Creates player, enemies and scene objects.
  */

class Factory
{
public:

    // Constructors/Destructors
    //  


    /**
     * Empty Constructor
     */
    Factory ();

    /**
     * Empty Destructor
     */
    virtual ~Factory ();

    // Static Public attributes
    //  

    // Public attributes
    //  


    // Public attribute accessor methods
    //  


    // Public attribute accessor methods
    //  


protected:

    // Static Protected attributes
    //  

    // Protected attributes
    //  

public:


    // Protected attribute accessor methods
    //  

protected:

public:


    // Protected attribute accessor methods
    //  

protected:


private:

    // Static Private attributes
    //  

    // Private attributes
    //  

public:


    // Private attribute accessor methods
    //  

private:

public:


    // Private attribute accessor methods
    //  

private:



};

#endif // FACTORY_H
