
#ifndef RENDERABLE_T_H
#define RENDERABLE_T_H

#include <cadena (string)>

/**
  * class Renderable_t
  * 
  */

class Renderable_t
{
public:

    // Constructors/Destructors
    //  


    /**
     * Empty Constructor
     */
    Renderable_t ();

    /**
     * Empty Destructor
     */
    virtual ~Renderable_t ();

    // Static Public attributes
    //  

    // Public attributes
    //  

    uint8_t sx;
    uint8_t sy;
    uint8_t pspr;

    // Public attribute accessor methods
    //  


    // Public attribute accessor methods
    //  


    /**
     * Set the value of sx
     * @param new_var the new value of sx
     */
    void setSx (uint8_t new_var)     {
            sx = new_var;
    }

    /**
     * Get the value of sx
     * @return the value of sx
     */
    uint8_t getSx ()     {
        return sx;
    }

    /**
     * Set the value of sy
     * @param new_var the new value of sy
     */
    void setSy (uint8_t new_var)     {
            sy = new_var;
    }

    /**
     * Get the value of sy
     * @return the value of sy
     */
    uint8_t getSy ()     {
        return sy;
    }

    /**
     * Set the value of pspr
     * @param new_var the new value of pspr
     */
    void setPspr (uint8_t new_var)     {
            pspr = new_var;
    }

    /**
     * Get the value of pspr
     * @return the value of pspr
     */
    uint8_t getPspr ()     {
        return pspr;
    }

protected:

    // Static Protected attributes
    //  

    // Protected attributes
    //  

public:


    // Protected attribute accessor methods
    //  

protected:

public:


    // Protected attribute accessor methods
    //  

protected:


private:

    // Static Private attributes
    //  

    // Private attributes
    //  

public:


    // Private attribute accessor methods
    //  

private:

public:


    // Private attribute accessor methods
    //  

private:


    void initAttributes () ;

};

#endif // RENDERABLE_T_H
