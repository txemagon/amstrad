
#ifndef INPUTMANAGER_H
#define INPUTMANAGER_H

#include <cadena (string)>
#include vector



/**
  * class InputManager
  * Holds the current state of inputs. Is event driven and not attached to game
  * thread (asynchronous).
  */

class InputManager
{
public:

    // Constructors/Destructors
    //  


    /**
     * Empty Constructor
     */
    InputManager ();

    /**
     * Empty Destructor
     */
    virtual ~InputManager ();

    // Static Public attributes
    //  

    // Public attributes
    //  


    // Public attribute accessor methods
    //  


    // Public attribute accessor methods
    //  


protected:

    // Static Protected attributes
    //  

    // Protected attributes
    //  

public:


    // Protected attribute accessor methods
    //  

protected:

public:


    // Protected attribute accessor methods
    //  

protected:


private:

    // Static Private attributes
    //  

    // Private attributes
    //  

public:


    // Private attribute accessor methods
    //  

private:

public:


    // Private attribute accessor methods
    //  

private:



};

#endif // INPUTMANAGER_H
