
#ifndef BRAIN_H
#define BRAIN_H
#include "GameObject.h"

#include <cadena (string)>
#include vector



/**
  * class Brain
  * 
  */

class Brain : public GameObject
{
public:

    // Constructors/Destructors
    //  


    /**
     * Empty Constructor
     */
    Brain ();

    /**
     * Empty Destructor
     */
    virtual ~Brain ();

    // Static Public attributes
    //  

    // Public attributes
    //  


    // Public attribute accessor methods
    //  


    // Public attribute accessor methods
    //  


protected:

    // Static Protected attributes
    //  

    // Protected attributes
    //  

public:


    // Protected attribute accessor methods
    //  

protected:

public:


    // Protected attribute accessor methods
    //  

protected:


private:

    // Static Private attributes
    //  

    // Private attributes
    //  

    Behaviour [] behaviour;
public:


    // Private attribute accessor methods
    //  

private:

public:


    // Private attribute accessor methods
    //  


    /**
     * Set the value of behaviour
     * @param new_var the new value of behaviour
     */
    void setBehaviour (Behaviour [] new_var)     {
            behaviour = new_var;
    }

    /**
     * Get the value of behaviour
     * @return the value of behaviour
     */
    Behaviour [] getBehaviour ()     {
        return behaviour;
    }
private:


    void initAttributes () ;

};

#endif // BRAIN_H
