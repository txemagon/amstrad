
#ifndef SIMULATIONMANAGER_H
#define SIMULATIONMANAGER_H

#include <cadena (string)>

/**
  * class SimulationManager
  * 
  */

class SimulationManager
{
public:

    // Constructors/Destructors
    //  


    /**
     * Empty Constructor
     */
    SimulationManager ();

    /**
     * Empty Destructor
     */
    virtual ~SimulationManager ();

    // Static Public attributes
    //  

    // Public attributes
    //  


    // Public attribute accessor methods
    //  


    // Public attribute accessor methods
    //  


protected:

    // Static Protected attributes
    //  

    // Protected attributes
    //  

public:


    // Protected attribute accessor methods
    //  

protected:

public:


    // Protected attribute accessor methods
    //  

protected:


private:

    // Static Private attributes
    //  

    // Private attributes
    //  

    InputManager inputs;
    MenuManager menu;
    SceneManager phase;
public:


    // Private attribute accessor methods
    //  

private:

public:


    // Private attribute accessor methods
    //  


    /**
     * Set the value of inputs
     * @param new_var the new value of inputs
     */
    void setInputs (InputManager new_var)     {
            inputs = new_var;
    }

    /**
     * Get the value of inputs
     * @return the value of inputs
     */
    InputManager getInputs ()     {
        return inputs;
    }

    /**
     * Set the value of menu
     * @param new_var the new value of menu
     */
    void setMenu (MenuManager new_var)     {
            menu = new_var;
    }

    /**
     * Get the value of menu
     * @return the value of menu
     */
    MenuManager getMenu ()     {
        return menu;
    }

    /**
     * Set the value of phase
     * @param new_var the new value of phase
     */
    void setPhase (SceneManager new_var)     {
            phase = new_var;
    }

    /**
     * Get the value of phase
     * @return the value of phase
     */
    SceneManager getPhase ()     {
        return phase;
    }
private:


    void initAttributes () ;

};

#endif // SIMULATIONMANAGER_H
