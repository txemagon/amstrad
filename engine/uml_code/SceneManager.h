
#ifndef SCENEMANAGER_H
#define SCENEMANAGER_H
#include "GameObject.h"

#include <cadena (string)>

/**
  * class SceneManager
  * 
  */

class SceneManager : public GameObject
{
public:

    // Constructors/Destructors
    //  


    /**
     * Empty Constructor
     */
    SceneManager ();

    /**
     * Empty Destructor
     */
    virtual ~SceneManager ();

    // Static Public attributes
    //  

    // Public attributes
    //  

    System* systems;

    // Public attribute accessor methods
    //  


    // Public attribute accessor methods
    //  


    /**
     * Set the value of systems
     * @param new_var the new value of systems
     */
    void setSystems (System* new_var)     {
            systems = new_var;
    }

    /**
     * Get the value of systems
     * @return the value of systems
     */
    System* getSystems ()     {
        return systems;
    }


    /**
     */
    void init ()
    {
    }


    /**
     */
    void run ()
    {
    }

protected:

    // Static Protected attributes
    //  

    // Protected attributes
    //  

public:


    // Protected attribute accessor methods
    //  

protected:

public:


    // Protected attribute accessor methods
    //  

protected:


private:

    // Static Private attributes
    //  

    // Private attributes
    //  

    Factory factory;
public:


    // Private attribute accessor methods
    //  

private:

public:


    // Private attribute accessor methods
    //  


    /**
     * Set the value of factory
     * @param new_var the new value of factory
     */
    void setFactory (Factory new_var)     {
            factory = new_var;
    }

    /**
     * Get the value of factory
     * @return the value of factory
     */
    Factory getFactory ()     {
        return factory;
    }
private:



    /**
     */
    void update ()
    {
    }


    /**
     */
    void render ()
    {
    }


    /**
     */
    void terminate ()
    {
    }

    void initAttributes () ;

};

#endif // SCENEMANAGER_H
