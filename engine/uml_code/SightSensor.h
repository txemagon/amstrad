
#ifndef SIGHTSENSOR_H
#define SIGHTSENSOR_H
#include "Sensor.h"

#include <cadena (string)>

/**
  * class SightSensor
  * Every object retains sight capabilities for the agent in which is implanted.
  */

class SightSensor : public Sensor
{
public:

    // Constructors/Destructors
    //  


    /**
     * Empty Constructor
     */
    SightSensor ();

    /**
     * Empty Destructor
     */
    virtual ~SightSensor ();

    // Static Public attributes
    //  

    // Public attributes
    //  


    // Public attribute accessor methods
    //  


    // Public attribute accessor methods
    //  


protected:

    // Static Protected attributes
    //  

    // Protected attributes
    //  

public:


    // Protected attribute accessor methods
    //  

protected:

public:


    // Protected attribute accessor methods
    //  

protected:


private:

    // Static Private attributes
    //  

    // Private attributes
    //  

public:


    // Private attribute accessor methods
    //  

private:

public:


    // Private attribute accessor methods
    //  

private:



};

#endif // SIGHTSENSOR_H
