
#ifndef BOT_H
#define BOT_H
#include "Agent.h"

#include <cadena (string)>

/**
  * class Bot
  * 
  */

class Bot : public Agent
{
public:

    // Constructors/Destructors
    //  


    /**
     * Empty Constructor
     */
    Bot ();

    /**
     * Empty Destructor
     */
    virtual ~Bot ();

    // Static Public attributes
    //  

    // Public attributes
    //  


    // Public attribute accessor methods
    //  


    // Public attribute accessor methods
    //  


protected:

    // Static Protected attributes
    //  

    // Protected attributes
    //  

    Brain brain;
public:


    // Protected attribute accessor methods
    //  

protected:

public:


    // Protected attribute accessor methods
    //  


    /**
     * Set the value of brain
     * @param new_var the new value of brain
     */
    void setBrain (Brain new_var)     {
            brain = new_var;
    }

    /**
     * Get the value of brain
     * @return the value of brain
     */
    Brain getBrain ()     {
        return brain;
    }
protected:


private:

    // Static Private attributes
    //  

    // Private attributes
    //  

public:


    // Private attribute accessor methods
    //  

private:

public:


    // Private attribute accessor methods
    //  

private:


    void initAttributes () ;

};

#endif // BOT_H
