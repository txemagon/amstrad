
#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H
#include "LifeCycleSndr.h"
#include "Renderable.h"
#include "Identifiable.h"

#include <cadena (string)>

/**
  * class GameObject
  * Any kind of object or group of the which can be rendered on the game.
  */

class GameObject : public LifeCycleSndr, public Renderable, public Identifiable
{
public:

    // Constructors/Destructors
    //  


    /**
     * Empty Constructor
     */
    GameObject ();

    /**
     * Empty Destructor
     */
    virtual ~GameObject ();

    // Static Public attributes
    //  

    // Public attributes
    //  


    // Public attribute accessor methods
    //  


    // Public attribute accessor methods
    //  


protected:

    // Static Protected attributes
    //  

    // Protected attributes
    //  

public:


    // Protected attribute accessor methods
    //  

protected:

public:


    // Protected attribute accessor methods
    //  

protected:


private:

    // Static Private attributes
    //  

    // Private attributes
    //  

public:


    // Private attribute accessor methods
    //  

private:

public:


    // Private attribute accessor methods
    //  

private:



};

#endif // GAMEOBJECT_H
