
#ifndef COORDINATES_T_H
#define COORDINATES_T_H

#include <cadena (string)>

/**
  * class Coordinates_t
  * 
  */

class Coordinates_t
{
public:

    // Constructors/Destructors
    //  


    /**
     * Empty Constructor
     */
    Coordinates_t ();

    /**
     * Empty Destructor
     */
    virtual ~Coordinates_t ();

    // Static Public attributes
    //  

    // Public attributes
    //  

    double x;
    double y;
    double z;

    // Public attribute accessor methods
    //  


    // Public attribute accessor methods
    //  


    /**
     * Set the value of x
     * @param new_var the new value of x
     */
    void setX (double new_var)     {
            x = new_var;
    }

    /**
     * Get the value of x
     * @return the value of x
     */
    double getX ()     {
        return x;
    }

    /**
     * Set the value of y
     * @param new_var the new value of y
     */
    void setY (double new_var)     {
            y = new_var;
    }

    /**
     * Get the value of y
     * @return the value of y
     */
    double getY ()     {
        return y;
    }

    /**
     * Set the value of z
     * @param new_var the new value of z
     */
    void setZ (double new_var)     {
            z = new_var;
    }

    /**
     * Get the value of z
     * @return the value of z
     */
    double getZ ()     {
        return z;
    }

protected:

    // Static Protected attributes
    //  

    // Protected attributes
    //  

public:


    // Protected attribute accessor methods
    //  

protected:

public:


    // Protected attribute accessor methods
    //  

protected:


private:

    // Static Private attributes
    //  

    // Private attributes
    //  

public:


    // Private attribute accessor methods
    //  

private:

public:


    // Private attribute accessor methods
    //  

private:


    void initAttributes () ;

};

#endif // COORDINATES_T_H
