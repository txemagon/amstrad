
#ifndef AI_H
#define AI_H

#include <cadena (string)>

/**
  * class AI
  * 
  */

class AI
{
public:

    // Constructors/Destructors
    //  


    /**
     * Empty Constructor
     */
    AI ();

    /**
     * Empty Destructor
     */
    virtual ~AI ();

    // Static Public attributes
    //  

    // Public attributes
    //  


    // Public attribute accessor methods
    //  


    // Public attribute accessor methods
    //  



    /**
     * @return Coordinates_t
     */
    Coordinates_t decide ()
    {
    }

protected:

    // Static Protected attributes
    //  

    // Protected attributes
    //  

public:


    // Protected attribute accessor methods
    //  

protected:

public:


    // Protected attribute accessor methods
    //  

protected:


private:

    // Static Private attributes
    //  

    // Private attributes
    //  

public:


    // Private attribute accessor methods
    //  

private:

public:


    // Private attribute accessor methods
    //  

private:



};

#endif // AI_H
