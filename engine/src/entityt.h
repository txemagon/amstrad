
#ifndef ENTITY_T_H
#define ENTITY_T_H

#include <cadena (string)>
#include vector



/**
  * class Entity_t
  * 
  */

class Entity_t
{
public:

    // Constructors/Destructors
    //  


    /**
     * Empty Constructor
     */
    Entity_t ();

    /**
     * Empty Destructor
     */
    virtual ~Entity_t ();

    // Static Public attributes
    //  

    // Public attributes
    //  

    Entity_t_enum type;
    Renderable_t image;

    // Public attribute accessor methods
    //  


    // Public attribute accessor methods
    //  


    /**
     * Set the value of type
     * @param new_var the new value of type
     */
    void setType (Entity_t_enum new_var)     {
            type = new_var;
    }

    /**
     * Get the value of type
     * @return the value of type
     */
    Entity_t_enum getType ()     {
        return type;
    }

    /**
     * Set the value of image
     * @param new_var the new value of image
     */
    void setImage (Renderable_t new_var)     {
            image = new_var;
    }

    /**
     * Get the value of image
     * @return the value of image
     */
    Renderable_t getImage ()     {
        return image;
    }

protected:

    // Static Protected attributes
    //  

    // Protected attributes
    //  

public:


    // Protected attribute accessor methods
    //  

protected:

public:


    // Protected attribute accessor methods
    //  

protected:


private:

    // Static Private attributes
    //  

    // Private attributes
    //  

    uint id;
    Coordinates_t position;
    Coordinates_t velocity;
    Coordinates_t acceleration;
public:


    // Private attribute accessor methods
    //  

private:

public:


    // Private attribute accessor methods
    //  


    /**
     * Set the value of id
     * @param new_var the new value of id
     */
    void setId (uint new_var)     {
            id = new_var;
    }

    /**
     * Get the value of id
     * @return the value of id
     */
    uint getId ()     {
        return id;
    }

    /**
     * Set the value of position
     * @param new_var the new value of position
     */
    void setPosition (Coordinates_t new_var)     {
            position = new_var;
    }

    /**
     * Get the value of position
     * @return the value of position
     */
    Coordinates_t getPosition ()     {
        return position;
    }

    /**
     * Set the value of velocity
     * @param new_var the new value of velocity
     */
    void setVelocity (Coordinates_t new_var)     {
            velocity = new_var;
    }

    /**
     * Get the value of velocity
     * @return the value of velocity
     */
    Coordinates_t getVelocity ()     {
        return velocity;
    }

    /**
     * Set the value of acceleration
     * @param new_var the new value of acceleration
     */
    void setAcceleration (Coordinates_t new_var)     {
            acceleration = new_var;
    }

    /**
     * Get the value of acceleration
     * @return the value of acceleration
     */
    Coordinates_t getAcceleration ()     {
        return acceleration;
    }
private:


    void initAttributes () ;

};

#endif // ENTITY_T_H
