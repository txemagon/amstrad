#include "renderobj.h"

RenderObj::RenderObj(RenderObjImpl* impl) {
    setImplementation(impl);
}

RenderObj::~RenderObj() {
    clearImpl();
}

void
RenderObj::clearImpl() {
    if (m_impl) {
        delete m_impl;
        m_impl = nullptr;
    }
}

void
RenderObj::setImplementation(RenderObjImpl* impl) {
    if (!impl)
        throw std::logic_error("Null pointer for setImplementation");
    clearImpl();
    m_impl = impl;
}

RenderObjImpl*
RenderObj::getImplementation() {
    return m_impl;
}

void
RenderObj::draw() { mimpl->draw(); }

void
RenderObj::setPosition(uint8_t x, uint8_t y) {
    m_impl->setPosition(x, y);
}

uint8_t RenderObj::getX() const ( return m_impl->getX(); )
uint8_t RenderObj::getY() const ( return m_impl->getY(); )

