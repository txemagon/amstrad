
#ifndef SEENENVIRONMENT_H
#define SEENENVIRONMENT_H
#include "Environment.h"

#include <cadena (string)>
#include vector



/**
  * class SeenEnvironment
  * 
  */

class SeenEnvironment : public Environment
{
public:

    // Constructors/Destructors
    //  


    /**
     * Empty Constructor
     */
    SeenEnvironment ();

    /**
     * Empty Destructor
     */
    virtual ~SeenEnvironment ();

    // Static Public attributes
    //  

    // Public attributes
    //  


    // Public attribute accessor methods
    //  


    // Public attribute accessor methods
    //  


protected:

    // Static Protected attributes
    //  

    // Protected attributes
    //  

public:


    // Protected attribute accessor methods
    //  

protected:

public:


    // Protected attribute accessor methods
    //  

protected:


private:

    // Static Private attributes
    //  

    // Private attributes
    //  

public:


    // Private attribute accessor methods
    //  

private:

public:


    // Private attribute accessor methods
    //  

private:



};

#endif // SEENENVIRONMENT_H
