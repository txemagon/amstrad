
#ifndef INPUTSYSTEM_H
#define INPUTSYSTEM_H
#include "System.h"

#include <cadena (string)>
#include vector



/**
  * class InputSystem
  * Joins input state into a negligible game input
  */

class InputSystem : virtual public System
{
public:

    // Constructors/Destructors
    //  


    /**
     * Empty Constructor
     */
    InputSystem ();

    /**
     * Empty Destructor
     */
    virtual ~InputSystem ();

    // Static Public attributes
    //  

    // Public attributes
    //  


    // Public attribute accessor methods
    //  


    // Public attribute accessor methods
    //  


protected:

    // Static Protected attributes
    //  

    // Protected attributes
    //  

public:


    // Protected attribute accessor methods
    //  

protected:

public:


    // Protected attribute accessor methods
    //  

protected:


private:

    // Static Private attributes
    //  

    // Private attributes
    //  

public:


    // Private attribute accessor methods
    //  

private:

public:


    // Private attribute accessor methods
    //  

private:



};

#endif // INPUTSYSTEM_H
