
#ifndef RENDERABLE_H
#define RENDERABLE_H

#include <cadena (string)>

/**
  * class Renderable
  * 
  */

class Renderable
{
public:

    // Constructors/Destructors
    //  


    /**
     * Empty Constructor
     */
    Renderable ();

    /**
     * Empty Destructor
     */
    virtual ~Renderable ();

    // Static Public attributes
    //  

    // Public attributes
    //  


    // Public attribute accessor methods
    //  


    // Public attribute accessor methods
    //  



    /**
     */
    virtual void render ()
    {
    }

protected:

    // Static Protected attributes
    //  

    // Protected attributes
    //  

public:


    // Protected attribute accessor methods
    //  

protected:

public:


    // Protected attribute accessor methods
    //  

protected:


private:

    // Static Private attributes
    //  

    // Private attributes
    //  

public:


    // Private attribute accessor methods
    //  

private:

public:


    // Private attribute accessor methods
    //  

private:



};

#endif // RENDERABLE_H
