
#ifndef PHYSICSSYSTEM_H
#define PHYSICSSYSTEM_H
#include "System.h"

#include <cadena (string)>
#include vector



/**
  * class PhysicsSystem
  * Updates positions and velocities. Time rate independent from RenderSystem and
  * AIControlSystem.
  * 
  * Exposes and octtree of nearby objects which updated while computing positions.
  * 
  * Synchronized with AISystem
  */

class PhysicsSystem : virtual public System
{
public:

    // Constructors/Destructors
    //  


    /**
     * Empty Constructor
     */
    PhysicsSystem ();

    /**
     * Empty Destructor
     */
    virtual ~PhysicsSystem ();

    // Static Public attributes
    //  

    // Public attributes
    //  


    // Public attribute accessor methods
    //  


    // Public attribute accessor methods
    //  


protected:

    // Static Protected attributes
    //  

    // Protected attributes
    //  

public:


    // Protected attribute accessor methods
    //  

protected:

public:


    // Protected attribute accessor methods
    //  

protected:


private:

    // Static Private attributes
    //  

    // Private attributes
    //  

    OctTree view;
public:


    // Private attribute accessor methods
    //  

private:

public:


    // Private attribute accessor methods
    //  


    /**
     * Set the value of view
     * @param new_var the new value of view
     */
    void setView (OctTree new_var)     {
            view = new_var;
    }

    /**
     * Get the value of view
     * @return the value of view
     */
    OctTree getView ()     {
        return view;
    }
private:


    void initAttributes () ;

};

#endif // PHYSICSSYSTEM_H
