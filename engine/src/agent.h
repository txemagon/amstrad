
#ifndef AGENT_H
#define AGENT_H
#include "GameObject.h"
#include "AI.h"

#include <cadena (string)>
#include vector



/**
  * class Agent
  * 
  */

class Agent : public GameObject, public AI
{
public:

    // Constructors/Destructors
    //  


    /**
     * Empty Constructor
     */
    Agent ();

    /**
     * Empty Destructor
     */
    virtual ~Agent ();

    // Static Public attributes
    //  

    // Public attributes
    //  


    // Public attribute accessor methods
    //  


    // Public attribute accessor methods
    //  



    /**
     * @return Coordinates_t
     * @param  input
     */
    Coordinates_t trim (Coordinates_t input)
    {
    }

protected:

    // Static Protected attributes
    //  

    // Protected attributes
    //  

public:


    // Protected attribute accessor methods
    //  

protected:

public:


    // Protected attribute accessor methods
    //  

protected:


private:

    // Static Private attributes
    //  

    // Private attributes
    //  

public:


    // Private attribute accessor methods
    //  

private:

public:


    // Private attribute accessor methods
    //  

private:



};

#endif // AGENT_H
