
#ifndef BEHAVIOUR_H
#define BEHAVIOUR_H
#include "Brain.h"

#include <cadena (string)>
#include vector



/**
  * class Behaviour
  * 
  */

class Behaviour : public Brain
{
public:

    // Constructors/Destructors
    //  


    /**
     * Empty Constructor
     */
    Behaviour ();

    /**
     * Empty Destructor
     */
    virtual ~Behaviour ();

    // Static Public attributes
    //  

    // Public attributes
    //  


    // Public attribute accessor methods
    //  


    // Public attribute accessor methods
    //  


protected:

    // Static Protected attributes
    //  

    // Protected attributes
    //  

public:


    // Protected attribute accessor methods
    //  

protected:

public:


    // Protected attribute accessor methods
    //  

protected:


private:

    // Static Private attributes
    //  

    // Private attributes
    //  

public:


    // Private attribute accessor methods
    //  

private:

public:


    // Private attribute accessor methods
    //  

private:



};

#endif // BEHAVIOUR_H
