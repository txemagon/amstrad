
#ifndef LIFECYCLERCV_H
#define LIFECYCLERCV_H

#include <cadena (string)>

/**
  * class LifeCycleRcv
  * 
  */

class LifeCycleRcv
{
public:

    // Constructors/Destructors
    //  


    /**
     * Empty Constructor
     */
    LifeCycleRcv ();

    /**
     * Empty Destructor
     */
    virtual ~LifeCycleRcv ();

    // Static Public attributes
    //  

    // Public attributes
    //  


    // Public attribute accessor methods
    //  


    // Public attribute accessor methods
    //  



    /**
     * Adds a new entity provided new data.
     * @param  new_entity New Entitiy to be created.
     */
    virtual void add (Entity_t new_entity)
    {
    }


    /**
     * Deletes an entity given an entity id.
     * @param  deleted_entity id of the entity to be deleted.
     */
    virtual void delete (uint deleted_entity)
    {
    }

protected:

    // Static Protected attributes
    //  

    // Protected attributes
    //  

    GameObject *[] listeners_deleted;
public:


    // Protected attribute accessor methods
    //  

protected:

public:


    // Protected attribute accessor methods
    //  


    /**
     * Set the value of listeners_deleted
     * @param new_var the new value of listeners_deleted
     */
    void setListeners_deleted (GameObject *[] new_var)     {
            listeners_deleted = new_var;
    }

    /**
     * Get the value of listeners_deleted
     * @return the value of listeners_deleted
     */
    GameObject *[] getListeners_deleted ()     {
        return listeners_deleted;
    }
protected:


private:

    // Static Private attributes
    //  

    // Private attributes
    //  

public:


    // Private attribute accessor methods
    //  

private:

public:


    // Private attribute accessor methods
    //  

private:



    /**
     * Cast to all listeners the id of the entity deleted.
     * @param  id id of the entity just deleted.
     */
    void cast_deleted (uint id)
    {
    }

    void initAttributes () ;

};

#endif // LIFECYCLERCV_H
