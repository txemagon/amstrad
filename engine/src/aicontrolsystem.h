
#ifndef AICONTROLSYSTEM_H
#define AICONTROLSYSTEM_H
#include "System.h"

#include <cadena (string)>

/**
  * class AIControlSystem
  * Gathers desired accelerations from agents in temporary array. Trims inputs with
  * Boids capabilities.
  * 
  * Exposes final desired accelerations to the physics system by overwriting
  * accelerations to the entity.
  */

class AIControlSystem : virtual public System
{
public:

    // Constructors/Destructors
    //  


    /**
     * Empty Constructor
     */
    AIControlSystem ();

    /**
     * Empty Destructor
     */
    virtual ~AIControlSystem ();

    // Static Public attributes
    //  

    // Public attributes
    //  


    // Public attribute accessor methods
    //  


    // Public attribute accessor methods
    //  


protected:

    // Static Protected attributes
    //  

    // Protected attributes
    //  

public:


    // Protected attribute accessor methods
    //  

protected:

public:


    // Protected attribute accessor methods
    //  

protected:


private:

    // Static Private attributes
    //  

    // Private attributes
    //  

public:


    // Private attribute accessor methods
    //  

private:

public:


    // Private attribute accessor methods
    //  

private:



};

#endif // AICONTROLSYSTEM_H
