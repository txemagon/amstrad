
#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H

#include <cadena (string)>

/**
  * class Environment
  * 
  */

class Environment
{
public:

    // Constructors/Destructors
    //  


    /**
     * Empty Constructor
     */
    Environment ();

    /**
     * Empty Destructor
     */
    virtual ~Environment ();

    // Static Public attributes
    //  

    // Public attributes
    //  


    // Public attribute accessor methods
    //  


    // Public attribute accessor methods
    //  



    /**
     * Returns a NULL terminated list of Entities in the same quadrant.
     * @return Entity_t *
     * @param  position
     */
    Entity_t * get (Coordinates_t position)
    {
    }

protected:

    // Static Protected attributes
    //  

    // Protected attributes
    //  

public:


    // Protected attribute accessor methods
    //  

protected:

public:


    // Protected attribute accessor methods
    //  

protected:


private:

    // Static Private attributes
    //  

    // Private attributes
    //  

public:


    // Private attribute accessor methods
    //  

private:

public:


    // Private attribute accessor methods
    //  

private:



};

#endif // ENVIRONMENT_H
