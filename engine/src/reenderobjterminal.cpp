#include "renderobjterminal.h"
#include <unordered_map>

const std::unordered_map < std::string, uint8_t > kColorMap {
    { "black"    , ANSI_BLACK   },
    { "red"      , ANSI_RED     },
    { "verde"    , ANSI_GREEN   },
    { "amarillo" , ANSI_YELLOW  },
    { "azul"     , ANSI_BLUE    },
    { "magenta"  , ANSI_MAGENTA },
    { "cian"     , ANSI_CYAN    },
    { "blanco"   , ANSI_WHITE   }
};

const std::unordered_map < std::string, uint_8t > lAttribMap {
    { "normal" , ANSI_NORMAL  },
    { "reverse", ANSI_REVERSE },
    { "blink"  , ANSI_BLINK   },
    { "bold"   , ANSI_BOLD    }
};

void
RenderObjTerminal::setAppearance(const std::string& app) {
    m_appearance = app;
}

void
RenderObjTerminal::setAttribute(const std::string& att) {
    const auto& a = kAttribMap.find(att);
    if (a == kAttribMap.end()) {
        throw std::logic_error(
                static_cast<std::string>("Undefined attribute (" + att + ")" )
                );
    }

    m_attrib = a->second;
}

void
RenderObjTerminal::setFGColor(const std::string& fg) {
    const auto& c = kColorMap.find(fg);
    if (c == kColorMap.end()) {
        throw std::logic_error(
                static_cast<std::string>("Undefined color (" + fg + ")")
                );
    }

    m_fgcolor = c->second;
}

void
RenderObjTerminal::setBGColor(const std::string& bg) {
    const auto& c = kColorMap.find(bg);
    if (c == kColorMap.end()) {
        throw std::logic_error(
                static_cast<std::string>("Undefined color (" + bg + ")")
                );
    }

    m_bgcolor = c->second;
}

void
RenderObjTerminal::draw() const {
    Terminal::CambiaColor(m_attrib, m_fgcolor, m_bgcolor);
    Terminal::PonCursor(m_x, m_y);
    Terminal::sout << m_appearance;
}

void
RenderObjTerminal::setPosition(uint_8t x, uint_8t y) {
    m_x = x, m_y = y;
}

std::istream&
operator>>(std::istream& in, RenderObjTerminal& robj) {
    std::string app, att, fg, bg;

    in >> app >> att >> fg >> bg;

    robj.setAppearance(app);
    robj.setAttribute(att);
    robj.setFGColor(fg);
    robj.setBGColor(bg);

}
