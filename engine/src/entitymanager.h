
#ifndef ENTITYMANAGER_H
#define ENTITYMANAGER_H
#include "LifeCycleRcv.h"

#include <cadena (string)>
#include vector



/**
  * class EntityManager
  * EntityManager disengages POO from data to make systems run.
  */

class EntityManager : virtual public LifeCycleRcv
{
public:

    // Constructors/Destructors
    //  


    /**
     * Empty Constructor
     */
    EntityManager ();

    /**
     * Empty Destructor
     */
    virtual ~EntityManager ();

    // Static Public attributes
    //  

    // Public attributes
    //  


    // Public attribute accessor methods
    //  


    // Public attribute accessor methods
    //  


protected:

    // Static Protected attributes
    //  

    // Protected attributes
    //  

public:


    // Protected attribute accessor methods
    //  

protected:

public:


    // Protected attribute accessor methods
    //  

protected:


private:

    // Static Private attributes
    //  

    // Private attributes
    //  

    // Array index corresponds to entity id. Array cell holds the position of the three arrays (positions, velocities and accelerations) where physical data for the given id is stored.
    std::vector<uint> id_index;
    std::vector <Coordinates_t> positions;
    std::vector <Coordinates_t> velocities;
    std::vector <Coordinates_t []> accelerations;
    // Indexes of movable entities. Movable entities are those who need from the Physics system to update its position.
    std::vector<int> movable;
    // Indexes of agent entities. Agent entities are those who need from the AiControl system to request a new acceleration.
    std::vector<int> agents;
public:


    // Private attribute accessor methods
    //  

private:

public:


    // Private attribute accessor methods
    //  


    /**
     * Set the value of id_index
     * Array index corresponds to entity id. Array cell holds the position of the three
     * arrays (positions, velocities and accelerations) where physical data for the
     * given id is stored.
     * @param new_var the new value of id_index
     */
    void setId_index (std::vector<uint> new_var)     {
            id_index = new_var;
    }

    /**
     * Get the value of id_index
     * Array index corresponds to entity id. Array cell holds the position of the three
     * arrays (positions, velocities and accelerations) where physical data for the
     * given id is stored.
     * @return the value of id_index
     */
    std::vector<uint> getId_index ()     {
        return id_index;
    }

    /**
     * Set the value of positions
     * @param new_var the new value of positions
     */
    void setPositions (std::vector <Coordinates_t> new_var)     {
            positions = new_var;
    }

    /**
     * Get the value of positions
     * @return the value of positions
     */
    std::vector <Coordinates_t> getPositions ()     {
        return positions;
    }

    /**
     * Set the value of velocities
     * @param new_var the new value of velocities
     */
    void setVelocities (std::vector <Coordinates_t> new_var)     {
            velocities = new_var;
    }

    /**
     * Get the value of velocities
     * @return the value of velocities
     */
    std::vector <Coordinates_t> getVelocities ()     {
        return velocities;
    }

    /**
     * Set the value of accelerations
     * @param new_var the new value of accelerations
     */
    void setAccelerations (std::vector <Coordinates_t []> new_var)     {
            accelerations = new_var;
    }

    /**
     * Get the value of accelerations
     * @return the value of accelerations
     */
    std::vector <Coordinates_t []> getAccelerations ()     {
        return accelerations;
    }

    /**
     * Set the value of movable
     * Indexes of movable entities. Movable entities are those who need from the
     * Physics system to update its position.
     * @param new_var the new value of movable
     */
    void setMovable (std::vector<int> new_var)     {
            movable = new_var;
    }

    /**
     * Get the value of movable
     * Indexes of movable entities. Movable entities are those who need from the
     * Physics system to update its position.
     * @return the value of movable
     */
    std::vector<int> getMovable ()     {
        return movable;
    }

    /**
     * Set the value of agents
     * Indexes of agent entities. Agent entities are those who need from the AiControl
     * system to request a new acceleration.
     * @param new_var the new value of agents
     */
    void setAgents (std::vector<int> new_var)     {
            agents = new_var;
    }

    /**
     * Get the value of agents
     * Indexes of agent entities. Agent entities are those who need from the AiControl
     * system to request a new acceleration.
     * @return the value of agents
     */
    std::vector<int> getAgents ()     {
        return agents;
    }
private:


    void initAttributes () ;

};

#endif // ENTITYMANAGER_H
