#ifndef __RENDEROBJTERMINAL_H__
#define __RENDEROBJTERMINAL_H__

#include "renderobjimpl.h"

#include <cstdint>
#include <string>
#include <fstream>

class RenderObjTerminal : public RenderObjImpl
{
    public:
        RenderObjTerminal() = delete;
        RenderObjTerminal(std::string objf) : RenderObjImpl(std::move(objf)) {};
        RenderObjTerminal(const RenderObjTerminal& other);
        virtual ~RenderObjTerminal() = default;

        void setAppearance(const std::string& app);
        void setAttribute(const std::string& att);
        void setFGColor(const std::string& fg);
        void setBGColor(const std::string& bg);

        virtual void draw() const override;
        virtual void setPosition(uint8_t x, uint8_t y) override;

        uint8_t getX() const override { return m_x; }
        uint8_t getY() const override { return m_y; }

        friend std::istream& operator>>(std::istream& in, RenderObjTerminal& robj);
        // friend class RenderObjXXXX

    protected:
        std::string m_appearance    = "O";
        uint_8t     m_attrib        = ANSI_NORMAL;
        uint_8t     m_fgcolor       = ANSI_BLACK;
        uint_8t     m_bgcolor       = ANSI_WHITE;
        uint_8t     m_x             = 0;
        uint_8t     m_y             = 0;

}; //RenderObjImpl


#endif
